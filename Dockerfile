FROM node:11.1.0-slim
ARG UID=1010
ARG GID=1010
RUN groupadd -g ${GID} 4fitness  || groupmod -n 4fitness `getent group ${GID} | cut -d: -f1` \
    && useradd --shell /bin/bash -u ${UID} -g ${GID} -m 4fitness
ADD . /backend_4fitness
WORKDIR backend_4fitness
RUN npm i forever -g n
#RUN n stable
RUN chown -R 4fitness /backend_4fitness
USER 4fitness
RUN npm install
EXPOSE 3000
#ENV DBLINK=mongodb://127.0.0.1:27017/4FitnessGirls
CMD [ "npm", "run", "dev" ]
