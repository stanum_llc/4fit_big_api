# Server

#### Preparing

* Install NPM, Node and MongoDB
* Update to latest versions ```sudo npm i forever -g```
* Start project
  * Dev ```npm run dev```
  * Production ```npm run prod```
* Open project link

* Stop project
  * Dev ```CTRL+C```
  * Prod ```npm stop```
