'use strict';
// Main modules

const app = require('./app/core/app.js');
const fs = require('fs');
const path = require('path');
const cp = require('child_process');

// ENV variables
if (!process.env.NODE_ENV)
	process.env.NODE_ENV = 'development';

process.env.LogAll = false;
process.env.CURRENT_API = '1.0.7.8.0';

// SHORT API VERSION SLICING
let APIV = process.env.CURRENT_API.split('.');
APIV = `${APIV[0]}.${APIV[1]}`;

process.env.API_VERSION = APIV;
process.env.RootPath = __dirname + '/app/';
process.env.Core = __dirname + '/app/core/';
process.env.PrivatePath = __dirname + '/private/';
process.env.PublicPath = __dirname + '/public/';
process.env.KeysPath = __dirname + '/app/core/keys/';
process.env.Front = __dirname + '/front/';
process.env.Out_File = __dirname + '/out.log';
process.env.Err_File = __dirname + '/err.log';

// Trainings videos path
process.env.VideosPath = path.join(__dirname, '../../../home/video/');
process.env.UserImagesPath = path.join(__dirname, '../../../home/video/userImages/');

process.env.FirebaseKey = 'AAAAjuxT8lk:APA91bFsOO7JOUoP-TdpO8F13ixIuByZQrZKEob4E4X6ybf5gL2MDKeIudqy5zJSK05PsqcDIzsrVDNiU3gWQA1t19FzyihbNm0xqgl9CH96XpXmnvQbtLlqTosEwASXJjxfHYUY9Jj0';

process.env.APIURL = 'http://api.4fitnessgirls.net/';
process.env.MONGO_URL = 'mongodb://localhost:27017/4FitnessGirls';

if (process.env.NODE_ENV == 'production') {
	process.env.APIURL = 'https://api.4fitnessgirls.com:4443/';
	process.env.MONGO_URL = 'mongodb://172.31.17.112:27017/4FitnessGirls';
} else if (process.env.NODE_ENV == 'stage') {
	process.env.APIURL = 'https://dev.4fitnessgirls.com:3000/';
	process.env.MONGO_URL = 'mongodb://192.168.0.104:27017/4FitnessGirls';
} else if (process.env.NODE_ENV == 'local') {
	process.env.APIURL = 'http://localhost:3000/';
	process.env.MONGO_URL = 'mongodb://localhost:27017/4FitnessGirls';
	process.env.UserImagesPath = path.join(__dirname, 'bin/');
}

if (process.env.DBLINK) {
	process.env.MONGO_URL = process.env.DBLINK;
}

require('./app/core/router.js')(app);

// RUN ASSISTANT
if (process.env.ASSISTANT == 'true' || process.env.NODE_ENV == 'local') {
	let Assistant = cp.fork(process.env.Core + 'assistant.js');
	Assistant.send({});
}

// Admin access only

require('./app/adminAccess/adminLog.js')(app);
require('./app/adminAccess/fillAllTables.js')(app);
require('./app/adminAccess/fillAllNetTables.js')(app);
require('./app/adminAccess/fillNutrition.js')(app);
require('./app/adminAccess/fillPurchases.js')(app);
require('./app/adminAccess/fillSeries.js')(app);
require('./app/adminAccess/fillAssistant.js')(app);

require('./app/adminAccess/fillDescriptions.js')(app);
require('./app/adminAccess/fillTrainings.js')(app);

require('./app/adminAccess/fillPregnantDescriptions.js')(app);
require('./app/adminAccess/fillPregnantTrainings.js')(app);

require('./app/adminAccess/fillWarmUp.js')(app);
require('./app/adminAccess/fillVideosBase.js')(app);
require('./app/adminAccess/dbSize.js')(app);
require('./app/adminAccess/checkDBConnections.js')(app);
require('./app/adminAccess/applyTranslates.js')(app);
