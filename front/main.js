$(document).ready(function() {
  $('#devices .ios, #devices .android').on('click', function() {
    $('#devices .ios').toggleClass('active');
    $('#devices .android').toggleClass('active');
    $('#devices .iphone').toggleClass('disable');
    $('#devices .samsung').toggleClass('disable');
  });

  $('#main').mousemove(function(e) {
    let pw = $(window).width();
    let ph = $(window).height();
    let mouseX = e.pageX * 100 / pw;
    let mouseY = e.pageY * 100 / ph;

  });

  $(window).scroll((e)=> {
    var devicesPosition = $('section#devices').offset().top - $(window).scrollTop();
    var trainingsPosition = $('section#trainings').offset().top - $(window).scrollTop();
    var assistantPosition = $('section#assistant').offset().top - $(window).scrollTop();

    if (devicesPosition <= 100)
      $('section#devices').addClass('animate');

    if (trainingsPosition <= 100)
      $('section#trainings').addClass('animate');

    if (assistantPosition <= 100)
      $('section#assistant').addClass('animate');
  });
});
