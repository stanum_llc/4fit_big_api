const jwt = require('jsonwebtoken');

module.exports = function(data, query, callback) {
  if (this.userToken) {
    const _id = jwt.decode(this.userToken)._id;
    let search = {userid: _id};
    let date = new Date();
    let today = date.getFullYear() + '.' + (date.getMonth() + 1) + '.' + date.getDate();
    if (!query.date)
      search.date = today;
    else
      search.date = query.date;

    this.DBCore.find('water', search, (res)=> {
      if (res == null)
        callback({code: 404, body: {message: 'No data about water balance'}});
      else {
        callback({code: 200, body: res.waterBalance});
      }
    });
  } else {
    callback({code: 401, body: {message: 'Unauthorized. Please send access token'}});
  }
};
