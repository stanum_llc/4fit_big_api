const jwt = require('jsonwebtoken');

module.exports = function(data, query, callback) {
  // console.log('0');
  if (this.userToken) {
    // console.log('1');
    // try {
    const _id = jwt.decode(this.userToken)._id;
    // } catch (e) {
    //   console.log(e);
    // }
    // console.log('2');
    // const Analyze = require(`${process.env.RootPath}modules/${this.version}/analyze/userKkal.js`);
    // const _analyze = new Analyze(this.userToken, '', 'userKkal');
    // Analyze(this.userToken, (plan)=> {
    // if (!plan.code) {
    // console.log('3');
    // console.log(search);
    // let search = {kilocalories: plan.toString()};
    // query.day ? search.day = query.day.toString() : '';
    this.DBCore.find('userPurchases', {
      userId: _id,
      type: {$in: ['trainings', 'pregnant']},
      status: 1
    }, (purchase)=> {
      if (purchase) {
        let search = {
          userId: _id,
          type: 'nutrition',
          purchaseId: purchase._id.toString()
        };

        this.DBCore.findAll('userCalendar', search, (res)=> {
          if (res == null)
            callback({code: 404, body: {message: 'Nutrition plan not found'}});
          else
            callback({code: 200, body: res});
        });
      } else {
        callback({code: 404, body: 'You dont have current nutrition program'});
      }
    });

    // } else {
    // callback({code: 401, body: {message: 'Unauthorized. Please send access token.'}});
    // }
    // });
  } else {
    callback({code: 401, body: {message: 'Unauthorized. Please send access token'}});
  }
};
