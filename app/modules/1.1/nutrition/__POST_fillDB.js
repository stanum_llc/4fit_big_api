const fs = require('fs');
const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const MongoUrl = process.env.MONGO_URL;

module.exports = function(data, callback) {
  if (this.userToken == 'WpNMu$@%o^8!0z@8O>^DY`9ZLgOkKJn_H0:_`z8/o?l#[kM]KU{aMo?nOr>S~?(') {
    const DB = new DBCore();
    DB.drop('nutrition');
    let nutritionFolder = `${process.env.PrivatePath}nutrition/`;
    fs.readdir(nutritionFolder, (err, files) => {
      let count = 0;
      files.forEach(file => {
        fs.readFile(`${nutritionFolder}${file}`, 'utf-8', (err, data)=> {
          nutritionParser(data);
          count++;
          if (count == files.length)
            callback({code: 200, body: {message: 'OK'}});
        });
      });
    });
  } else
    callback({code: 401});
};

function nutritionParser(data) {
  let d = data;
  d = d.replace(/,/g, '.');
  d = d.replace(/;/g, ',');
  d = d.replace(/# /g, '');
  d = d.replace(/;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;/g, '');
  d = d.replace('кол-во грамм', 'грамм');
  d = d.replace('кол-во штук', 'штук');
  d = d.replace('кол-во грамм', 'з-грамм');
  d = d.replace('кол-во штук', 'з-штук');

  const csv = d.split('\n');
  const names = csv[0].split(',');
  let json = {};
  let r = {};

  delete csv[0];

  csv.map((e, i)=> {
    if (e) {
      json[i] = {};
      let n = e.split(',');

      n.map((ne,ni)=> {
        if (ne.length > 0)
          json[i][names[ni]] = ne;
      });
    }
  });
  let day;
  let days;
  let group;
  let priyom;
  let waterLimit;
  for (let e in json) {
    if (json[e]['группы'] && json[e]['группы'].match(/группа/gi)) {
      group = json[e]['группы'].match(/\d/gi).join('');
      if (!r.group)
        r[group] = {};
    }
    if (json[e]['день'] && json[e]['день'].match(/день/gi)) {
      day = json[e]['день'].match(/([0-9]{1,})/gi)[0];
      days = (json[e]['день'].match(/\/([0-9]{1,})/gi)[0]).toString().replace('/', '');
      if (!r[group][day])
        r[group][day] = {
          kilocalories: group,
          day: day,
          days: days,
          waterLimit
        };
      if (!r[group][day]['menu'])
        r[group][day]['menu'] = [];
    }
    if (json[e]['приёма пищи'] && json[e]['приёма пищи'].match(/приём пищи/gi)) {
      priyom = json[e]['приёма пищи'].match(/([0-9]{1})/gi) - 1;
      if (!r[group][day].menu[priyom])
        r[group][day].menu[priyom] = {};
    }
    if (json[e]['приёма пищи'] && json[e]['приёма пищи'].match(/сумма/gi)) {
      r[group][day].menu[priyom].protein = json[e]['БЕЛОК гр'];
      r[group][day].menu[priyom].fat = json[e]['ЖИР грм'];
      r[group][day].menu[priyom].carbohydrates = json[e]['УГЛЕВ грм'];
      r[group][day].menu[priyom].kilocalories = json[e]['ККАЛ'];
    }
    if (json[e]['']) {
      let count = parseFloat(json[e][''].match(/[0-9](.[0-9])?( )?лит/));
      waterLimit = count;
      if (!r[group][day].waterLimit)
        r[group][day].waterLimit = waterLimit;
    }
    if (json[e]['наименование'] && (json[e]['штук'] || json[e]['грамм'])) {
      let menu = {
        name: json[e]['наименование'],
        gramm: parseInt(json[e]['грамм']),
        count: parseInt(json[e]['штук']),
        protein: parseInt(json[e]['БЕЛОК гр']),
        fat: parseInt(json[e]['ЖИР грм']),
        carbohydrates: parseInt(json[e]['УГЛЕВ грм']),
        kilocalories: parseInt(json[e]['ККАЛ'])
      };
      if (json[e]['замена']) {
        menu.replace = {
          name: parseInt(json[e]['замена']),
          gramm: parseInt(json[e]['з-грамм']),
          count: parseInt(json[e]['з-штук'])
        };
      }
      if (!r[group][day].menu[priyom].products)
        r[group][day].menu[priyom].products = [];
      r[group][day].menu[priyom].products.push(menu);
    }
  }

  let result = r;
  const DB = new DBCore();

  Object.keys(result[group]).map((e,i)=> {
    DB.insert('nutrition', result[group][e]);
  });
}

class DBCore{
  insert(col, data) {
    connectToDB(col, (collection, close)=> {
      collection.insert(data, (err, result)=> {
        if (err)
          global.sysLog(err, 'err');
        close();
      });
    });
  }
  drop(col) {
    connectToDB(col, (collection, close)=> {
      collection.drop(()=> {
        close();
      });
    });
  }
};

function connectToDB(col, callback) {
  MongoClient.connect(MongoUrl, (err, database)=> {
    if (err)
      global.sysLog(err, 'err');
    callback(database.db('4FitnessGirls').collection(col), ()=> {
      database.close();
    });
  });
}
