const jwt = require('jsonwebtoken');

module.exports = function(data, callback) {
  const calendarUpdater = require(`${process.env.RootPath}modules/${this.version}/calendar/updater.js`);
  console.log(this.userToken);
  if (this.userToken) {
    const _id = jwt.decode(this.userToken)._id;
    data = this.Encryptor.encryptMany(data);
    if (data) {
      this.DBCore.update('users', {
        _id: _id
      }, data, (res) => {
        if (res && res.result.ok == 1 &&
          (res.result.nModified == 1 || res.result.n == 1)) {
          this.DBCore.find('users', {
            _id: _id
          }, (res) => {

            res = this.Encryptor.decryptMany(res);

            delete res.password;
            delete res.accessPassword;

            console.log(res);

            callback({
              code: 200,
              body: res
            });
          });
        } else {
          callback({
            code: 404,
            body: {
              message: 'User not found. Please, update your token'
            }
          });
        }
      });
    } else {
      callback({
        code: 204,
        body: {
          message: 'Empty update data'
        }
      });
    }
  } else {
    callback({
      code: 401,
      body: {
        message: 'Unauthorized. Please send access token'
      }
    });
  }
};
