const randKey = require('randomstring');

module.exports = function(data, callback) {
  data.email = data.email.replace(/ /g, '');
  data = this.Encryptor.encryptMany(data);
  if (data.email) {

    const MAIL = require(`${process.env.RootPath}/core/mailer.js`);
    this.DBCore.find('users', data, (res)=> {
      if (res == null)
        callback({code: 404, body: {message: 'User not found'}});
      else {
        res = this.Encryptor.decryptMany(res);
        const mailer = new MAIL(res.email);
        const id = res._id;
        let update = {'accessPassword': randKey.generate(7)};
        let updateEnc = this.Encryptor.encryptMany(update);
        this.DBCore.update('users', {_id: id}, updateEnc, (res)=> {
          mailer.send('accessPassword', {
            text: update.accessPassword,
            subject: 'Change password'
          });
          let body = {message: 'Success'};
          if (process.env.NODE_ENV == 'development')
            body = {message: update.accessPassword};

          callback({code: 200, body: body});
        });
      }
    });
  } else {
    callback({code: 404, body: {message: 'User not found'}});
  }
};
