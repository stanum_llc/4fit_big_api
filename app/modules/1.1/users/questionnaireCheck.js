const fs = require('fs');
const jwt = require('jsonwebtoken');
const Encryptor = require(process.env.Core + 'encryptor.js');
const Enc = new Encryptor('userData');

module.exports = function(data) {
  data = Enc.decryptMany(data);
  let result = false;

  let yo = new Date(Date.now() - Date.parse(data.bdate));
  var ageDate = new Date(yo);
  let years = Math.abs(ageDate.getUTCFullYear() - 1970);

  if (!data.weight || !data.height || !data.bdate || data.weight == '' ||
      data.height == '' || data.bdate == '' || data.weight == '' ||
      years == NaN || years == 'NaN')
    result = true;

  return result;
};
