const jwt = require('jsonwebtoken');

module.exports = function(data, query, callback) {
  this.DBCore.findAll('signedOnEmails', {}, (users)=> {
    let text = '<table>';
    users.map((user, index)=> {
      text += `<tr><td>${user.email}</td><td>${user.date} </tr> `;
      if (index == users.length - 1) {
        text += '</table>';
        text += `
        <style>
        td{
          border: 1px solid black;
          padding: .5em;
        }
        </style>
        `;
        callback({code: 200, body: text});
      }
    });
  });
};
