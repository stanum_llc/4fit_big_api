const fs = require('fs');
const jwt = require('jsonwebtoken');
const questionareCheck = require(`./questionnaireCheck.js`);

module.exports = function(data, callback) {
  data = this.Encryptor.encryptMany(data);
  console.log(data);
  this.DBCore.find('users', data, (res) => {
    if (res == null) {
      callback({ code: 404, body: { message: 'User not found' } });
    } else {
      const cert = fs.readFileSync(`${process.env.KeysPath}token.key`, 'utf-8');
      const uid = res._id.toString();
      const token = jwt.sign({ _id: uid }, cert);
      const questionare = questionareCheck(res);

      let user = this.Encryptor.decryptMany(res);

      delete user.password;
      delete user.accessPassword;
      delete user.password;

      callback({
        code: 200,
        body: {
          token: token,
          questionnaire: questionare,
          user: user
        }
      });
    }
  });
};
