const randKey = require('randomstring');
const jwt = require('jsonwebtoken');

module.exports = function(data, callback) {
  data = this.Encryptor.encryptMany(data);
  if (this.userToken) {
    const _id = jwt.decode(this.userToken)._id;
    if (data.accessPassword) {
      this.DBCore.find('users', {_id: _id, accessPassword: data.accessPassword}, (res)=> {
        if (res == null)
          callback({code: 404, body: {message: 'Code incorrect'}});
        else {
          this.DBCore.update('users', {_id: res._id},
            {
              email: data.newEmail,
              accessPassword: data.newEmail
            }, (res)=> {
              if (res.result.ok == 1 &&
                (res.result.nModified == 1 || res.result.n == 1)) {
                this.DBCore.find('users', {_id: _id, email: data.newEmail}, (res)=> {
                  callback({code: 200, body: this.Encryptor.decryptMany(res)});
                });
              } else {
                callback({code: 404, body: {message: 'User not found'}});
              }
            });
        }
      });
    } else {
      // if (data.email) {
      const MAIL = require(`${process.env.RootPath}/core/mailer.js`);
      this.DBCore.find('users', {_id: _id}, (res)=> {
        if (res == null)
          callback({code: 404, body: {message: 'User not found'}});
        else {
          res = this.Encryptor.decryptMany(res);
          const mailer = new MAIL(res.email);
          let update = {'accessPassword': randKey.generate(7)};
          updateEnc = this.Encryptor.encryptMany(update);
          this.DBCore.update('users', {_id: _id}, updateEnc, (res)=> {
            mailer.send('accessPassword', {
              text: update.accessPassword,
              subject: 'Change email'
            });
            let body = {message: 'Success'};
            if (process.env.NODE_ENV == 'development')
              body = {message: update.accessPassword};

            callback({code: 200, body: body});
          });
        }
      });
      // } else {
      // callback({code: 404, body: {message: 'User not found'}});
      // }
    }
  } else {
    callback({code: 401, body: {message: 'Unauthorized. Please send access token'}});
  }
};
