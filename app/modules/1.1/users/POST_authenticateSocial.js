const fs = require('fs');
const jwt = require('jsonwebtoken');

module.exports = function(data, callback) {
  const questionareCheck = require(`${process.env.RootPath}modules/${this.version}/users/questionnaireCheck.js`);
  const assistantMessage = require(`${process.env.RootPath}modules/${this.version}/assistant/writeMessage.js`);
  // :// TODO: Instagram authentication. Need domain and registered application
  if (data.social == 'fb') {
    this.Social.fbTokenCheck(data.accessToken, (response) => {
      if (!response.error) {
        let search = {
          fb_id: response.id
        };
        search = this.Encryptor.encryptMany(search);
        this.DBCore.find('users', search, (res) => {
          if (res == null) {
            let userData = {
              name: response.first_name,
              surname: response.last_name,
              fb_id: response.id,
              notifications: [1,1,1,1]
            };
            userData = this.Encryptor.encryptMany(userData);
            this.DBCore.insert('users', userData, (res) => {
              const cert = fs.readFileSync(`${process.env.KeysPath}token.key`);
              const uid = res.ops[0]._id.toString();
              const token = jwt.sign({
                _id: uid
              }, cert);
              const questionare = questionareCheck(res);
              // let user = this.Encryptor.decryptMany(res);
              this.DBCore.find('users', search, (res) => {
                delete res.password;
                delete res.accessPassword;
                delete res.password;

                assistantMessage(this.DBCore, {
                  uid: res._id,
                  message: `Hello, ${res.name}! I am your personal fitness assistant! I will remind you of missed workouts, nutrition and everything that will be important for you to achieve goals! You can rely on me! I will be your assistant!`,
                  type: 'text',
                  action: ''
                });

                callback({
                  code: 200,
                  body: {
                    token: token,
                    questionnaire: questionare,
                    user: res
                  }
                });
              });
            });
          } else {
            const cert = fs.readFileSync(`${process.env.KeysPath}token.key`);
            const uid = res._id.toString();
            const token = jwt.sign({
              _id: uid
            }, cert);
            const questionare = questionareCheck(res);
            let user = this.Encryptor.decryptMany(res);

            delete user.password;
            delete user.accessPassword;
            delete user.password;

            callback({
              code: 200,
              body: {
                token: token,
                questionnaire: questionare,
                user: user
              }
            });
          }
        });
      } else {
        console.log({code: 190,
          body: {
            message: response.error.message
          }});
        callback({
          code: 190,
          body: {
            message: response.error.message
          }
        });
      }
    });
  } else if (data.social == 'vk') {
    this.Social.vkTokenCheck(data.accessToken, (response) => {
      let search = {
        vk_id: response.id.toString()
      };

      console.log(search);
      // search = this.Encryptor.encryptMany(search);
      this.DBCore.find('users', search, (res) => {
        if (res == null) {
          let userData = {
            name: response.first_name,
            surname: response.last_name,
            vk_id: response.id.toString(),
            notifications: [1,1,1,1]
          };
          userData = this.Encryptor.encryptMany(userData);
          this.DBCore.insert('users', userData, (res) => {

            const calendarUpdater = require(`${process.env.RootPath}modules/${this.version}/calendar/updater.js`);

            calendarUpdater(res.ops[0]._id.toString(), {type: 'registration'}, ()=> {});

            const cert = fs.readFileSync(`${process.env.KeysPath}token.key`);
            const uid = res.ops[0]._id.toString();
            const token = jwt.sign({
              _id: uid
            }, cert);
            const questionare = questionareCheck(res);
            let user = this.Encryptor.decryptMany(res);

            delete user.password;
            delete user.accessPassword;
            delete user.password;

            assistantMessage(this.DBCore, {
              uid: user._id,
              message: `Hello, ${user.name}! I am your personal fitness assistant! I will remind you of missed workouts, nutrition and everything that will be important for you to achieve goals! You can rely on me! I will be your assistant!`,
              type: 'text',
              action: ''
            });

            callback({
              code: 200,
              body: {
                token: token,
                questionnaire: questionare,
                user: user
              }
            });
          });
        } else {
          const cert = fs.readFileSync(`${process.env.KeysPath}token.key`);
          const uid = res._id.toString();
          const token = jwt.sign({
            _id: uid
          }, cert);
          const questionare = questionareCheck(res);
          let user = this.Encryptor.decryptMany(res);

          delete user.password;
          delete user.accessPassword;
          delete user.password;

          callback({
            code: 200,
            body: {
              token: token,
              questionnaire: questionare,
              user: user
            }
          });
        }
      });
    });
  } else if (data.social == 'inst') {
    console.log(data);
    if (!data.message && data.user) {
      const search = {
        inst_id: data.user.id
      };
      this.DBCore.find('users', search, (res) => {
        console.log(res);
        if (res == null) {
          const userData = {
            name: data.user.full_name.split(' ')[0],
            surname: data.user.full_name.split(' ')[1],
            inst_id: data.user.id.toString()
          };
          this.DBCore.insert('users', userData, (res) => {
            const cert = fs.readFileSync(`${process.env.KeysPath}token.key`);
            const uid = res.ops[0]._id.toString();
            console.log(uid);
            const token = jwt.sign({
              _id: uid
            }, cert);
            const questionare = questionareCheck(res);
            let user = this.Encryptor.decryptMany(res);

            delete user.password;
            delete user.accessPassword;
            delete user.password;

            callback({
              code: 200,
              body: {
                token: token,
                questionnaire: questionare,
                inst_token: data.user.id,
                user: user
              }
            });
          });
        } else {
          const cert = fs.readFileSync(`${process.env.KeysPath}token.key`);
          const uid = res._id.toString();
          const token = jwt.sign({
            _id: uid
          }, cert);
          const questionare = questionareCheck(res);
          let user = this.Encryptor.decryptMany(res);

          delete user.password;
          delete user.accessPassword;
          delete user.password;

          callback({
            code: 200,
            body: {
              token: token,
              questionnaire: questionare,
              inst_token: data.user.id,
              user: user
            }
          });
        }
      });
    } else {
      callback({
        code: 401,
        body: {
          message: 'Wrong instagram code, please try again'
        }
      });
    }
  }
};
