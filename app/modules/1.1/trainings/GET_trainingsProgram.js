const jwt = require('jsonwebtoken');
const _ = require('underscore');

module.exports = function(data, query, callback) {
  if (this.userToken) {
    let _id;
    try {
      let t = jwt.decode(this.userToken);
      _id = t._id;
    } catch (e) { callback({code: 503, body: 'error'});}
    if (_id) {
      this.DBCore.find('users', {_id: _id}, (res)=> {
        if (res == null) {
          callback({code: 404, body: {
            message: 'User not found. Please, update your token'
          }});
        } else {
          this.DBCore.find('userPurchases', {userId: _id, status: 1}, (res)=> {
            if (res != null) {
              const search = {
                purchaseId: res._id.toString(),
                userId: _id, type: {$in: ['training', 'day-off']}
              };
              this.DBCore.findAll('userCalendar', search, (res)=> {
                res = _.sortBy(res, 'date');
                res.map((e,i)=> {
                  res[i].list = _.sortBy(res[i].list, 'number');
                });
                callback({code: 200, body: res});
              });
            } else {
              callback({code: 404, body: {message: 'You dont have current trainings program'}});
            }
          });
        }
      });
    } else {
      callback({code: 401, body: {
        message: 'Unauthorized. Please send access token'
      }});
    }
  } else {
    callback({code: 401, body: {
      message: 'Unauthorized. Please send access token'
    }});
  }
};
