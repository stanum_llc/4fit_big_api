const jwt = require('jsonwebtoken');
const _ = require('underscore');

module.exports = function(data, callback) {

  const calendarUpdater = require(`${process.env.RootPath}modules/${this.version}/calendar/updater.js`);
  const Firebase = require(`${process.env.Core}firebase.js`);
  const user = require(`${process.env.RootPath}modules/${this.version}/users/user.js`);
  const assistantMessage = require(`${process.env.RootPath}modules/${this.version}/assistant/writeMessage.js`);

  if (this.userToken) {
    user(this.userToken, (user) => {
      const _id = user._id;
      const searchPurchase = {
        type: {
          $in: ['trainings', 'pregnant']
        },
        status: 1,
        userId: _id.toString()
      };
      this.DBCore.find('userPurchases', searchPurchase, (purchaseInfo) => {
        if (purchaseInfo) {
          const purchaseId = purchaseInfo._id.toString();

          let searchDay = {
            day: parseInt(data.day),
            week: parseInt(data.week),
            purchaseId: purchaseId
          };
          this.DBCore.find('userCalendar', searchDay, (day) => {
            if (day) {
              const dayId = day._id;
              this.DBCore.update('userCalendar', {
                _id: dayId
              }, {
                done: 1
              }, (res) => {
                if (res.result.nModified == 1) {
                  let searchDay = {
                    date: {$gt: day.date},
                    purchaseId: purchaseId,
                    type: 'training'
                  };
                  this.DBCore.findAll('userCalendar', searchDay, (days) => {
                    days = _.sortBy(days, 'date');
                    let day = days[0];
                    let endDate = day.date;
                    endDate = new Date(endDate);
                    endDate = [
                      endDate.getDate() < 10 ? '0' + endDate.getDate() : endDate.getDate(),
                      endDate.getMonth() + 1 < 10 ? '0' + (endDate.getMonth() + 1) : endDate.getMonth() + 1,
                      endDate.getFullYear()
                    ];

                    endDate = endDate.join('.');

                    this.DBCore.find('assistant', {alias: 'dayDone'}, (message)=>{
                      let training = {
                        date: endDate
                      }
                      let msg = JSON.parse(JSON.stringify(message));
                      let text = 'msg.text = `' + msg.text[0] + '`';
                      eval(text);

                      if (user.firebase) {
                        setTimeout(()=> {
                          let message = {
                            to: user.firebase,
                            notification: {
                              title: '4 Fitness Girls',
                              body: msg.text
                            }
                          };

                          Firebase.sendNotification(message);
                        }, 100);
                      }

                      assistantMessage(this.DBCore, {
                        uid: user._id,
                        message: msg.text,
                        type: msg.type,
                        action: msg.action
                      });
                    });

                    callback({
                      code: 200,
                      body: {
                        message: 'Success'
                      }
                    });
                  });
                } else {}
              });
            } else {
              callback({
                code: 404,
                body: {
                  message: 'Day not found'
                }
              });
            }
          });
        } else {
          callback({
            code: 404,
            body: {
              message: 'Purchase not found'
            }
          });
        }
      });
    });
  } else {
    callback({
      code: 401,
      body: {
        message: 'Unauthorized. Please send access token'
      }
    });
  }
};
