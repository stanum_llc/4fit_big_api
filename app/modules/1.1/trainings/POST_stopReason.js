const jwt = require('jsonwebtoken');
const _ = require('underscore');
// const calendarUpdater = require(`${process.env.RootPath}modules/${this.version}/calendar/updater.js`);

module.exports = function(data, callback) {
  console.log(data);
  if (this.userToken) {
    if (data.dayId && data.reason) {
      this.DBCore.update('userCalendar', {_id: data.dayId}, {stopReason: data.reason}, (result)=> {
        callback({code: 200, body: {message: 'Success'}});
      });
    } else {
      callback({code: 418, body: {message: 'Please send all required data'}});
    }
  } else {
    callback({
      code: 401,
      body: {
        message: 'Unauthorized. Please send access token'
      }
    });
  }
};
