const jwt = require('jsonwebtoken');
// const calendarUpdater = require(`${process.env.RootPath}modules/${this.version}/calendar/updater.js`);

module.exports = function(data, callback) {
  if (this.userToken) {
    const _id = jwt.decode(this.userToken)._id;
    const searchPurchase = {
      type: {$in: ['trainings','pregnant']},
      status: 1,
      userId: _id
    };

    this.DBCore.find('userPurchases', searchPurchase, (purchaseInfo) => {
      if (purchaseInfo) {
        const _id = purchaseInfo._id;
        let newState = 0;

        console.log(data);

        if (data.status == 'finish')
          newState = 0;
        else if (data.status == 'pause')
          newState = 2;

        let programUpdate = {
          status: newState,
          weightDifference: data.weightDifference,
          waterTotal: data.waterTotal,
          caloriesBurned: data.caloriesBurned,
          caloriesEaten: data.caloriesEaten,
          caloriesTotal: data.caloriesTotal,
          startDate: data.startDate,
          endDate: data.endDate
        };

        this.DBCore.update('userPurchases', {_id: _id}, programUpdate, (res)=> {
          if (res.result.ok == 1)
            callback({code: 200, body: {message: 'Success'}});
          else
            callback({code: 404, body: {
              message: 'Program not found or already have another status'
            }});
        });

      } else {
        callback({code: 404, body: {message: 'Program not found'}});
      }
    });
  } else {
    callback({
      code: 401,
      body: {
        message: 'Unauthorized. Please send access token.'
      }
    });
  }
};
