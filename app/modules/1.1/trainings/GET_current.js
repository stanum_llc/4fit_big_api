const jwt = require('jsonwebtoken');

module.exports = function(data, query, callback) {
  if (this.userToken) {
    let _id;
    try {
      let t = jwt.decode(this.userToken);
      _id = t._id;
    } catch (e) { callback({code: 503, body: 'error'});}
    if (_id) {
      try {
        this.DBCore.find('users', {_id: _id.toString()}, (user)=> {
          if (user == null) {
            callback({code: 404, body: {
              message: 'User not found. Please, update your token'
            }});
          } else {
            this.DBCore.find('userPurchases', {userId: _id.toString(), status: 1}, (res)=> {
              if (res) {
                const search = {purchaseId: res._id.toString(), type: 'training'};
                this.DBCore.find('purchases', {pid: res.purchaseId}, (purchase)=> {
                  if (purchase) {
                    this.DBCore.findAll('userCalendar', search, (trainingProgram)=> {
                      if (trainingProgram) {
                        search.type = 'day-off';
                        let trainingDays = trainingProgram.length;
                        this.DBCore.findAll('userCalendar', search, (daysOffProgram)=> {
                          let dayOffs = daysOffProgram.length;

                          let response = {
                            android_id: purchase.android_id,
                            ios_id: purchase.ios_id,
                            image: purchase.image,
                            subscription: purchase.subscription,
                            price: purchase.price,
                            name: purchase.label,
                            target: purchase.target,
                            description: purchase.description,
                            buyDate: new Date(res.date).getTime(),
                            trainingDays: trainingDays,
                            dayOffs: dayOffs,
                            weeks: parseInt((trainingDays + dayOffs) / 7),
                            totalDays: trainingDays + dayOffs
                          };
                          callback({code: 200, body: response});
                        });
                      } else {
                        callback({code: 404, body: {message: 'Program not found'}});
                      }
                    });
                  } else {
                    callback({code: 404, body: {message: 'Purchase not found'}});
                  }
                });
              } else {
                callback({code: 404, body: {message: 'Purchase not found'}});
              }
            });
          }
        });
      } catch (e) {
        console.log(e);
        callback({code: 503, body: {message: e}});
      }
    } else {
      callback({code: 401, body: {
        message: 'Unauthorized. Please send access token'
      }});
    }
  } else {
    callback({code: 401, body: {
      message: 'Unauthorized. Please send access token'
    }});
  }
};
