const jwt = require('jsonwebtoken');
const _ = require('underscore');

module.exports = function(data, query, callback) {
  if (this.userToken) {
    const _id = jwt.decode(this.userToken)._id;
    this.DBCore.findAll('assistantMessages', {userId: _id}, (messages)=> {
      if (query.limit) {
        messages = _.sortBy(messages, 'date');
        messages = messages.slice(messages.length - query.limit, messages.length);
      }
      callback({code: 200, body: messages});
    });
  } else {
    callback({code: 401, body: {message: 'Unauthorized. Please send access token'}});
  }
};
