const jwt = require('jsonwebtoken');

module.exports = function(data, callback) {
  if (this.userToken) {
    const _id = jwt.decode(this.userToken)._id;
    this.DBCore.find('assistantMessages', {_id: data.id}, (oldMessage)=> {
      let date = oldMessage.date ? oldMessage.date : Date.getTime();
      let message = {
        message: data.message,
        userId: _id,
        user: 1,
        date: oldMessage.date + 1,
        type: 'text'
      };
      this.DBCore.update('assistantMessages', {_id: data.id}, {type: 'text', show: false, read: true}, (res)=> {
        this.DBCore.insert('assistantMessages', message, (res)=> {
          callback({code: 200, body: {message: 'success'}});
        });
      });
    });
  } else {
    callback({code: 401, body: {message: 'Unauthorized. Please send access token'}});
  }
};
