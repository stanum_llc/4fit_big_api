const jwt = require('jsonwebtoken');

module.exports = function(data, query, callback) {
  if (this.userToken) {
    const _id = jwt.decode(this.userToken)._id;

    let today = new Date(new Date().toISOString().slice(0, 10)).getTime();
    // let tomorrow = new Date(today + 1000 * 60 * 60 * 24).toISOString().slice(0, 10);
    // let yesterday = new Date(today - 1000 * 60 * 60 * 24).toISOString().slice(0, 10);

    from = query.from ? new Date(query.from).getTime() : today;
    to = query.to ? new Date(query.to).getTime() : today;

    let search = {date: {
      $gt: from,
      $lt: to
    }};

    if (query.type)
      search.type = query.type;

    this.DBCore.findAll('calendar', search, (res)=> {
        if (res.length == 0) {
          if (query.from && query.to)
            callback({code: 404, body: {message: 'No one record for this period'}});
          else
            callback({code: 404, body: {message: 'No one record for today'}});
        } else {
          let result = JSON.parse(JSON.stringify(res));

          callback({code: 200, body: result});
        }
      });
  } else {
    callback({code: 401, body: {message: 'Unauthorized. Please send access token'}});
  }
};
