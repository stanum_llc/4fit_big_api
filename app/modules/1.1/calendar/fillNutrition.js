const DB = require(`${process.env.Core}db.js`);
const _ = require('underscore');

process.on('disconnect', function() {
  console.log('parent exited');
  process.exit(0);
});

setTimeout(()=> {
  DB.disconnect();
  process.send({
    code: 503,
    body: {
      message: 'Error while generating program. Process is closed now'
    }
  });
  process.exit();
}, 1000 * 60 * 5);

process.on('message', (data)=> {
  process.env.LogAll == true && console.log('child_process fillNutrition is started');
  if (data.Id) {
    const Analyze = require(`${process.env.RootPath}modules/${process.env.API_VERSION}/analyze/userKkal.js`);
    // const _analyze = new Analyze(this.userToken, '', 'userKkal');
    Analyze(data.Id, (plan)=> {
      if (plan.toString() != 'NaN') {
        let search = {kilocalories: plan.toString()};
        // query.day ? search.day = query.day.toString() : '';
        DB.findAll('nutrition', search, (res)=> {
          if (res == null) {
            DB.disconnect();
            process.send({
              code: 404,
              body: {message: 'Nutrition plan not found'}
            });
          } else {
            let nutrition = _.indexBy(res, 'day');
            let nutritionArray = [];

            Object.keys(nutrition).map((e,i)=> {
              let start = new Date;
              start.setHours(12, 0, 0, 0);
              start = start.getTime();

              // TODO: change on i+1 to increment one more day
              let date = start + (1000 * 60 * 60 * 24 * (i));
              // console.log('n', new Date(date));

              delete nutrition[e]._id;

              nutrition = JSON.parse(JSON.stringify(nutrition));

              nutrition[e].date = date;
              nutrition[e].userId = data.Id;
              nutrition[e].type = 'nutrition';
              nutrition[e].purchaseId = data.purchaseId;
              nutritionArray.push(nutrition[e]);
              // console.log(nutritionArray.length, Object.keys(nutrition).length);
              if (nutritionArray.length == Object.keys(nutrition).length) {
                nutritionArray.map((n,i)=> {
                  // DB.insert('userCalendar', n, (res)=> {
                  DB.insertMany('userCalendar', nutritionArray, (res)=> {
                    if (i == nutritionArray.length - 1) {
                      DB.disconnect();
                      process.send({
                        code: 200,
                        body: {message: 'DONE'}
                      });
                    }
                  });
                  // });
                });
              }
            });
          }
        });
      } else {
        DB.disconnect();
        process.send({
          code: 401,
          body: {
            message: 'Wrong KKAL calculation. Please check your account data.'
          }
        });
      }
    });
  } else {
    DB.disconnect();
    process.send({
      code: 401,
      body: {
        message: 'Unauthorized. Please send access token'
      }
    });
  }
});
