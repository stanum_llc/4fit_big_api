const jwt = require('jsonwebtoken');
const DB = require(`${process.env.Core}db.js`);
const crypto = require('crypto');

module.exports = function(uid, data, callback) {
  // let today = new Date(new Date(new Date().getTime()).toISOString().slice(0, 10)).getTime();

  let today = new Date();
  today.setHours(12, 0, 0, 0);
  today = today.getTime();

  // let today = new Date().getTime();
  DB.find('calendar', {userId: uid, date: today, type: data.type}, (res)=> {
    let todayData = {
      userId: uid,
      date: today
    };
    if (res == null) {
      Object.keys(data).map((e,i)=> {
        todayData[e] = data[e];
      });
      // console.log(todayData);
      DB.insert('calendar', todayData, (res)=> {
        console.log('Writed in calendar successfully');
        callback();
      });
    } else {
      Object.keys(data).map((e,i)=> {
        todayData[e] = data[e];
      });
      // console.log(todayData);
      DB.update('calendar', {_id: res._id}, todayData, (res)=> {
        console.log('Writed in calendar successfully');
        callback();
      });
    }
  });
};
