const DB = require(`${process.env.Core}db.js`);
const _ = require('underscore');
const fs = require('fs');
const cert = fs.readFileSync(`${process.env.KeysPath}series.key`, 'utf-8');
const jwt = require('jsonwebtoken');

let purchaseId;
let id;
let trimestr;
// TODO: Comment increment 1 day for tests
let startDate = new Date;// + (1000 * 60 * 60 * 24);
startDate.setHours(12, 0, 0, 0);
startDate = startDate.getTime();

const approachTimeout = 60;
const exerciseTimeout = 120;

let daysProcessed = 0;
let TrainingNumber = 1;

process.on('disconnect', function() {
  console.log('parent exited');
  process.exit(0);
});

setTimeout(()=> {
  DB.disconnect();
  process.send({
    code: 503,
    body: {
      message: 'Error while generating program. Process is closed now'
    }
  });
  process.exit();
}, 1000 * 60 * 5);

process.on('message', (data)=> {
  console.log('child_process fillForPregnant is started');
  try {
    id = data.Id;
    purchaseId = data.purchaseId;
    const weeks = 4;
    const days = [1,0,1,0,1,0,0];

    DB.find('users', {
      _id: data.Id
    }, (res) => {
      const user = res;
      trimestr = (user.weekOfPregnancy < 14 ? 1 : user.weekOfPregnancy < 28 ? 2 : 3);

      for (let i = 0; i < 4; i++) {
        const week = i;
        days.map((e, dayIndex)=> {
          processDay(data, e, dayIndex, week, (day)=> {

            day.approachTimeout = approachTimeout;
            day.exerciseTimeout = exerciseTimeout;

            DB.insert('userCalendar', day, (res)=> {
              daysProcessed++;
              console.log('Pregnant program', daysProcessed);
              if (daysProcessed == 28) {
                DB.disconnect();
                process.send({
                  code: 200,
                  body: {
                    message: 'Success'
                  }
                });
              }
            });
          });
        });
      }
    });
  } catch (e) {
    console.log(e);
  }
});

function processDay(data, dayType, dayIndex, weekNumber, callback) {
  let date = startDate + (1000 * 60 * 60 * (24 * (dayIndex + weekNumber * 7)));
  let dayTrainingTime = 0;

  const limits = {
    warmup: 4,
    lfk: 3,
    body: 3,
    legs: 3,
    hitch: 3
  };

  const dayList = [
    'warmup',
    'lfk',
    'body',
    'legs',
    'hitch'
  ];

  let dayTrain = {
    purchaseId: purchaseId,
    userId: id,
    day: dayIndex,
    week: weekNumber,
    name: 'Тренировка ' + TrainingNumber,
    done: 0,
    date: date,
    time: 0,
    list: [],
    type: 'training'
  };

  if (dayType == 1) {
    TrainingNumber++;
    dayTrainingTime = 15 * 60 + 120;
    dayList.map((e,i)=> {
      DB.findLimited('pregnantTrainings', {trimestr: trimestr, part: e}, limits[e], {}, (trainings)=> {
        if (trainings.length < limits[e])
          console.log({trimestr: trimestr, part: e});
        if (trainings) {
          trainings = _.shuffle(trainings);
          trainings.map((training, trainingIndex)=> {
            // let training = trainings[index];

            delete training._id;

            training.number = i;
            // training.repeatsDetails = {
            //           approaches: '2',
            //           repeats: '15',
            //           type: 'ra'
            //         };

            // console.log(training);

            DB.find('videosBase', {search: training.search, place: 3}, (videoInfo)=> {
              // delete videoInfo._id;
              let short = {
                u: id,
                type: 'shortVideo',
              };
              let full = {
                u: id,
                type: 'fullVideo',
              };

              if (videoInfo) {

                full.name = videoInfo.video;
                short.name = videoInfo.video;
                full.place = videoInfo.place;
                short.place = videoInfo.place;
                let time = 0;
                if (Object.keys(training.repeatsDetails).length > 0) {
                  if (training.repeatsDetails.repeats) {
                    time = parseInt(videoInfo.videoDuration) *
                              parseInt(training.repeatsDetails.repeats);

                    if (training.repeatsDetails.approaches) {
                      time = parseInt(videoInfo.videoDuration) *
                              parseInt(training.repeatsDetails.repeats) *
                              parseInt(training.repeatsDetails.approaches);

                      time += (parseInt(training.repeatsDetails.approaches) - 1) * approachTimeout;
                    }
                  }
                } else if (training.time)
                  time = training.time;

                dayTrainingTime += time;
              } else {
                console.log('v', training.search);
                short.name = '1';
                full.name = '1';
                short.place = 1;
                full.place = 1;
              }

              short = jwt.sign(short, cert);
              full = jwt.sign(full, cert);

              training.videoSize = videoInfo.size ? videoInfo.size : 0;

              training.video = full;
              training.shortVideo = short;

              DB.find('pregnantDescriptions', {search: training.search}, (description)=> {
                if (description) {
                  delete description.name;
                  delete description.search;
                  delete description._id;

                  training.position = description.position;
                  training.breathing = description.breathing;
                  training.mainMuscles = description.mainMuscles;
                  training.mainAccents = description.mainAccents;
                  training.additionalInfo = description.additionalInfo;
                  training.additionalMuscles = description.additionalMuscles;

                  training.weight = '0';
                  delete training.trimestr;
                  delete training._id;
                  delete training.search;
                  delete training.note;
                  delete training.part;

                  if (e == 'warmup')
                    training.type = 'warmup';
                  else if (e == 'hitch')
                    training.type = 'warmdown';

                } else {
                  console.log('d', training.search);
                }

                dayTrain.list.push(training);

                if (dayTrain.list.length == 16) {
                  dayTrain.durationTime = dayTrainingTime;
                  callback(dayTrain);
                }
              });
            });
          });
        } else {
          console.log('Pregnant program cant found by', {trimestr: trimestr, part: e});
        }
      });
    });
  } else {
    dayTrain.type = 'day-off';
    dayTrain.name = 'Выходной';
    dayTrain.done = 1;
    dayTrain.list = [];
    dayTrain.durationTime = dayTrainingTime;

    callback(dayTrain);
  }
}
