const jwt = require('jsonwebtoken');
const fs = require('fs');
const _ = require('underscore');
const cert = fs.readFileSync(`${process.env.KeysPath}series.key`, 'utf-8');
const DB = require(`${process.env.Core}db.js`);

const Enc = require(`${process.env.Core}encryptor.js`);
const Encryptor = new Enc('userData');

const Mattermost = require('node-mattermost');

const processThisDay = require(__dirname + '/TrainingsGenerator/processThisDay.js');
const userProgramSearch = require(__dirname + '/TrainingsGenerator/userProgramSearch.js');
const checkTrainingsUnique = require(__dirname + '/TrainingsGenerator/checkTrainingsUnique.js');

let id;
let purchaseId;
let userLevel;
let daysCount = 0;
let daysProcessed = 0;
let tPlace;

let exerciseTimeout = 0;
let approachTimeout = 0;

process.on('disconnect', function() {
  console.log('parent exited');
  DB.disconnect();
  process.exit(0);
});

setTimeout(() => {
  DB.disconnect();
  console.log('Error program filling', id);
  process.send({
    code: 503,
    body: {
      message: 'Error while generating program. Process is closed now'
    }
  });
  process.exit(0);
}, 1000 * 60 * 2);

process.on('message', (data) => {
  process.env.LogAll == true && console.most('child_process fillForProgram is started');
  id = data.Id;
  purchaseId = data.purchaseId;
  userLevel = data.level;
  DB.find('users', {
    _id: id
  }, (res) => {
    if (res) {
      res = Encryptor.decryptMany(res);
      if (res == null) {

        process.send({
          code: 404,
          body: {
            message: 'User not found. Please, update your token'
          }
        });
      } else {
        console.log('User found');
        try {
          let user = JSON.parse(JSON.stringify(res));

          tPlace = user.trainingPlace.toString();
          const place = tPlace == '0' ? 'home' :
            tPlace == '1' ? 'street' :
              tPlace == '2' ? 'gym' : '';
          const search = userProgramSearch(userLevel, user);

          // Find all trainings by search object
          DB.find('training', search, (trainings) => {

            if (trainings != null && trainings.weeks.length > 0) {

              console.log('Training found');

              let t = JSON.parse(JSON.stringify(trainings));
              t = t[0] || t;

              let program = [];

              for (let i = 0; i < t.weeks.length; i++) {
                daysCount += t.weeks[i].length;
              }

              // Find all pulls for this program
              DB.findAll('pull', search, (pull) => {

                pull = JSON.parse(JSON.stringify(pull));
                // generate()
                // Start day of trainings

                // TODO: Comment increment 1 day for tests
                let startDate = new Date;
                startDate.setHours(12, 0, 0, 0);
                startDate = startDate.getTime();// - (1000 * 60 * 60 * 24 * 8);
                t.weeks.map((week, weekIndex) => {
                  week.map((day, dayIndex) => {
                    // console.log(week);
                    if (!day.name)
                      console.log(dayIndex, weekIndex, search);

                    let dayData = {
                      'startDate': startDate,
                      'day': day,
                      'dayIndex': dayIndex,
                      'weekIndex': weekIndex,
                      'exerciseTimeout': exerciseTimeout,
                      'approachTimeout': approachTimeout,
                      'pull': pull,
                      'place': place
                    };

                    const processVariables = {
                      purchaseId: purchaseId,
                      id: id,
                      userLevel: userLevel,
                      tPlace: tPlace
                    };

                    processThisDay(processVariables, dayData, (dayTrain, list) => {
                      pushDay(dayTrain, list);
                    });
                  });
                });
              });
            } else {

              process.send({
                code: 400,
                body: {
                  code: 5003,
                  message: 'Problem with filling calendar for current purchase'
                }
              });
            }
          });
        } catch (e) {
          console.log(e);

          process.send({
            code: 400,
            body: {
              code: 5002,
              message: 'Problem with filling calendar for current purchase'
            }
          });
        }
      }
    } else {

      process.send({
        code: 404,
        body: {
          message: 'User not found. Please update your token'
        }
      });
    }
  });
});

function pushDay(dayTrain, list) {
  // console.log('Daytrainlist length ', dayTrain.list.length, list);
  if (dayTrain.list.length == list) {
    checkTrainingsUnique(dayTrain, (uniqueTrainings) => {
      DB.insert('userCalendar', uniqueTrainings, (res) => {
        daysProcessed++;

        if (process.env.LogAll == true)
          console.log('Day', daysProcessed);

        // console.log(daysProcessed, daysCount);

        if (daysProcessed == daysCount) {
          console.log(id, 'Inserted ' + daysProcessed + ' days of training program');
          console.log('Fill 4 program process closing');

          process.send({
            code: 200,
            body: {
              message: 'DONE'
            }
          });
        }
      });
    });
  } else {
    // console.log("||", dayTrain.name);
    // console.log('Trainings by day less then must be');
  }
}

console.most = function(text) {
  console.log(text);
  let mattermostURL = 'http://mattermost.envionsoftware.com/hooks/rtcpoj5d8tfhpxrbrkwa7fspkc';
  var mattermost = new Mattermost(mattermostURL, {});
  mattermost.send({
    text: text.toString()
  });
};
