module.exports = function(dayTrain, callback) {
  if (dayTrain.list.length > 1) {
    let uniques = [];
    let doneCalc = dayTrain.list.length;
    try {
      uniqueDayTraining = JSON.parse(JSON.stringify(dayTrain));
      uniqueDayTraining.list = [];
      dayTrain.list.map((e,i)=> {
        doneCalc -= 1;
        let name = e.name.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
        if (!uniques.includes(name)) {
          uniques.push(name);
          uniqueDayTraining.list.push(e);
        }

        if (doneCalc == 0) {
          callback(uniqueDayTraining);
        }
      });
    } catch (e) {
      console.log(e);
    }
  } else {
    callback(dayTrain);
  }
};
