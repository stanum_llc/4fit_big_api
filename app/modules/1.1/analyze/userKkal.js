const jwt = require('jsonwebtoken');
const DBCore = require(`${process.env.RootPath}core/db.js`);

const Enc = require(`${process.env.RootPath}core/encryptor.js`);
const Encryptor = new Enc('userData');

module.exports = function(userId, callback) {
  // const _id = jwt.decode(userToken)._id;
  const _id = userId;
  let plan;
  DBCore.find('users', {
    _id: _id
  }, (res) => {
    if (res == null) {
      callback({
        code: 404,
        body: {
          message: 'User not found. Please, update your token'
        }
      });
    } else {
      res = Encryptor.decryptMany(res);
      try {
        let yo = new Date(Date.now() - Date.parse(res.bdate));
        var ageDate = new Date(yo);
        let years = Math.abs(ageDate.getUTCFullYear() - 1970);

        if (years <= 17) {
          plan = (12.2 * res.weight) + 746;
        } else if (years >= 18 && years <= 29) {
          plan = (14.7 * res.weight) + 496;
        } else if (years >= 30 && years <= 60) {
          plan = (8.7 * res.weight) + 829;
        } else if (years > 60) {
          plan = (10.5 * res.weight) + 569;
        }

        if (res.userLevel < 3) {
          let t = res.target != null ? parseInt(res.target) : 1;
          if (t == 0)
            plan -= 500;
          else if (t == 1)
            plan += 500;
          else if (t == 2)
            plan += plan * 15 / 100;
        } else {
          let week = res.weekOfPregnancy;

          if (week < 14) plan += plan * 5 / 100;
          else if (week < 28) plan += plan * 10 / 100;
          else plan += plan * 25 / 100;
        }

        let round = Math.round(plan / 100) * 100;
        plan = plan < round ? (round / 100 - 1) * 100 + 50 : round + 50;

        if (plan < 1150) plan = 1150;
        if (plan > 2650) plan = 2650;

        console.log('KKAL', plan);
      } catch (e) {
        console.log('error', e);
      }

      callback(plan);
    }
  });
};
