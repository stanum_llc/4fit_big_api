const jwt = require('jsonwebtoken');

module.exports = function(data, callback) {
  data = this.Encryptor.encryptMany(data);
  if (this.userToken) {
    const _id = jwt.decode(this.userToken)._id;
    this.DBCore.find('users', {_id: _id}, (user)=> {
      if (user) {
        // delete user.userLevel;
        // delete user.target;
        // delete user.trainingPlace;
        // delete user.weekOfPregnancy;

        user.userLevel = null;
        user.target = '0';
        user.trainingPlace = '0';
        user.weekOfPregnancy = null;

        this.DBCore.update('users', {_id: _id}, user, (res)=> {
          callback({code: 200, body: {message: 'Success'}});
        });
      } else {
        callback({code: 404, body: {message: 'User not found. Please update your token'}});
      }
    });
  } else {
    callback({code: 404, body: {message: 'User not found'}});
  }
};
