const jwt = require('jsonwebtoken');

module.exports = function(data, callback) {
  const calendarUpdater = require(`${process.env.RootPath}modules/${this.version}/calendar/updater.js`);
  if (this.userToken) {
    const _id = jwt.decode(this.userToken)._id;
    data = this.Encryptor.encryptMany(data);
    this.DBCore.update('users', {_id: _id}, data, (res)=> {
      // console.log(res);
      if (res.result.ok == 1 &&
        (res.result.nModified == 1 || res.result.n == 1)) {
        let response = {
          message: 'Success',
          image: `${process.env.APIURL}public/user/${data.image}`
        };
        callback({code: 200, body: response});
      } else {
        callback({code: 404, body: {message: 'User not found. Please, update your token'}});
      }
    });
  } else {
    callback({code: 401, body: {message: 'Unauthorized. Please send access token'}});
  }
};
