const jwt = require('jsonwebtoken');
// const calendarUpdater = require(`${process.env.RootPath}modules/${this.version}/calendar/updater.js`);

module.exports = function(data, callback) {
  if (this.userToken) {
    const _id = jwt.decode(this.userToken)._id;
    data = this.Encryptor.encryptMany(data);
    this.DBCore.update('users', {_id: _id}, data, (res)=> {
      if (res && res.result.ok == 1 &&
        (res.result.nModified == 1 || res.result.n == 1)) {
        callback({code: 200, body: {message: 'Success'}});
      } else {
        callback({code: 404, body: {message: 'User not found. Please, update your token'}});
      }
    });
  } else {
    callback({code: 401, body: {message: 'Unauthorized. Please send access token'}});
  }
};
