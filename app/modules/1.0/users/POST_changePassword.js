const randKey = require('randomstring');
const jwt = require('jsonwebtoken');

module.exports = function(data, callback) {
  if (!data.oldPassword) {
    if (data.newPassword.length > 4) {
      let search = {email: data.email, accessPassword: data.accessPassword};
      search.email = search.email.replace(/ /g, '');
      search = this.Encryptor.encryptMany(search);
      console.log(search);
      this.DBCore.find('users', search, (res)=> {
        if (res == null)
          callback({code: 404, body: {message: 'User not found'}});
        else
          this.DBCore.update('users', {_id: res._id},
            {
              password: this.Encryptor.encrypt(data.newPassword),
              accessPassword: this.Encryptor.encrypt(data.newPassword)
            }, (res)=> {
            if (res.result.ok == 1 &&
              (res.result.nModified == 1 || res.result.n == 1)) {
              callback({code: 200, body: {message: 'Success'}});
            } else {
              callback({code: 404, body: {message: 'User not found'}});
            }
          });
      });
    } else {
      callback({code: 401, body: {message: 'Unauthorized. Please send access token'}});
    }
  } else {
    if (this.userToken) {
      const _id = jwt.decode(this.userToken)._id;
      // this.DBCore.find('users', {_id: _id}, (data)=> {
      const newPass = this.Encryptor.encrypt(data.newPassword);
      const oldPass = this.Encryptor.encrypt(data.oldPassword);
      this.DBCore.find('users', {_id: _id}, (res)=> {
        if (res.password == oldPass) {
          this.DBCore.update('users', {_id: _id},
            {
              password: newPass
            },
            (res)=> {
            if (res.result.ok == 1 &&
              (res.result.nModified == 1 || res.result.n == 1)) {
              callback({code: 200, body: {message: 'Success'}});
            } else {
              callback({code: 404, body: {message: 'User not found'}});
            }
          });
        } else {
          callback({code: 401, body: {message: 'Wrong current password'}});
        }
      });
      // });
    } else {
      callback({code: 401, body: {message: 'Unauthorized. Please send access token'}});
    }
  }
};
