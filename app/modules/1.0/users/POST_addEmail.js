const randKey = require('randomstring');
const jwt = require('jsonwebtoken');

module.exports = function(data, callback) {
  data = this.Encryptor.encryptMany(data);
  if (this.userToken) {
    const _id = jwt.decode(this.userToken)._id;
    const MAIL = require(`${process.env.RootPath}/core/mailer.js`);
    this.DBCore.find('users', {_id: _id}, (res)=> {
      if (res == null)
        callback({code: 404, body: {message: 'User not found. Please, update your token'}});
      else {
        if (res.email == '' && res.password == '') {
          res = this.Encryptor.decryptMany(res);
          const mailer = new MAIL(res.email);
          const id = res._id;
          let update = {'accessPassword': randKey.generate(7)};
          let updateEnc = this.Encryptor.encryptMany(update);
          this.DBCore.update('users', {_id: id}, updateEnc, (res)=> {
            mailer.send('accessPassword', {
              text: update.accessPassword,
              subject: 'Change password'
            });
            let body = {message: 'Success'};
            if (process.env.NODE_ENV == 'development')
              body = {message: update.accessPassword};

            callback({code: 200, body: body});
          });
        } else {
          callback({code: 409, body: {message: 'This user already has email and password'}});
        }
      }
    });
  } else {
    callback({code: 404, body: {message: 'Unauthorized. Please send access token'}});
  }
};
