const jwt = require('jsonwebtoken');
const DBCore = require(`${process.env.Core}/db.js`);
const Enc = require(`${process.env.Core}/encryptor.js`);
Encryptor = new Enc('userData');

module.exports = function(userToken, callback) {
  if (userToken) {
    const _id = jwt.decode(userToken)._id;
    DBCore.find('users', {_id: _id}, (res)=> {
      if (res == null) {
        callback({code: 404, body: {message: 'User not found. Please, update your token'}});
      } else {
        let result = JSON.parse(JSON.stringify(res));

        delete result.password;
        delete result.accessPassword;

        result = this.Encryptor.decryptMany(result);

        callback(result);
      }
    });
  } else {
    callback({code: 401, body: {message: 'Unauthorized. Please send access token'}});
  }
};
