const fs = require('fs');
const jwt = require('jsonwebtoken');
const questionareCheck = require(`./questionnaireCheck.js`);

module.exports = function(data, query, callback) {
  data = this.Encryptor.encryptMany(data);

  const _id = jwt.decode(this.userToken)._id;
  this.DBCore.find('users', {_id: _id}, (res)=> {
    if (res == null) {
      callback({code: 404, body: {message: 'User not found'}});
    } else {
      const questionare = questionareCheck(res);
      callback({code: 200, body: {questionare: questionare}});
    }
  });
};
