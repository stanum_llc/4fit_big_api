const jwt = require('jsonwebtoken');

module.exports = function(data, query, callback) {
  if (this.userToken) {
    const _id = jwt.decode(this.userToken)._id;
    this.DBCore.find('users', {_id: _id}, (res)=> {
      if (res == null) {
        callback({code: 404, body: {message: 'User not found. Please, update your token'}});
      } else {
        let result = JSON.parse(JSON.stringify(res));

        delete result.password;
        delete result.accessPassword;

        result = this.Encryptor.decryptMany(result);

        callback({code: 200, body: result});
      }
    });
  } else {
    callback({code: 401, body: {message: 'Unauthorized. Please send access token'}});
  }
};
