const fs = require('fs');
const jwt = require('jsonwebtoken');
const calendarUpdater = require(`${process.env.RootPath}modules/${this.version}/calendar/updater.js`);

module.exports = function(data, callback) {
  const assistantMessage = require(`${process.env.RootPath}modules/${this.version}/assistant/writeMessage.js`);
  let search = {email: data.email};
  search = this.Encryptor.encryptMany(search);
  this.DBCore.find('users', search, (res)=> {

    this.userModel.email = data.email;
    this.userModel.password = data.password;
    this.userModel.name = data.name;
    this.userModel.surname = data.surname;

    if (res == null) {

      let user = data;
      data = this.Encryptor.encryptMany(this.userModel);

      this.DBCore.insert('users', data, (res)=> {

        const cert = fs.readFileSync(`${process.env.KeysPath}token.key`);
        const uid = res.ops[0]._id.toString();
        const token = jwt.sign({_id: uid}, cert);

        calendarUpdater(res.ops[0]._id.toString(), {type: 'registration'}, ()=> {});

        assistantMessage(this.DBCore, {
          uid: user._id,
          message: `Hello, ${this.userModel.name}! I am your personal fitness assistant! I will remind you of missed workouts, nutrition and everything that will be important for you to achieve goals! You can rely on me! I will be your assistant!`,
          type: 'text',
          action: ''
        });

        user = this.Encryptor.decryptMany(res.ops[0]);

        delete user.password;
        delete user.accessPassword;

        callback({code: 201, body: {token: token, user: user}});
      });
    } else {
      callback({code: 409, body: {message: 'User already exist'}});
    }
  });
};
