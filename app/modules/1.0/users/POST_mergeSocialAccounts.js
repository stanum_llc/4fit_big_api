const jwt = require('jsonwebtoken');

module.exports = function(data, callback) {
  const self = this;
  if (self.userToken) {
    const _id = jwt.decode(self.userToken)._id;
    if (data.social == 'fb') {
      self.DBCore.find('users', {_id: _id}, (res)=> {
        if (res == null) {
          callback({code: 401, body:
            {message: 'Unauthorized. Please send access token'}});
        } else {
          if (res.fb_id) {
            callback({code: 409, body:
              {message: 'System already have account with this social Id'}});
          } else {
            self.Social.fbTokenCheck(data.accessToken, (response)=> {
              if (!response.error) {
                let update = {fb_id: response.id};
                update = this.Encryptor.encryptMany(update);
                self.DBCore.update('users', {_id: _id}, update, (res)=> {
                  if (res.result.ok == 1 &&
                    (res.result.nModified == 1 || res.result.n == 1)) {
                    callback({code: 200, body: {message: 'Success'}});
                  } else {
                    callback({code: 404, body:
                      {message: 'User not found. Please, update your token'}});
                  }
                });
              } else {
                callback({code: 190, body:
                  {message: response.error.message}});
              }
            });
          }
        }
      });
    } else if (data.social == 'vk') {
      self.DBCore.find('users', {_id: _id}, (res)=> {
        if (res == null) {
          callback({code: 401, body: {message: 'Unauthorized (wrong token)'}});
        } else {
          if (res.vk_id) {
            callback({code: 409, body:
              {message: 'System already have account with this social Id'}});
          } else {
            self.Social.vkTokenCheck(data.accessToken, (response)=> {
              let update = {vk_id: response.id.toString()};
              update = this.Encryptor.encryptMany(update);
              self.DBCore.update('users', {_id: _id}, update, (res)=> {
                if (res.result.ok == 1 &&
                  (res.result.nModified == 1 || res.result.n == 1)) {
                  callback({code: 200, body: {message: 'Success'}});
                } else {
                  callback({code: 404, body:
                    {message: 'User not found. Please, update your token'}});
                }
              });
            });
          }
        }
      });
    } else if (data.social == 'inst') {
      self.DBCore.find('users', {_id: _id}, (res)=> {
        if (res == null) {
          callback({code: 401, body: {message: 'Unauthorized. Please send access token'}});
        } else {
          if (res.inst_id) {
            callback({code: 409, body:
              {message: 'System already have account with this social Id'}});
          } else {
            // self.Social.vkTokenCheck(data.accessToken, (response)=> {
            let update = {inst_id: data.accessToken};
            update = this.Encryptor.encryptMany(update);
            self.DBCore.update('users', {_id: _id}, update, (res)=> {
              if (res.result.ok == 1 &&
                (res.result.nModified == 1 || res.result.n == 1)) {
                callback({code: 200, body: {message: 'Success'}});
              } else {
                callback({code: 404, body:
                  {message: 'User not found. Please, update your token'}});
              }
            });
            // });
          }
        }
      });
    }
  } else {
    callback({code: 401, body: {message: 'Unauthorized. Please send access token'}});
  }
};
