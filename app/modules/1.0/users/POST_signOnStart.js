const fs = require('fs');
const jwt = require('jsonwebtoken');

module.exports = function(data, callback) {
  if (data.email) {
    this.DBCore.find('signedOnEmails', {email: data.email}, (res)=> {
      if (res == null) {
        const date = new Date();
        this.DBCore.insert('signedOnEmails', {email: data.email, date: date}, (res)=> {
          callback({code: 200, body: {message: 'success'}});
        });
      }
    });
  }
};
