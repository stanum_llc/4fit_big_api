const jwt = require('jsonwebtoken');

module.exports = function(data, callback) {
  const calendarUpdater = require(`${process.env.RootPath}modules/${this.version}/calendar/updater.js`);
  if (this.userToken && data.water) {
    const _id = jwt.decode(this.userToken)._id;

    this.DBCore.find('userPurchases', {
      userId: _id,
      type: 'trainings',
      status: 1
    }, (purchase) => {
      if (purchase) {
        const purchaseId = purchase._id;

        let search = {
          purchaseId: purchaseId.toString(),
          type: 'nutrition',
          day: data.day.toString()
        };

        this.DBCore.find('userCalendar', search, (day) => {
          if (day) {
            day.waterDrinked = data.water;
            calendarUpdater(_id, {
              'nutrition': day,
              type: 'nutrition'
            }, () => {
              this.DBCore.update('userCalendar', {
                _id: day._id,
                day: data.day.toString()
              }, day, (res) => {
                calendarUpdater(_id, {
                  water: data.water,
                  day: day._id.toString(),
                  type: 'water'
                }, () => {
                  callback({
                    code: 200,
                    body: {
                      message: 'Success'
                    }
                  });
                });
              });
            });
          } else {
            callback({
              code: 404,
              body: {
                message: 'Day not found'
              }
            });
          }
        });
      } else {
        callback({
          code: 404,
          body: {
            message: 'Purchase not found'
          }
        });
      }
    });
  } else {
    callback({
      code: 401,
      body: {
        message: 'Unauthorized. Please send access token'
      }
    });
  }
};
