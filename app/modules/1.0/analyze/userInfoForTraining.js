const jwt = require('jsonwebtoken');
const DBCore = require(`${process.env.RootPath}core/db.js`);

const Enc = require(`${process.env.RootPath}core/encryptor.js`);
const Encryptor = new Enc('userData');

module.exports = function(userId, callback) {
  // const _id = jwt.decode(userToken)._id;
  const _id = userId;
  DBCore.find('users', {
    _id: _id
  }, (res) => {
    if (res == null) {
      callback({
        code: 404,
        body: {
          message: 'User not found. Please, update your token'
        }
      });
    } else {
      let user = JSON.parse(JSON.stringify(res));
      let yo = new Date(Date.now() - Date.parse(user.bdate));
      let ageDate = new Date(yo);
      const years = Math.abs(ageDate.getUTCFullYear() - 1970);

      // Trainings place
      const tPlace = res.trainingPlace;
      const place = tPlace == '0' ? 'home' :
        tPlace == '1' ? 'street' :
        tPlace == '2' ? 'gym' : 'pregnant';

      // Timeouts based on userLevel
      if (userLevel == 0) {
        exerciseTimeout = 120;
        approachTimeout = 90;
      } else if (userLevel == 1) {
        exerciseTimeout = 120;
        approachTimeout = 60;
      } else if (userLevel == 2) {
        exerciseTimeout = 90;
        approachTimeout = 60;
      }

      // Basic trainings search Object
      let search = {
        years: '',
        weight: '',
        height: '',
        level: parseInt(userLevel) + 1
      };

      // Search filling
      // YO
      if (years < 29)
        search.years = {
          to: 29
        };
      else if (years >= 30 && years <= 45)
        search.years = {
          from: 30,
          to: 45
        };
      else if (years > 45)
        search.years = {
          from: 45
        };
      // WEIGHT
      if (parseInt(user.weight) < 55)
        search.weight = {
          to: 55
        };
      else if (parseInt(user.weight) >= 55 && parseInt(user.weight) <= 70)
        search.weight = {
          from: 55,
          to: 70
        };
      else if (parseInt(user.weight) > 70)
        search.weight = {
          from: 70
        };
      //HEIGHT
      if (parseInt(user.height) < 170)
        search.height = {
          to: 170
        };
      else if (parseInt(user.height) >= 170)
        search.height = {
          from: 170
        };

      callback(search);
    }
  });
};
