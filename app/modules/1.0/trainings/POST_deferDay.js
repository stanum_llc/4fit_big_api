const jwt = require('jsonwebtoken');
const _ = require('underscore');
// const calendarUpdater = require(`${process.env.RootPath}modules/${this.version}/calendar/updater.js`);

module.exports = function(data, callback) {
  let updatedDays = [];
  console.log(data);
  if (this.userToken) {
    if (data.day >= 0 && data.defer >= 0 && data.week >= 0) {
      const _id = jwt.decode(this.userToken)._id;
      // console.log(_id);
      this.DBCore.find('userPurchases', {userId: _id, type: {$in: ['trainings', 'pregnant']}, status: 1}, (purchase)=> {
        if (purchase) {
          this.DBCore.findAll('userCalendar', {
            userId: _id.toString(),
            type: {$in: ['training', 'day-off', 'warmdown', 'warmup']},
            purchaseId: purchase._id.toString()
          }, (days)=> {
            days = _.sortBy(days, 'date');
            days.map((day, index)=> {
              if (day.week >= data.week) {
                if ((day.week == data.week && day.day >= data.day) || (day.week > data.week)) {
                  // if (!(day.week <= data.week && day.day <= data.day)) {
                  let d = day.date + (1000 * 60 * 60 * (24 * data.defer));
                  console.log(new Date(day.date), '->', new Date(d));
                  day.date = d;
                  updatedDays.push(day);
                }
              }
              if (index == days.length - 1) {
                days.map((e,i)=> {
                  this.DBCore.update('userCalendar', {_id: e._id}, e, (res)=> {
                    if (i == days.length - 1) {
                      callback({code: 200, body: {message: 'Success'}});
                    }
                  });
                });
              }
            });
          });
        } else {
          callback({code: 404, body: {message: 'Purchase not found'}});
        }
      });
    } else {
      callback({code: 418, body: {message: 'Please send all required data'}});
    }
  } else {
    callback({
      code: 401,
      body: {
        message: 'Unauthorized. Please send access token'
      }
    });
  }
};
