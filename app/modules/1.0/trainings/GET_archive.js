const jwt = require('jsonwebtoken');
const _ = require('underscore');

module.exports = function(data, query, callback) {
  if (this.userToken) {
    let _id;
    try {
      let t = jwt.decode(this.userToken);
      _id = t._id;
    } catch (e) { callback({code: 503, body: 'error'});}
    if (_id) {
      this.DBCore.find('users', {_id: _id}, (res)=> {
        if (res == null) {
          callback({code: 404, body: {
            message: 'User not found. Please, update your token'
          }});
        } else {
          // this.DBCore.find('userPurchases', {userId: _id, status: 1}, (res)=> {
          // if (res != null) {
          this.DBCore.findAll('userPurchases', {userId: _id.toString(), status: {$in: [0,2]}}, (purchases)=> {
            if (purchases.length > 0) {
              purchases = _.sortBy(purchases, 'date');

              if (query.limit)
                purchases = purchases.slice(0, query.limit);

              purchases.map((purchase, purchaseIndex)=> {
                const search = {
                  userId: _id, purchaseId: purchase._id.toString(), type: {$in: ['training', 'day-off']}
                };
                this.DBCore.find('purchases', {pid: purchase.purchaseId}, (p)=> {
                  if (p) {
                    purchases[purchaseIndex].label = p.label;
                    purchases[purchaseIndex].android_id = p.android_id;
                    purchases[purchaseIndex].ios_id = p.ios_id;
                    purchases[purchaseIndex].shortDescription = p.shortDescription;
                  }

                  this.DBCore.findAll('userCalendar', search, (training)=> {
                    training = _.sortBy(training, 'date');
                    training.map((e,i)=> {
                      training[i].list = _.sortBy(training[i].list, 'number');
                    });
                    purchases[purchaseIndex].trainings = training;

                    if (purchaseIndex == purchases.length - 1)
                      callback({code: 200, body: purchases});
                  });
                });
              });
            } else {
              callback({code: 404, body: []});
            }
          });
          // } else {
          // callback({code: 404, body: {message: 'You dont have current trainings program'}});
          // }
          // });
        }
      });
    } else {
      callback({code: 401, body: {
        message: 'Unauthorized. Please send access token'
      }});
    }
  } else {
    callback({code: 401, body: {
      message: 'Unauthorized. Please send access token'
    }});
  }
};
