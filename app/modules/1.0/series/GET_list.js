const jwt = require('jsonwebtoken');
const fs = require('fs');
// let Purchase = require(`./purchase.js`);

module.exports = function(data, query, callback) {
  const cert = fs.readFileSync(`${process.env.KeysPath}series.key`, 'utf-8');
  if (this.userToken) {
    const _id = jwt.decode(this.userToken)._id;
    this.DBCore.findAll('series', {}, (res)=> {
      if (res.length > 0) {
        let series = res;

        series[0].purchased = true;

        this.DBCore.findAll('userPurchases', {type: 'series', userId: _id}, (seriesPurchases)=> {

          seriesPurchases.map((e,i)=> {
            if (e.details)
              series[e.details - 1].purchased = true;
          });

          // series[1].purchased = false;
          // series[2].purchased = false;
          series.map((season, seasonIndex)=> {
            let trailerData = {
              userToken: this.userToken,
              season: season.season,
              name: 'teaser',
              type: 'video'
            };
            let seasonCover = {
              userToken: this.userToken,
              season: season.season,
              name: 'seasonCover',
              type: 'image'
            };
            let seasonImage = {
              userToken: this.userToken,
              season: season.season,
              name: 'seasonImage',
              type: 'image'
            };

            series[seasonIndex].seasonTrailer = jwt.sign(trailerData, cert);
            series[seasonIndex].seasonCover = jwt.sign(seasonCover, cert);
            series[seasonIndex].seasonImage = jwt.sign(seasonImage, cert);

            season.series.map((se, si)=> {
              let cover = {
                userToken: this.userToken,
                series: se.series,
                season: season.season,
                type: 'image',
                name: 'cover'
              };
              let video = {
                userToken: this.userToken,
                series: se.series,
                season: season.season,
                type: 'video',
                name: 'video'
              };
              let image = {
                userToken: this.userToken,
                series: se.series,
                season: season.season,
                type: 'image',
                name: se.series
              };



              series[seasonIndex].series[si].image = jwt.sign(image, cert);
              series[seasonIndex].series[si].cover = jwt.sign(cover, cert);
              fs.existsSync(`${process.env.VideosPath}${season.season}/${se.series}.mp4`) == true ?
                  series[seasonIndex].series[si].video = jwt.sign(video, cert) :
                  '' ;
              // series[seasonIndex].series[si].video = jwt.sign(video, cert);
            });
          });
          callback({code: 200, body: series});
        });
      }
    });
    // const userId = this.jwt.decode(this.token)._id;
    // Purchase = new Purchase(this.userToken, 'GET', 'purchases');
    // Purchase.GET_purchases(this.userToken, 'series', (result) => {
    //   result.map((e, i) => {
    //     this.DBCore.insert('tempLinks', {
    //       file: e.name,
    //       userId: userId,
    //       type: 'series'
    //     }, (result) => {});
    //     if (i == result.length) {
    //       this.DBCore.findAll('tempLinks', {
    //         userId: userId,
    //         type: 'series'
    //       }, (result) => {
    //         let r = [];
    //         result.map((re, ri) => {
    //           r[i] = result[i].url;
    //         });
    //         callback({code: 200, body: r});
    //       });
    //     }
    //   });
    // });
  } else {
    callback({
      code: 401,
      body: {
        message: 'Unauthorized. Please send access token'
      }
    });
  }
};
