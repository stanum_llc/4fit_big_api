const jwt = require('jsonwebtoken');

module.exports = function(data, query, callback) {
  if (this.userToken) {
    let today = new Date(new Date(new Date().getTime()).toISOString().slice(0, 10)).getTime();
    // let today = new Date().getTime();
    DB.find('calendar', {userId: uid, date: today}, (res)=> {
      let todayData = {
        userId: uid,
        date: today
      };
      if (res == null) {
        Object.keys(data).map((e,i)=> {
          if (!todayData[e]) todayData[e] = [];
          todayData[e].push(data[e]);
        });

        DB.insert('calendar', todayData, (res)=> {
          callback();
        });
      } else {
        Object.keys(data).map((e,i)=> {
          todayData[e] = res[e];
          todayData[e].push(data[e]);
        });
        DB.update('calendar', {_id: res._id}, todayData, (res)=> {
          callback();
        });
      }
    });
  } else {
    callback({code: 401, body: {message: 'Unauthorized. Please send access token'}});
  }
};
