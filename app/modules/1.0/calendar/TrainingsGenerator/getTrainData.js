const jwt = require('jsonwebtoken');
const fs = require('fs');
const _ = require('underscore');
const cert = fs.readFileSync(`${process.env.KeysPath}series.key`, 'utf-8');
const DB = require(`${process.env.Core}db.js`);

const Enc = require(`${process.env.Core}encryptor.js`);
const Encryptor = new Enc('userData');

module.exports = function(variables, train, name, callback) {
  const tPlace = variables.tPlace;
  const id = variables.id;

  let videoSearch = {
    search: name.replace(/^\s+|\s+$/g, ''),
    place: parseInt(tPlace)
  };
  DB.find('videosBase', videoSearch, (video) => {

    if (!video && process.env.NODE_ENV != 'local') {
      console.log('Video not found. ', train.name);
    }
    let short = {
      u: id,
      type: 'shortVideo',
    };
    let full = {
      u: id,
      type: 'fullVideo',
    };

    train.videoSize = 0;

    if (video) {
      full.name = video.video;
      short.name = video.video;
      full.place = video.place;
      short.place = video.place;
      train.videoSize = video.size ? video.size : 0;
    } else if (videoSearch.search == 'бег') {
      full.name = 188;
      short.name = 188;
      full.place = 2;
      short.place = 2;
    } else if (videoSearch.search == 'ходьба') {
      full.name = 187;
      short.name = 187;
      full.place = 2;
      short.place = 2;
    } else {
      short.name = '1';
      full.name = '1';
      short.place = 1;
      full.place = 1;
    }

    short = jwt.sign(short, cert);
    full = jwt.sign(full, cert);

    train.video = full;
    train.shortVideo = short;
    // train.repeatTime = 120;

    DB.find('descriptions', {
      search: name
    }, (description) => {

        let repeatsObject = {
          approaches: '',
          repeats: ''
        };

        train.repeatsDetails = {};
        train.repeats = train.repeats.replace(/^\s*/, '');
        if (train.repeats.match(/[0-9]\*[0-9]/i)) {
          let left = 0, right = 0;
          try {
            left = train.repeats.split('*')[0];
            right = train.repeats.split('*')[1];
            train.repeatsDetails.repeats = parseInt(right.split('-')[0].split(' ')[0]);
            train.repeatsDetails.approaches = parseInt(left.split('-')[0].split(' ')[0]);
            train.time = video ?
              video.videoDuration * parseInt(train.repeatsDetails.approaches) * parseInt(train.repeatsDetails.repeats) :
              2 * parseInt(train.repeatsDetails.approaches) * parseInt(train.repeatsDetails.repeats);

            train.time += train.repeatsDetails.approaches * approachTimeout;

            if (train.repeats.match(/сек/)) {
              train.time = parseInt(train.repeatsDetails.repeats);
              delete train.repeatsDetails.repeats;
            }
          } catch (e) {
            // console.log(left, right);
            // console.log(train.repeats);
          }
        } else if (train.repeats.match('-')) {
          // console.log("2", train.repeats);
          train.repeatsDetails.repeats = parseInt(train.repeats.split('-')[0]);
          train.time = video ?
            video.videoDuration * train.repeatsDetails.repeats :
            2 * train.repeatsDetails.repeats;
          if (train.repeats.match(/мин/gi)) {
            train.repeatsDetails = {};
            train.time = parseInt(train.repeats.split('-')[0]);
          }
        } else {
          // console.log("3", train.repeats);
          train.repeatsDetails.repeats = parseInt(train.repeats.split(/\s/)[0]);
          train.time = video ?
            video.videoDuration * train.repeatsDetails.repeats :
            2 * train.repeatsDetails.repeats;
          if (train.repeats.match(/сек/g)) {
            train.repeatsDetails = {};
            train.time = parseInt(train.repeats.split(' ')[0]);
          }
        }

        if (description != null) {
          train.position = description.position;
          train.breathing = description.breathing;
          train.mainMuscles = description.mainMuscles;
          train.mainAccents = description.mainAccents;
          train.additionalInfo = description.additionalInfo;
          train.additionalMuscles = description.additionalMuscles;
        }

        callback(train);
      });
  });
};
