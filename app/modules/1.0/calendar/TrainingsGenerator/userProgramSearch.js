const jwt = require('jsonwebtoken');
const fs = require('fs');
const _ = require('underscore');
const cert = fs.readFileSync(`${process.env.KeysPath}series.key`, 'utf-8');
const DB = require(`${process.env.Core}db.js`);

module.exports = function (userLevel, user) {
  let yo = new Date(Date.now() - Date.parse(user.bdate));
  let ageDate = new Date(yo);
  const years = Math.abs(ageDate.getUTCFullYear() - 1970);
  console.log(years);

  // Timeouts based on userLevel
  if (userLevel == 0) {
  	exerciseTimeout = 120;
  	approachTimeout = 90;
  } else if (userLevel == 1) {
  	exerciseTimeout = 120;
  	approachTimeout = 60;
  } else if (userLevel == 2) {
  	exerciseTimeout = 90;
  	approachTimeout = 60;
  }

  // Basic trainings search Object
  let search = {
  	years: '',
  	weight: '',
  	height: '',
  	level: parseInt(userLevel)
  };

  // Search filling
  // YO
  if (years <= 29)
  	search.years = {
  		to: 29
  	};
  else if (years >= 30 && years <= 45)
  	search.years = {
  		from: 30,
  		to: 45
  	};
  else if (years > 45)
  	search.years = {
  		from: 45
  	};
  // WEIGHT
  if (parseInt(user.weight) < 55)
  	search.weight = {
  		to: 55
  	};
  else if (parseInt(user.weight) >= 55 && parseInt(user.weight) <= 70)
  	search.weight = {
  		from: 55,
  		to: 70
  	};
  else if (parseInt(user.weight) > 70)
  	search.weight = {
  		from: 70
  	};
  //HEIGHT
  if (parseInt(user.height) < 170)
  	search.height = {
  		to: 170
  	};
  else if (parseInt(user.height) >= 170)
  	search.height = {
  		from: 170
  	};

  console.log(search);

  return search;
};
