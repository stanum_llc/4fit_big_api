const jwt = require('jsonwebtoken');
const fs = require('fs');
const _ = require('underscore');
const cert = fs.readFileSync(`${process.env.KeysPath}series.key`, 'utf-8');
const DB = require(`${process.env.Core}db.js`);

const Enc = require(`${process.env.Core}encryptor.js`);
const Encryptor = new Enc('userData');
const getTrainData = require('./getTrainData.js');

let trainingRandom = 0;

module.exports = function(variables, data, callback) {
  const purchaseId = variables.purchaseId;
  const id = variables.id;
  const userLevel = variables.userLevel;
  const tPlace = variables.tPlace;

  const startDate = data.startDate;
  const day = data.day;
  const dayIndex = data.dayIndex;
  const weekIndex = data.weekIndex;
  const exerciseTimeout = data.exerciseTimeout;
  const pull = data.pull;
  const place = data.place;

  let pullsObject = {};
  // Processing pulls from array to object with uniq names without spaces etc.
  pull.map((p, pi) => {
    let name = p.name.replace(/\n/, '').replace(/ /g, '').toLowerCase();
    pullsObject[name] = p;
  });
  let date = startDate + (1000 * 60 * 60 * (24 * (dayIndex + weekIndex * 7)));
  let dayName = day.name;
  let dayTrain = {
    purchaseId: purchaseId,
    userId: id,
    day: dayIndex,
    week: weekIndex,
    name: dayName,
    done: 0,
    date: date,
    time: 0,
    list: [],
    type: 'training',
  };

  if (dayTrain.name.match(/выходной/gi)) {
    dayTrain.type = 'day-off';
    dayTrain.name = 'Выходной';
    dayTrain.done = 1;
    callback(dayTrain, 0);
  } else {
    // Approximately training duration
    // dayTrain.time = day.list.length * exerciseTimeout;

    let dayTrainingTime = day.list.length * exerciseTimeout + 120;

    dayTrain.durationTime = dayTrainingTime;

    day.list.map((pN, pullIndex) => {
      // current pull uniq name
      pullName = pN.replace(/\n/g, '').replace(/ /g, '').toLowerCase();
      let p = pullsObject[pullName]; //pull by name

      // if system has this pull
      if (p) {
        p = JSON.parse(JSON.stringify(p));
        try {

          p.data[place] = _.shuffle(p.data[place]);
          generateRandom(p.data[place].length, (index) => {
            p.data[place] = _.shuffle(p.data[place]);

            let train = p.data[place][index];

            train.number = pullIndex;

            // if got this training
            if (train) {
              // Temporarry empty video links
              let name = p.data[place][index].name.replace(/\n/, '').replace(/ /g, '').toLowerCase();
              getTrainData({tPlace: tPlace, id:id}, train, name, (trainWithData) => {

                dayTrain.durationTime = dayTrain.durationTime ?
                      dayTrain.durationTime + parseInt(trainWithData.time) :
                      parseInt(trainWithData.time) + 1200;

                if (!dayTrain.durationTime)
                  dayTrain.durationTime = 2200;

                dayTrain.list.push(trainWithData);
                // if (dayTrain.durationTime == null)
                delete index;
                callback(dayTrain, day.list.length);
              });
            } else {
              console.log(p.data[place].length, index);
            }
          });
        } catch (e) {
          console.log(e);
          console.log(pullName, place);
          console.log('main', dayIndex + ' / ' + weekIndex + ' / ' + place);
        }
      } else {
        const pullSearch = {
          search: userLevel + '_' + pullName
        };
        DB.find('pull', pullSearch, (warmupPull) => {
          if (warmupPull) {
            generateRandom(warmupPull.data[place].length, (index) => {
              if (warmupPull.data[place].length == 0) {
                console.log('warmup', pullSearch);
              }
              let train = warmupPull.data[place][index];
              // if got this training
              if (train) {
                train.number = pullIndex;
                // Temporarry empty video links
                let name = warmupPull.data[place][index].name.replace(/\n/, '').replace(/ /g, '').toLowerCase();
                try {
                  getTrainData({tPlace: tPlace, id:id}, train, name, (trainWithData) => {
                    dayTrain.durationTime += parseInt(trainWithData.time);
                    if (warmupPull.name.match(/аминка/))
                      trainWithData.type = 'warmdown';
                    else if (warmupPull.name.match(/азминка/))
                      trainWithData.type = 'warmup';

                    dayTrain.list.push(trainWithData);
                    callback(dayTrain, day.list.length);
                  });
                } catch (e) {
                  console.log(e);
                }
              } else {
                console.log(pullSearch, place, index);
              }
            });
          } else {
            console.log('Pull ' + pullSearch.search + ' not found.', '\n' + dayIndex + '\t' + weekIndex);
          }
        });
      }
    });
  }
};

function generateRandom(max, callback) {
  // console.log(max);
  let rand = -1;
  if (max >= 2) {
    while (rand == -1 || rand == trainingRandom) {
      rand = Math.floor(Math.random() * max + 0);
      if (rand != -1 && rand != trainingRandom) {
        callback(rand);
      }
    }
  } else {
    callback(Math.floor(Math.random() * max + 0));
  }
}
