const jwt = require('jsonwebtoken');

module.exports = function(data, query, callback) {
  if (this.userToken) {
    const _id = jwt.decode(this.userToken)._id;
    let search = {enabled: 1};
    if (query.type)
      search = {enabled: 1, type: query.type};

    this.DBCore.findAll('purchases', search, (res)=> {
      this.DBCore.find('users', {_id: _id}, (user)=> {
        user = this.Encryptor.decryptMany(user);
        if (res.length == 0 || res == null) {
          callback({code: 404, body: {message: 'Purchases not found. Please, update your token'}});
        } else {
          let result = JSON.parse(JSON.stringify(res));
          result.forEach((e,i)=> {
            this.DBCore.find('userPurchases', {userId: _id, purchaseId: e._id}, (res)=> {
              res == null ? result[i].status = 1 : result[i].status = 0;

              // if (e.type == 1)
              //   result[i].price = [e.price[parseInt(user.userLevel)]];
              // let level = 0;
              // if (purchase.android_id == 'program_beginer')
              //   level = 0;
              // if (purchase.android_id == 'program_middle')
              //   level = 1;
              // if (purchase.android_id == 'program_professional')
              //   level = 2;

              if (i == result.length - 1)
                callback({code: 200, body: result});
            });
          });
        }
      });
    });
  } else {
    callback({code: 401, body: {message: 'Unauthorized. Please send access token'}});
  }
};
