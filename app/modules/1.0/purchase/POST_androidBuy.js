const fs = require('fs');
const jwt = require('jsonwebtoken');
let cp = require('child_process');

module.exports = function(data, callback) {
    const fillCalendarPath = `${process.env.RootPath}modules/${this.version}/calendar/fillForProgram.js`;
    const fillNutritionPath = `${process.env.RootPath}modules/${this.version}/calendar/fillNutrition.js`;
    const calendarUpdater = require(`${process.env.RootPath}modules/${this.version}/calendar/updater.js`);
    const fillPregnantPath = `${process.env.RootPath}modules/${this.version}/calendar/fillPregnantProgram.js`;
    const user = require(`${process.env.RootPath}modules/${this.version}/users/user.js`);
    const Firebase = require(`${process.env.Core}firebase.js`);
    const assistantMessage = require(`${process.env.RootPath}modules/${this.version}/assistant/writeMessage.js`);
    const googlePlayCheck = require(`./googlePlayCheck.js`);

    // console.log(data);
    // googlePlayCheck(data.productId, data.storeToken);
    if (this.userToken) {
      user(this.userToken, (user) => {
        if (user) {
          const userId = jwt.decode(this.userToken)._id;

          const android_id = data.android_id ? data.android_id : null;
          const apple_id = data.apple_id ? data.apple_id : null;

          const date = new Date();
          const today = date.getFullYear() + '.' + (date.getMonth() + 1) + '.' + date.getDate();
          let time = new Date().getTime();
          let tomorrow = new Date(time + 24 * 60 * 60 * 1000).toISOString().slice(0, 10);

          purchaseSearch = {
            android_id: data.productId
          };

          if (data.target)
            purchaseSearch.target = parseInt(data.target);

          if (data.weekOfPregnancy)
            this.DBCore.update('users', {_id: userId}, {weekOfPregnancy: data.weekOfPregnancy}, (res)=> {});

          this.DBCore.find('purchases', purchaseSearch, (res) => {
            if (res != null && data.productId) {
              global.sysLog(res.type, 'log');

              let purchase = {
                userId: userId,
                weight: user.weight,
                status: 1,
                date: today,
                type: res.type,
                name: res.name,
                label: res.label,
                description: res.shortDescrition,
                subscription: res.subscription,
                purchaseId: res.pid,
                target: user.target,
                android_id: data.productId,
                details: res.details,
                image: res.image
              };

              calendarUpdater(userId, {purchase: purchase, type: 'purchase'}, ()=> {});

              let update = {};

              if (res.type == 'trainings' || res.type == 'pregnant')
                update = {
                  status: 0,
                  endDate: new Date().getTime()
                };
              else if (res.type == 'series')
                purchase.status = 0;

              this.DBCore.updateAll('userPurchases', {
                userId: userId,
                status: 1,
              }, update, (result) => {
                this.DBCore.insert('userPurchases', purchase, (response) => {
                  if (purchase.type == 'trainings') {
                    try {
                      if (!data.target)
                        callback({
                          code: 404,
                          body: {
                            message: 'Target not found'
                          }
                        });
                      else
                        this.DBCore.update('users', {
                          _id: userId
                        }, {
                          target: data.target
                        }, (res) => {
                          let level = 0;

                          purchase.target = data.target;

                          if (data.productId == 'beginner_monthly')
                            level = 0;
                          else if (data.productId == 'middle_monthly')
                            level = 1;
                          else if (data.productId == 'professional_monthly')
                            level = 2;

                          let fillCalendar = cp.fork(fillCalendarPath);
                          let insertedId = response.insertedIds['0'];

                          fillCalendar.send({
                            Id: userId,
                            purchaseId: insertedId,
                            level: level
                          });

                          fillCalendar.on('message', (response) => {
                            fillCalendar.kill('SIGINT');
                            if (response.code != 200)
                              callback(response);
                            global.sysLog(fillCalendar.killed ?
                              'child_process fillForProgram is killed' :
                              'child_process fillForProgram is still here', 'log');

                            const fillNutrition = cp.fork(fillNutritionPath);

                            fillNutrition.send({
                              Id: userId,
                              purchaseId: insertedId,
                              level: level
                            });

                            fillNutrition.on('message', (response) => {
                              // global.sysLog(response, 'log');
                              fillNutrition.kill('SIGINT');
                              if (response.code != 200)
                                callback(response);

                              global.sysLog(fillNutrition.killed ?
                                'child_process fillNutrition is killed' :
                                'child_process fillNutrition is still here', 'log');

                              let endDate = Date.now();
                              endDate += (1000 * 60 * 60 * 24 * 27);
                              endDate = new Date(endDate);
                              endDate = [
                                endDate.getDate() < 10 ? '0' + endDate.getDate() : endDate.getDate(),
                                endDate.getMonth() + 1 < 10 ? '0' + (endDate.getMonth() + 1) : endDate.getMonth() + 1,
                                endDate.getFullYear()
                              ];

                              endDate = endDate.join('.');

                              // console.log(endDate);

                              this.DBCore.find('assistant', {alias: 'purchase'}, (message)=>{
                                let training = {
                                  name: purchase.label,
                                  endDate: endDate
                                }
                                let msg = JSON.parse(JSON.stringify(message));
                                let text = 'msg.text = `' + msg.text[0] + '`';
                                eval(text);

                                if (user.firebase) {
                                  setTimeout(()=> {
                                    let message = {
                                      to: user.firebase,
                                      notification: {
                                        title: '4 Fitness Girls',
                                        body: msg.text
                                      }
                                    };

                                    Firebase.sendNotification(message);
                                  }, 10000);
                                }

                                assistantMessage(this.DBCore, {
                                  uid: user._id,
                                  message: msg.text,
                                  type: msg.type,
                                  action: msg.action
                                });
                              });

                              try {
                                callback({
                                  code: 200,
                                  body: {
                                    message: 'DONE'
                                  }
                                });
                              } catch (e) {

                              }
                            });
                          });
                        });
                    } catch (e) {
                      console.log(e);
                    }
                  } else if (purchase.type == 'pregnant') {
                    let insertedId = response.insertedIds['0'];
                    let fillPregnant = cp.fork(fillPregnantPath);

                    this.DBCore.update('users', {
                      _id: userId
                    }, {
                      userLevel: 3
                    }, (res) => {
                      fillPregnant.send({
                        Id: userId,
                        purchaseId: insertedId,
                        level: 3
                      });

                      fillPregnant.on('message', (response) => {
                        fillPregnant.kill('SIGINT');
                        if (response.code != 200)
                          callback(response);

                        global.sysLog((fillPregnant.killed ?
                          'child_process fillPregnant is killed' :
                          'child_process fillPregnant is still here'), 'log');
                        // try {
                        //   // callback({
                        //   //   code: 200,
                        //   //   body: {
                        //   //     message: 'DONE'
                        //   //   }
                        //   // });
                        // } catch (e) {
                        //
                        // }

                        const fillNutrition = cp.fork(fillNutritionPath);

                        fillNutrition.send({
                          Id: userId,
                          purchaseId: insertedId,
                          level: 3
                        });

                        fillNutrition.on('message', (response) => {
                          fillNutrition.kill('SIGINT');
                          if (response.code != 200)
                            callback(response);

                          global.sysLog((fillNutrition.killed ?
                            'child_process fillNutrition is killed' :
                            'child_process fillNutrition is still here'), 'log');

                          let endDate = Date.now();
                          endDate += (1000 * 60 * 60 * 24 * 29);
                          endDate = new Date(endDate);

                          endDate = [
                            endDate.getDate() < 10 ? '0' + endDate.getDate() : endDate.getDate(),
                            endDate.getMonth() + 1 < 10 ? '0' + (endDate.getMonth() + 1) : endDate.getMonth() + 1,
                            endDate.getFullYear()
                          ];

                          endDate = endDate.join('.');

                          this.DBCore.find('assistant', {alias: 'purchase'}, (message)=>{
                            let training = {
                              name: purchase.label,
                              endDate: endDate
                            }
                            let msg = JSON.parse(JSON.stringify(message));
                            let text = 'msg.text = `' + msg.text[0] + '`';
                            eval(text);

                            if (user.firebase) {
                              setTimeout(()=> {
                                let message = {
                                  to: user.firebase,
                                  notification: {
                                    title: '4 Fitness Girls',
                                    body: msg.text
                                  }
                                };

                                Firebase.sendNotification(message);
                              }, 10000);
                            }

                            assistantMessage(this.DBCore, {
                              uid: user._id,
                              message: msg.text,
                              type: msg.type,
                              action: msg.action
                            });
                          });

                          try {
                            callback({
                              code: 200,
                              body: {
                                message: 'DONE'
                              }
                            });
                          } catch (e) {

                          }
                        });
                      });
                    });
                  } else {
                    callback({
                      code: 200,
                      body: {
                        message: 'Purchased successfully'
                      }
                    });
                  }
                });
              });
            } else {
              callback({
                code: 404,
                body: {
                  message: 'Product not found'
                }
              });
            }
          });
        } else {
          callback({
            code: 404,
            body: {
              message: 'User not found. Please update your access token'
            }
          });
        }
      });
    } else {
      callback({
        code: 401,
        body: {
          message: 'Unauthorized. Please send access token'
        }
      });
    }
  };
