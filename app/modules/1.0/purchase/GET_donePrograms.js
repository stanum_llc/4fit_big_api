const jwt = require('jsonwebtoken');

module.exports = function(data, query, callback) {
  if (this.userToken) {
    const _id = jwt.decode(this.userToken)._id;

    let search = {userId: _id, type: {$in: ['trainings','pregnant']}, status: {$in: [0,2]}};

    this.DBCore.findAll('userPurchases', search, (res)=> {
      if (res == null) {
        callback({code: 404, body: {message: 'Purchases not found. Please, update your token'}});
      } else {
        let result = JSON.parse(JSON.stringify(res));
        delete result._id;
        callback({code: 200, body: result});
      }
    });
  } else {
    callback({code: 401, body: {message: 'Unauthorized. Please send access token'}});
  }
};
