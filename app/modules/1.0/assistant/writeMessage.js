const jwt = require('jsonwebtoken');
const mongodb = require('mongodb');
const fs = require('fs');

// const DB = require(`${process.env.Core}db.js`);
const cert = fs.readFileSync(`${process.env.KeysPath}token.key`, 'utf-8');
let DB;

const Enc = require(`${process.env.Core}encryptor.js`);
const Encryptor = new Enc('userData');

let message = {
  'userId': '',
  'message': 'message text',
  'user': 1,
  'date': 'timestamp',
  'show': true,
  'type': 'weight/transference/water/nutrition/text',
  'controls': []
};

module.exports = function(database, data, callback) {
  DB = database;
  DB.find('users', {_id: data.uid}, (user)=> {
    if (user) {
      user = Encryptor.decryptMany(user);
      message.type = data.type;
      message.date = new Date().getTime();
      message.message = data.message,
      message.user = 0;
      message.userId = user._id.toString();
      message.controls = [];

      if (data.type == 'weight') {
        message.controls = [
          {
            'type': 'input',
            'value': user.weight,
            'url': process.env.APIURL + 'api/users/updateInfo',
            'request': 'PUT'
          },
          {
            'type': 'button',
            'value': 'Send',
            'url': process.env.APIURL + 'api/users/updateInfo',
            'request': 'PUT'
          }
        ];
      } else if (data.type == 'transference') {
        message.controls = [
          {
            'type': 'button',
            'value': 'Postpone to the next day',
            'url': process.env.APIURL + 'api/training/deferDay',
            'request': 'POST'
          },
          {
            'type': 'button',
            'value': 'OK',
            'url': '',
            'request': ''
          }
        ];
      } else if (data.type == 'water') {
        message.controls = [
          {
            'type': 'button',
            'value': 'Mark water intakes',
            'url': process.env.APIURL + 'api/nutrition/waterBalance',
            'request': 'PUT'
          },
          {
            'type': 'button',
            'value': 'Postpone',
            'url': '',
            'request': ''
          }
        ];
      } else if (data.type == 'nutrition') {
        message.controls = [
          {
            'type': 'button',
            'value': 'Mark meals',
            'url': process.env.APIURL + 'api/nutrition/checkMeal',
            'request': 'PUT'
          },
          {
            'type': 'button',
            'value': 'Postpone',
            'url': '',
            'request': ''
          }
        ];
      }
      else if (data.type == 'lostUser') {
        message.controls = [
          {
            'type': 'button',
            'value': 'Can\'t train right now',
            'url': '',
            'request': 'POST'
          },
          {
            'type': 'button',
            'value': 'Report a problem',
            'url': '',
            'request': 'POST'
          },
          {
            'type': 'button',
            'value': 'Too hard for me',
            'url': '',
            'request': 'POST'
          }
        ];
      }
      saveMessage(message);
    }
  });
};

function saveMessage(message) {
  let m = JSON.parse(JSON.stringify(message));

  m._id = jwt.sign(m.userId + '-' + m.date, cert);
  m._id = m._id.slice(m._id.length - 13, m._id.length - 1).toString();
  m._id = new mongodb.ObjectId(m._id);
  DB.insert('assistantMessages', m, (res)=> {
    // DB.disconnect();
    if (res && res.code == 11000) {
      saveMessage(message);
    } else {
      // console.log('Message sent ', m.message);
    }
  });
}
