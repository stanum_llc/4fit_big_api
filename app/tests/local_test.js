class Test {
  constructor() {
    this.userAccessToken = '';

    this.fb_token = 'EAAcZB7UtdZChwBAAZBntmRMRwarvnroOa0QvKvS5ZAzFCVKwOHoZCz2EQXtWHIZCfz8dgB7wpp1tAbQnWs9ZCUHYgUcxfH23F9dgY5u1z8o6vTIaUILkrIZCycxYDv48tYclnRFj9WxCLk9M43ZA1HIz5o0RB0nTD4lKi7WCn9BLM08v24pBiZAyB21wgyl2F6pyjBj4Jql1tqtgZDZD';

    this.vk_token = '8f5ee950fb8927fed3e5784820a13b806400d19a8a7704dfbd2d1e5aceede4c988ce69d9c8d0132f76441';

    this.wrongToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YTQyNDVjNmMzMzQyOTdmNmQzMmQ5MWMiLCJpYXQiOjE1MTQyMTQ4MDZ9.iCj1hamVlEj22JdsboQj0-JvXe10BMPnUS7t5aObJTA';

    this.server = 'http://localhost:3000';
    let arr = [];

    for (let i = 0; i < process.env.TEST_COUNT; i++) {
      arr[i] = i;
    }
    // for (let index = 0; index < process.env.TEST_COUNT; index++) {
    // arr.map((e,index)=> {

      this.user = {
        email: 'dvotintsev@envionsoftware.com',
        password: '123456789',
        name: 'test',
        surname: '_'
      };

      // Testing functions
      this.registration = require(__dirname + '/user/registration.js');
      this.mergeSocialAccounts = require(__dirname + '/user/mergeSocialAccounts.js');
      this.authenticate = require(__dirname + '/user/authenticate.js');
      this.updateInfo = require(__dirname + '/user/updateInfo.js');
      this.getUserInfo = require(__dirname + '/user/getUserInfo.js');
      // this.purchasesList = require(__dirname + '/purchase/list.js');

      // Run testing
      this.registration(this);
      // this.mergeSocialAccounts(this);
      this.authenticate(this);
      this.updateInfo(this);
      this.getUserInfo(this);
      // this.purchasesList(this);
    // });
  }
}

const _test = new Test();
