class Test {
  constructor() {
    this.userAccessToken = '';

    this.fb_token = 'EAACEdEose0cBAKMTOwSYKBnt3OgA6jD5o0f9wYMjT54ahPt1Ln03vRV6Dv5s2bZB07o1rirhaSYSc4YTNZAAxKgZCVOhXGEHO1KPgJvZCdoFh4E44YnmbXWr5F92wrA60fjSaYzumI3LcIOKMz4VmYuVQx7OZAFVAnWDHh9Ntgnkj57mXZAb6vScZBgVPVgxxwZD';

    this.vk_token = '8f5ee950fb8927fed3e5784820a13b806400d19a8a7704dfbd2d1e5aceede4c988ce69d9c8d0132f76441';

    this.wrongToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YTQyNDVjNmMzMzQyOTdmNmQzMmQ5MWMiLCJpYXQiOjE1MTQyMTQ4MDZ9.iCj1hamVlEj22JdsboQj0-JvXe10BMPnUS7t5aObJTA';

    this.server = 'http://api.4fitnessgirls.com:4000';

    this.user = {
      email: 'dvotintsev@envionsoftware.com',
      password: '123456789',
      name: 'test',
      surname: '_'
    };

    // Testing functions
    this.registration = require(__dirname + '/user/registration.js');
    this.mergeSocialAccounts = require(__dirname + '/user/mergeSocialAccounts.js');
    this.authenticate = require(__dirname + '/user/authenticate.js');
    this.updateInfo = require(__dirname + '/user/updateInfo.js');
    this.getUserInfo = require(__dirname + '/user/getUserInfo.js');

    // Run testing
    this.registration(this.server);
    this.mergeSocialAccounts(this.server);
    this.authenticate(this.server);
    this.updateInfo(this.server);
    this.getUserInfo(this.server);
  }
}

const _test = new Test();
