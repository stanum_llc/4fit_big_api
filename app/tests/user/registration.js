const fs = require('fs');
const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;

chai.use(chaiHttp);

module.exports = function(data) {
  const self = this;
  describe('Registration', function() {
    it('Registration success', function(done) {
      chai.request(data.server)
        .post('/API/users/registration')
        // .attach('image', fs.readFileSync(__dirname + '/image.png'), 'image.png')
        // .field('email', 'dvotintsev@envionsoftware.com')
        // .field('password', '123456789')
        // .field('name', 'Daniel')
        // .field('surname', 'Votintsev')
        .send(data.user)
        .end(function(err, res) {
          self.userAccessToken = res.body.token;
          expect(res).to.have.status(201);
          expect(res.body.token).to.be.string;
          done();
        });
    });
    // it('User already exist', function(done) {
    //   chai.request(server)
    //     .post('/API/users/registration')
    //     .send({
    //       email: 'test_account@test.com',
    //       password: '123456789',
    //       name: 'John',
    //       surname: 'Dou'
    //     })
    //     .end(function(err, res) {
    //       expect(res).to.have.status(409);
    //       expect(res.body.message).to.be.string;
    //       done();
    //     });
    // });
  });
};
