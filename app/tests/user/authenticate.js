const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;

chai.use(chaiHttp);

module.exports = function(data) {
  const self = this;
  describe('Authenticate', function() {
    it('Authenticated', function(done) {
      chai.request(data.server)
        .post('/API/users/authenticate')
        .send({
          email: 'dvotintsev@envionsoftware.com',
          password: '123456789'
        })
        .end(function(err, res) {
          expect(res).to.have.status(200);
          expect(res.body.token).to.be.string;
          done();
        });
    });
    it('User not found', function(done) {
      chai.request(data.server)
        .post('/API/users/authenticate')
        .send({
          email: '__testU',
          password: '!±?'
        })
        .end(function(err, res) {
          expect(res).to.have.status(404);
          expect(res.body.token).to.be.string;
          done();
        });
    });
    describe('Social authenticate', function() {
      it('Facebook auth OK', function(done) {
        chai.request(data.server)
          .post('/API/users/authenticateSocial')
          .send({
            accessToken: self.fb_token,
            social: 'fb'
          })
          .end(function(err, res) {
            expect(res).to.have.status(200);
            expect(res.body.token).to.be.string;
            done();
          });
      });
      it('VK auth OK', function(done) {
        chai.request(data.server)
          .post('/API/users/authenticateSocial')
          .send({
            accessToken: self.vk_token,
            social: 'vk'
          })
          .end(function(err, res) {
            expect(res).to.have.status(200);
            expect(res.body.token).to.be.string;
            done();
          });
      });
    });
  });
};
