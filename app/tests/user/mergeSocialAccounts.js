const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;

chai.use(chaiHttp);
module.exports = function(data) {
  const self = this;
  describe('Merge social accounts', function() {

    describe('VK', function() {
      it('Add VK', function(done) {
        chai.request(data.server)
          .post('/API/users/mergeSocialAccounts')
          .set('token', self.userAccessToken)
          .send({
            accessToken: self.vk_token,
            social: 'vk'
          })
          .end(function(err, res) {
            expect(res).to.have.status(200);
            expect(res.body.message).to.be.string;
            done();
          });
      });
      it('System already have account with this social Id', function(done) {
        chai.request(data.server)
          .post('/API/users/mergeSocialAccounts')
          .set('token', self.userAccessToken)
          .send({
            accessToken: self.vk_token,
            social: 'vk'
          })
          .end(function(err, res) {
            expect(res).to.have.status(409);
            expect(res.body.message).to.be.string;
            done();
          });
      });
    });
    describe('Facebook', function() {
      it('Add Facebook', function(done) {
        chai.request(data.data.server)
          .post('/API/users/mergeSocialAccounts')
          .set('token', self.userAccessToken)
          .send({
            accessToken: self.fb_token,
            social: 'fb'
          })
          .end(function(err, res) {
            expect(res).to.have.status(200);
            expect(res.body.message).to.be.string;
            done();
          });
      });
      it('System already have account with this social Id', function(done) {
        chai.request(data.server)
          .post('/API/users/mergeSocialAccounts')
          .set('token', self.userAccessToken)
          .send({
            accessToken: self.fb_token,
            social: 'fb'
          })
          .end(function(err, res) {
            expect(res).to.have.status(409);
            expect(res.body.message).to.be.string;
            done();
          });
      });
    });
    it('Wrong user token', function(done) {
      chai.request(data.server)
        .post('/API/users/mergeSocialAccounts')
        .set('token', self.wrongToken)
        .send({
          accessToken: self.vk_token,
          social: 'vk'
        })
        .end(function(err, res) {
          process.env.userAccessToken = res.body.token;
          expect(res).to.have.status(401);
          expect(res.body.message).to.be.string;
          done();
        });
    });
  });
};
