const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;

chai.use(chaiHttp);

module.exports = function(data) {
  const self = this;
  describe('Get user info', function() {
    it('OK', function(done) {
      chai.request(data.server)
        .get('/API/users/information')
        .set('token', self.userAccessToken)
        .send()
        .end(function(err, res) {
          expect(res).to.have.status(200);
          expect(res.body.token).to.be.string;
          done();
        });
    });
    it('User not found', function(done) {
      chai.request(data.server)
        .get('/API/users/information')
        .set('token', self.wrongToken)
        .send()
        .end(function(err, res) {
          expect(res).to.have.status(404);
          expect(res.body.token).to.be.string;
          done();
        });
    });
    it('Unauthorized. Please send access token.', function(done) {
      chai.request(data.server)
        .get('/API/users/information')
        .send()
        .end(function(err, res) {
          expect(res).to.have.status(401);
          expect(res.body.token).to.be.string;
          done();
        });
    });
  });
};
