const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
const randKey = require('randomstring');

chai.use(chaiHttp);

module.exports = function(data) {
  const self = this;
  describe('Update user information', function() {
    it('Update success', function(done) {
      chai.request(data.server)
        .put('/API/users/updateInfo')
        .set('token', self.userAccessToken)
        .send({
          weight: '80',
          height: '180',
          bdate: '12.30.1996',
          userLevel: 1,
          trainingPlace: 1,
          target: 1
        })
        .end(function(err, res) {
          expect(res).to.have.status(200);
          expect(res.body.token).to.be.string;
          done();
        });
    });
    it('User not found', function(done) {
      chai.request(data.server)
        .put('/API/users/updateInfo')
        .set('token', self.wrongToken)
        .send({
          weight: '80',
          height: '180',
          bdate: '12.30.1996',
          userLevel: 1,
          trainingPlace: 1,
          target: 1
        })
        .end(function(err, res) {
          expect(res).to.have.status(404);
          expect(res.body.token).to.be.string;
          done();
        });
    });
    it('Unauthorized. Please send access token', function(done) {
      chai.request(data.server)
        .put('/API/users/updateInfo')
        .send({
          weight: '80',
          height: '180',
          bdate: '12.30.1996',
          userLevel: 1,
          trainingPlace: 1,
          target: 1
        })
        .end(function(err, res) {
          expect(res).to.have.status(401);
          expect(res.body.token).to.be.string;
          done();
        });
    });
  });
};
