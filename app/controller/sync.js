class Assistant {
  constructor(token, httpMethod, method, version) {
    this.DBCore = require('../core/db.js');

    this.httpMethod = httpMethod;
    this.method = method;
    this.version = version ? version : process.env.API_VERSION;

    if (token)
      this.userToken = token;

    const fileName = httpMethod + '_' + method + '.js';
    // const path = process.env.RootPath + 'modules/sync/' + fileName;

    const path = `${process.env.RootPath}modules/${this.version}/sync/${fileName}`;
    this[httpMethod + '' + method] = require(path);
  }
  disconnect() {
    this.DBCore.disconnect();
  }
}

module.exports = Assistant;
