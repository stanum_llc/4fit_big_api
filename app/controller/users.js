class User {
  constructor(token, httpMethod, method, version) {
    this.DBCore = require('../core/db.js');
    const Encryptor = require('../core/encryptor.js');
    this.Encryptor = new Encryptor('userData');
    this.Social = require('../core/social.js');
    this.httpMethod = httpMethod;
    this.method = method;
    this.version = version ? version : process.env.API_VERSION;

    if (token)
      this.userToken = token;

    this.userModel = {
      email: null,
      name: null,
      surname: null,
      fb_id: null,
      inst_id: null,
      vk_id: null,
      weight: 0,
      height: 0,
      bdate: null,
      userLevel: null,
      trainingPlace: '0',
      target: '0',
      password: null,
      email: null,
      image: null,
      notifications: [1,1,1,1]
    };

    const fileName = httpMethod + '_' + method + '.js';
    // const path = process.env.RootPath + 'modules/' + version + '/users/' + fileName;
    const path = `${process.env.RootPath}modules/${this.version}/users/${fileName}`;

    this[httpMethod + '' + method] = require(path);
  }
  disconnect() {
    this.DBCore.disconnect();
  }
}

module.exports = User;
