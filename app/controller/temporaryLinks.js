const fs = require('fs');

class TemporaryLinks {
  constructor(type, token, quality=undefined) {
    this.token = token;
    this.type = type;
    this.quality = quality ? parseInt(quality) : undefined;
    this.DBCore = require('../core/db.js');
    this.jwt = require('jsonwebtoken');
  }
  disconnect() {
    this.DBCore.disconnect();
  }
  getMedia(callback) {
    // console.log('_', this.token);
    if (this.token.indexOf('.jpg') > 0)
      this.token = this.token.substring(0, this.token.indexOf('.jpg'));

    let file = this.jwt.decode(this.token);
    let path = '';
    if (this.type == 'series') {

      if (file.type == 'image') {
        path = process.env.PrivatePath;
        path += 'series/season_' + file.season + '/' + file.name;
        path += '.jpg';
      } else if (file.type == 'video') {
        path = process.env.VideosPath;
        // TODO: Return this line to show chosed series
        path += '' + file.season + '/';
        // path += '1/'; // REMOVE this and next few lines
        if (file.name == 'teaser')
          path += file.name;
        else
          path += file.series;

        path += '.mp4';
      }

    } else if (this.type == 'trainings') {

      let place = file.place == '0' ? 'nastya' :
      file.place == '1' ? 'olya' :
      file.place == '2' ? 'sveta' : 'natasha';

      if (this.quality)
        path = place + '/' + this.quality + '/' + file.name + '.mp4';
      else {
        // // TODO: Remove this path to work with all videos
        // if (parseInt(file.name) > 70)
        //   file.name = parseInt(Math.random() * (70 - 1) + 1);

        path = place + '/short/' + file.name + '.mp4';

        let filePath = `${process.env.VideosPath}${path}`;
        if (!fs.existsSync(filePath)) {
          console.log('Not found', filePath);
          path = process.env.PublicPath + 'emptyVideo.mp4';
        }
      }
    }

    callback({code: 200, body: {url: path}});
  }
}

module.exports = TemporaryLinks;
