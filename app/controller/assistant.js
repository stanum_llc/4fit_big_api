class Assistant {
  constructor(token, httpMethod, method, version) {
    this.DBCore = require('../core/db.js');

    this.httpMethod = httpMethod;
    this.method = method;
    this.version = version ? version : process.env.API_VERSION;

    if (token)
      this.userToken = token;

    const fileName = httpMethod + '_' + method + '.js';
    // const path = process.env.RootPath + 'modules/assistant/' + fileName;
    const path = `${process.env.RootPath}modules/${this.version}/assistant/${fileName}`;
    this[httpMethod + '' + method] = require(path);
  }
  disconnect() {
    this.DBCore.disconnect();
  }
}

module.exports = Assistant;
