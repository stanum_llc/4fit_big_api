class Calendar {
  constructor(token, httpMethod, method, version) {
    this.DBCore = require('../core/db.js');

    const Enc = require('../core/encryptor.js');
    this.Encryptor = new Enc('userData');

    this.httpMethod = httpMethod;
    this.method = method;
    this.version = version ? version : process.env.API_VERSION;

    if (token)
      this.userToken = token;

    const fileName = httpMethod + '_' + method + '.js';
    // const path = process.env.RootPath + 'modules/calendar/' + fileName;
    const path = `${process.env.RootPath}modules/${this.version}/calendar/${fileName}`;

    this[httpMethod + '' + method] = require(path);
  }
  disconnect() {
    this.DBCore.disconnect();
  }
}

module.exports = Calendar;
