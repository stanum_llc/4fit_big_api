class Purchase {
  constructor(token, httpMethod, method, version) {

    const Enc = require('../core/encryptor.js');
    this.Encryptor = new Enc('userData');

    this.httpMethod = httpMethod;
    this.method = method;
    this.version = version ? version : process.env.API_VERSION;

    if (token)
      this.userToken = token;

    const fileName = httpMethod + '_' + method + '.js';
    // const path = process.env.RootPath + 'modules/purchase/' + fileName;
    const path = `${process.env.RootPath}modules/${this.version}/purchase/${fileName}`;
    this.DBCore = require('../core/db.js');
    this[httpMethod + '' + method] = require(path);
  }
  disconnect() {
    this.DBCore.disconnect();
  }
}

module.exports = Purchase;
