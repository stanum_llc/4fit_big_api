const nodemailer = require('nodemailer');

class Mailer {
  constructor(email) {
    this.to = email;
  }
  send(type, data) {
    this.createTransporter((transporter) => {
      let mailOptions = {
        from: '"4 Fitness Girls" <noreply.4fitnessgirls@gmail.com>',
        to: this.to,
        subject: data.subject ? data.subject : '',
        text: data.text ? data.text : '',
        html: data.html ? data.html : ''
      };

      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          return console.log(error);
        }
        try {
          // require('openurl').open(nodemailer.getTestMessageUrl(info));
        } catch (e) {
          // global.sysLog('No openurl module.', 'log');
        };
      });
    });
  }

  createTransporter(callback) {
    const transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
      user: 'norep.4fitnessgirls@gmail.com',
      pass: 'dfdsffsdfml^^66e8df'
        //user: 'noreply.4fitnessgirls@gmail.com',
        //pass: 'Fitness2018'
      }
    });
    callback(transporter);
    // nodemailer.createTestAccount((err, account) => {
    //   let transporter = nodemailer.createTransport({
    //     host: 'smtp.ethereal.email',
    //     port: 587,
    //     secure: false, // true for 465, false for other ports
    //     auth: {
    //       user: account.user, // generated ethereal user
    //       pass: account.pass // generated ethereal password
    //     }
    //   });
    //   callback(transporter);
    // });
  }
}

module.exports = Mailer;
