const Statistics = require('../statistics.js');

module.exports = function(req, callback) {
  const cpath = `${process.env.RootPath}controller/${req.params.class}`;
  const token = req.headers.token ? req.headers.token : '';
  let start = new Date().getTime();

  try {
    const CONTROL = require(cpath);
    const controller = new CONTROL(token, req.method, req.params.method, req.params.version);
    const query = req.query ? req.query : null;
    const method = req.method + '' + req.params.method;
    try {
      controller[method](req.params, query, (response) => {
      let finish = new Date().getTime();
      response.time = finish-start;
        Statistics.adminStatistic(response, req.params, req.method, token);
        controller.disconnect();
        delete controller;
        callback(response);
      });
    } catch (e) {
      global.sysLog(req.params.class + '\t' + req.params.method + '\t' + 'FAIL', 'err');
      global.sysLog(e, 'log');
      let response = {code: 503, body: 'Oops. Something went wrong!', error: e};
      let finish = new Date().getTime();
      response.time = finish-start;
      Statistics.adminStatistic(response, req.params, req.method, token);
      callback(response);
    }
  } catch (e) {
    global.sysLog(e, 'err');
    //In case of error in method - return  that Service is Unavailable
    let response = {code: 503, body: 'Oops. Something went wrong!', error: e};
    let finish = new Date().getTime();
    response.time = finish-start;
    Statistics.adminStatistic(response, req.params, req.method, token);
    callback(response);
  }
};
