const formidable = require('formidable');
const path = require('path');
const fs = require('fs');

const fileName = Math.random().toString(36) + '.jpg';

module.exports = function(req, form, callback) {
  const fileName = Math.random().toString(36) + '.jpg';
  let hasImage = false;
  form.multiples = true;
  form.uploadDir = process.env.UserImagesPath;
  const uploadDir = process.env.UserImagesPath;

  try {
    form.on('fileBegin', function(name, file) {
      if (file)
        hasImage = true;
      try {
        file.path = path.join(uploadDir, fileName);
      } catch (e) {
        console.log(e);
        global.sysLog(e, 'err');
      }
    });

    form.parse(req, (err, fields, files) => {
      if (hasImage == true) {
        fields.image = fileName;
        fs.chmodSync(path.join(uploadDir, fileName), 0777);
      }

      callback(fields, hasImage == true ? fileName : undefined);
    });
  } catch (e) {
    console.log(e);
    global.sysLog(e, 'log');
  }
};
