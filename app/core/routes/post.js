const Statistics = require('../statistics.js');
const formidable = require('formidable');
const path = require('path');
const fs = require('fs');

module.exports = function(req, callback) {
  const cpath = `${process.env.RootPath}controller/${req.params.class}`;
  const token = req.headers.token ? req.headers.token : '';
  let contype = req.headers['content-type'];
  let start = new Date().getTime();

  if (!contype) contype = 'x-www-form-urlencoded';

  try {
    const CONTROL = require(cpath);
    const controller = new CONTROL(token, req.method, req.params.method, req.params.version);
    if (contype.indexOf('application/json') >= 0 ||
      contype.indexOf('x-www-form-urlencoded') >= 0) {
      // JSON
      try {
        controller[req.method + '' + req.params.method](req.body, (response) => {
          let finish = new Date().getTime();
          response.time = finish-start;
          Statistics.adminStatistic(response, req.params, req.method, token);
          controller.disconnect();
          delete controller;
          callback(response);
        });
      } catch (e) {
        global.sysLog(req.params.class + '\t' + req.params.method + '\t' + 'FAIL', 'err');
        global.sysLog(e, 'log');
        let response = {
          code: 503,
          body: 'Oops. Something went wrong!',
          error: e
        };
        Statistics.adminStatistic(response, req.params, req.method, token);
        callback(response);
      }
    } else {
      //multipart form-data
      const form = new formidable.IncomingForm();
      const formRouter = require(process.env.Core + 'routes/formPOST.js');
      formRouter(req, form, (fields, fileName) => {
        if (fileName)
          fields.image = fileName;

        try {
          controller[req.method + '' + req.params.method](fields, (response) => {
            if (response.code == 409) {
              if(fileName){
                let unlinkFile = path.join(process.env.UserImagesPath.toString(), fileName.toString());
                fs.unlink(unlinkFile, (err, result) => {
                  err && console.log(err);
                });
              }
            }
            let finish = new Date().getTime();
            response.time = finish-start;
            Statistics.adminStatistic(response, req.params, req.method, token);
            controller.disconnect();
            delete controller;
            callback(response);
          });
        } catch (e) {
          global.sysLog(req.params.class + '\t' + req.params.method + '\t' + 'FAIL', 'err');
          global.sysLog(e, 'log');
          let response = {
            code: 503,
            body: 'Oops. Something went wrong!',
            error: e
          };
          Statistics.adminStatistic(response, req.params, req.method, token);
          callback(response);
        }
      });
    }
  } catch (e) {
    global.sysLog(e, 'err');
    //In case of error in method - return  that Service is Unavailable
    let response = {
      code: 503,
      body: 'Oops. Something went wrong!',
      error: e
    };
    Statistics.adminStatistic(response, req.params, req.method, token);
    callback(response);
  }
};
