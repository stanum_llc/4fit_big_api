const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const MainDB = process.env.MONGO_URL;

const collections = [
  'assistantMessages',
  'calendar',
  'descriptions',
  'nutrition',
  'pregnantDescriptions',
  'pregnantTrainings',
  'pull',
  'purchases',
  'series',
  'signedOnEmails',
  'training',
  'userCalendar',
  'userPurchases',
  'users',
  'videosBase',
];

const _DBLINKS = [
  ['mongodb://localhost:27017/', '1testDB'],
  ['mongodb://localhost:27017/', '2testDB']
];

process.on('disconnect', function() {
  console.log('Sync process: parent exited. I\'m closing too.');
  process.exit(0);
});

let closes = [];

process.on('message', (dt) => {
  let MAINDB, DBS = [];
  MongoClient.connect(MainDB, (err, database) => {
    closes.push(database);
    // console.log('_', collection);
    MAINDB = database.db('4FitnessGirls');
    _DBLINKS.map((e,i)=> {
      MongoClient.connect(e[0] + '' + e[1], (err, database) => {
        DBS[i] = database.db(e[1]);
        closes.push(database);

        if (i == _DBLINKS.length - 1 && DBS.length == _DBLINKS.length) {
          startSync(MAINDB, DBS, ()=> {
            closes.map((close, closeindex)=> {
              close.close();

              if (closeindex == closes.length - 1) {
                process.exit(0);
              }
            });
          });
        }
      });
    });
  });
});

function startSync(MAINDB, DBS, callback) {
  console.log('start sync DBs');
  collections.map((collection, colIndex)=> {
    MAINDB.collection(collection).find().toArray((err, maindocs) => {
      maindocs.map((doc, index)=> {
        DBS.map((DB, i)=> {
          DB.collection(collection).findOne(new mongodb.ObjectId(doc._id), (err, result)=> {
            if (!result) {
              DB.collection(collection).insert(doc, (err, res)=> {
                if (colIndex == collections.length - 1 && index == maindocs.length - 1 && i == DBS.length - 1) {
                  callback();
                }
              });
            } else {
              if (result.modify && doc.modify) {
                if (result.modify < doc.modify) {
                  DB.collection(collection).updateOne(new mongodb.ObjectId(doc._id), {
                    $set: doc
                  }, (err, result) => {
                    if (colIndex == collections.length - 1 && index == maindocs.length - 1 && i == DBS.length - 1) {
                      callback();
                    }
                  });
                }
              } else {
                let d = new Date();
                doc.modify = d.getTime();
                MAINDB.collection(collection).updateOne(new mongodb.ObjectId(doc._id), {
                  $set: doc
                }, (err, result) => {
                  DB.collection(collection).updateOne(new mongodb.ObjectId(doc._id), {
                    $set: doc
                  }, (err, result) => {
                    if (colIndex == collections.length - 1 && index == maindocs.length - 1 && i == DBS.length - 1) {
                      callback();
                    }
                  });
                });
              }
            }
          });
        });
      });
    });
  });
}
