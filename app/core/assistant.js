process.on('disconnect', function () {
	console.log('Assistant: parent exited. I\'m closing too.');
	process.exit(0);
});

const writeMessagePath = `${process.env.RootPath}modules/${process.env.API_VERSION}/assistant/writeMessage.js`;

process.on('message', (data) => {
	console.log('Assistant is working in background');
	let today = new Date();
	today.setHours(12, 0, 0, 0);
	today = today.getTime();
	if (process.env.NODE_ENV == 'production')
		today += 3;

	let processedTime = 0;

	const DB = require(`${process.env.Core}db.js`);
	const Firebase = require(`${process.env.Core}firebase.js`);
	const _ = require('underscore');
	const assistantMessage = require(writeMessagePath);

	setInterval(function () {
		let d = new Date();

		let hour = d.getHours();
		if (process.env.NODE_ENV == 'production')
			hour += 3;
		else if (process.env.NODE_ENV == 'stage')
			hour += 2;

		let minute = d.getMinutes();
		minute = Math.floor(minute / 10) * 10;
		if (hour < 10) hour = '0' + hour;
		if (minute < 10) minute = '0' + minute;
		const time = `${hour}:${minute}`;

		DB.findAll('assistant', {
			time: time
		}, (messages) => {
			messages.map((message, index)=>{
				if (message) {
					DB.findAll('users', {}, (users) => {
						const day = d.getDay();

						users.map((user, index) => {
							switch (message.alias) {
	            // TRAINING
							case 'training':
								if (user.notifications && user.notifications.split(',')[0] == 1) {
									let msg = JSON.parse(JSON.stringify(message));
									const i = Math.floor(Math.random() * (msg.text.length - 0) + 0);
									let text = 'msg.text = `' + msg.text[i] + '`';
									eval(text);
									sendNotification(user, msg);
								}
								break;
	            // TRAINING MOVE
							case 'trainingMove':
								if (user.notifications && user.notifications.split(',')[0] == 1) {
									DB.find('userCalendar', {
										userId: user._id.toString(),
										date: today,
										type: 'training'
									}, (todayTraining) => {
										if (!todayTraining || todayTraining.done == 0) {
											let msg = JSON.parse(JSON.stringify(message));
											const i = Math.floor(Math.random() * (msg.text.length - 0) + 0);
											let text = 'msg.text = `' + msg.text[i] + '`';
											eval(text);
											sendNotification(user, msg);
										}
									});
								}
								break;
	            // TRAINING PROGRAM END
							case 'trainingProgramEnd':
								if (user.notifications && user.notifications.split(',')[0] == 1) {
									DB.find('userPurchases', {
									  userId: user._id.toString(),
									  status: 1
									}, (userPurchase) => {
									  if(userPurchase){
									    const purchaseId = userPurchase._id.toString();
									    const search = {
									      userId: user._id.toString(),
									      purchaseId: purchaseId,
									      date: today,
		                    type: 'training'
									    };
									    DB.find('userCalendar', search, (trainings) => {
		                    if(trainings){
		                      trainings.day = parseInt(trainings.day);
		                      console.log(trainings.day);
		                      if([9,16,24].includes(trainings.day)){
		                        trainings.daysToFinish = 27-trainings.day;
		                        let plan = 'согласно плану';
		                        if(userPurchase.weight){
		                          if(userPurchase.label == 'Жиросжигающая'){
		                            if(user.weight > userPurchase.weight || user.weight == userPurchase.weight)
		                              plan = 'недостаточно быстро';
		                          } else if(userPurchase.label == 'Увеличение мышечной массы') {
		                            if(user.weight < userPurchase.weight || user.weight == userPurchase.weight)
		                              plan = 'недостаточно быстро';
		                          }
		                        }
		                        let msg = JSON.parse(JSON.stringify(message));
		    							      const i = Math.floor(Math.random() * (msg.text.length - 0) + 0);
		    							      let text = 'msg.text = `' + msg.text[i]+'`';
		    							      eval(text);
		    							      sendNotification(user, msg);
		                      }
		                    }
		                  });
		                }
		              });
								}
								break;
	            // BREAKFAST
							case 'breakfast':
								if (user.notifications && user.notifications.split(',')[3] == 1) {
									DB.find('userCalendar', {
										userId: user._id.toString(),
										date: today,
										type: 'nutrition'
									}, (nutrition) => {
										// console.log(nutrition);
										if (nutrition) {
											let msg = JSON.parse(JSON.stringify(message));
											const i = Math.floor(Math.random() * (msg.text.length - 0) + 0);
											let text = 'msg.text = `' + msg.text[i] + '`';
											eval(text);
											sendNotification(user, msg);
										}
									});
								}
								break;
	            // NUTRITION
							case 'nutrition':
								if (user.notifications && user.notifications.split(',')[3] == 1) {
		              DB.find('userPurchases', {
		                userId: user._id.toString(),
		                status: 1
		              }, (userPurchase) => {
		                if (userPurchase) {
		                  DB.find('calendar', {
		                    userId: user._id.toString(),
		                    date: today,
		                    type: 'nutrition'
		                  }, (nutrition) => {
		                    let msg = JSON.parse(JSON.stringify(message));
		  									const i = Math.floor(Math.random() * (msg.text.length - 0) + 0);
		  									let text = 'msg.text = `' + msg.text[i] + '`';
		  									eval(text);
		  									sendNotification(user, msg);
		                  });
		                }
		              });
								}
								break;
	            // WATER MORNING
	            case 'waterMorning':
								if (user.notifications && user.notifications.split(',')[2] == 1) {
		              DB.find('userPurchases', {
		                userId: user._id.toString(),
		                status: 1
		              }, (userPurchase) => {
		                if(userPurchase){
		                  let msg = JSON.parse(JSON.stringify(message));
		                  const i = Math.floor(Math.random() * (msg.text.length - 0) + 0);
		                  let text = 'msg.text = `' + msg.text[i] + '`';
		                  eval(text);
		                  sendNotification(user, msg);
		                }
		              });
								}
	              break;
	            // WATER BALANCE
	            case 'waterBalance':
								if (user.notifications && user.notifications.split(',')[2] == 1) {
		              DB.find('userPurchases', {
		                userId: user._id.toString(),
		                status: 1
		              }, (userPurchase) => {
		                // console.log(userPurchase);
		                if (userPurchase) {
		                  DB.find('userCalendar', {
		                    userId: user._id.toString(),
		                    date: today,
		                    type: 'nutrition',
												purchaseId: userPurchase._id.toString()
		                  }, (nutrition) => {
		                    if (nutrition && nutrition.waterDrinked < nutrition.waterLimit) {
		                      let msg = JSON.parse(JSON.stringify(message));
		                      const i = Math.floor(Math.random() * (msg.text.length - 0) + 0);
		                      let text = 'msg.text = `' + msg.text[i] + '`';
		                      eval(text);
		                      sendNotification(user, msg);
		                    }
		                  });
		                }
		              });
								}
	              break;
	            // Weight
	            case 'weight':
								if (user.notifications && user.notifications.split(',')[1] == 1) {
		              if(d.getDay() == 5 || process.env.NODE_ENV == 'stage' || process.env.NODE_ENV=='local'){
		                let msg = JSON.parse(JSON.stringify(message));
		                const i = Math.floor(Math.random() * (msg.text.length - 0) + 0);
		                let text = 'msg.text = `' + msg.text[i] + '`';
		                eval(text);
		                sendNotification(user, msg);
		              }
								}
	              break;
	            // LOST USER
	            case 'lostUser':
	              DB.findAll('calendar', {userId: user._id.toString()}, (userActivity)=>{
	                if(userActivity.length>0){

	                  userActivity = _.sortBy(userActivity, 'date');
	                  let difference = today - userActivity[0].date;
	                  let days = Math.ceil(difference/(1000*60*60*24));

	                  if(message.text[days]){
	                    let msg = JSON.parse(JSON.stringify(message));
	                    let text = 'msg.text = `' + msg.text[days] + '`';
	                    eval(text);
	                    sendNotification(user, msg);
	                  }
	                }
	              });
	              break;
							}
						});
					});
				}
			});
		});
	}, 1000 * 60 * 10);

	function sendNotification(user, message) {
		if (user.firebase) {
			let assistantNotify = {
				to: user.firebase,
				notification: {
					title: '4 Fitness Girls',
					body: message.text
				}
			};
			Firebase.sendNotification(assistantNotify);
		}
		assistantMessage(DB, {
		  uid: user._id,
		  message: message.text,
		  type: message.type,
		  action: message.action
		});
	};
});
