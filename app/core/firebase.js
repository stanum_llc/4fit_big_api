const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const MongoUrl = process.env.MONGO_URL;

const FCM = require('fcm-push');
const fcm = new FCM(process.env.FirebaseKey);

class Firebase {
  constructor() {
    // var FCM = require('fcm-push');
    // var fcm = new FCM(process.env.FirebaseKey);
    // let message = {
    //   to: 'eAJobw_5X2M:APA91bH5WR8N6z447Id__dePXzjTyeuwhRVewQtVdKmMm2AQrm82iSAPNCoyH9HTqUvYnKa46n-OYUbFlPdCFY30r0Qrw1fyFiIY49X0QUtbUUVDSLae3JCPezjhcW1uA-a0cWOfbVxn',
    //   notification: {
    //     title: '4 Fitness Girls',
    //     body: `TEST`
    //   }
    // };
    // if (message.to != '' && message.to.length > 5) {
    //   fcm.send(message, function(err, response) {
    //     if (err) {
    //       console.log('Something has gone wrong!', err);
    //     } else {
    //       console.log('Successfully sent with response: ', response);
    //     }
    //   });
    // }
  }
  sendNotification(message, additional) {
    if (message.to != '' && message.to.length > 5) {
      fcm.send(message, function(err, response) {
        if (err) {
          // console.log('Something has gone wrong!', err);
          // console.log(message);
        } else {
          console.log('Successfully sent');
        }
      });
    }
  }
};

module.exports = new Firebase;
