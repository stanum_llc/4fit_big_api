const Statistics = require('./statistics.js');
const request = require('request');
const fs = require('fs');

const DB = require('./db.js');

// const _m = require('./dataFiles/messages.json');

module.exports = (app) => {

  app.get('/public/image/:image', (req, res) => {
    try {
      res.sendFile(`${process.env.PublicPath}images/${req.params.image}`);
    } catch (e) {
      res.status(404).send();
    }
  });

  app.get('/validation_video', (req, res) => {
    res.sendFile(`${process.env.PublicPath}instagram video.mp4`);
  });

  app.get('/public/user/:image', (req, res) => {
    if (req.params.image)
      res.sendFile(`${process.env.UserImagesPath}${req.params.image}`);
    else
      res.status(404).send();
  });

  app.get('/well-known/acme-challenge', (req, res)=> {
    res.send(200);
  });

  app.get('/privacy_policy', (req, res) => {
    res.sendFile(`${process.env.PublicPath}privacypolicy.html`);
  });

  app.get('/', (req, res) => {
    res.sendFile(`${process.env.Front}index.html`);
  });

  app.get('/version', (req, res)=> {
    DB.findLimited('admin_log', {method: {$in: ['POST purchase > iosBuy', 'POST purchase > androidBuy']}, time: {$ne:null}}, 10, {date: -1}, (logs)=>{
      let time=0;
      logs.map((e,i)=>{
        if(e.time)
          time+=e.time;
      });
      time /= logs.length;
      res.send(`${process.env.CURRENT_API} ${process.env.NODE_ENV} ${time.toFixed(2)}`);
    });
  });

  app.get('/front/:file', (req, res) => {
    res.sendFile(`${process.env.Front}${req.params.file}`);
  });

  app.get('/front/img/:file', (req, res) => {
    res.sendFile(`${process.env.Front}images/${req.params.file}`);
  });

  app.get('/findEmptyVideos', (req, res)=> {
    DB.findAll('pull', {}, (pulls)=> {
      let nope = [];
      pulls.map((pull, pullIndex)=> {
        pull.data.gym.map((g, gi)=> {

          let name = g.name.replace(/ /g, '').replace(/\n/g, '').toLowerCase();

          setTimeout(()=> {
            DB.find('videosBase', {search: name}, (resp)=> {
              if (!resp)
                nope.push(g.name);
              if (pullIndex == pulls.length - 1)
                res.send(nope);
            });
          }, 200);
        });
        pull.data.home.map((m, mi)=> {
          let name = m.name.replace(/ /g, '').replace(/\n/g, '').toLowerCase();

          setTimeout(()=> {
            DB.find('videosBase', {search: name}, (resp)=> {
              if (!resp)
                nope.push(m.name);
              if (pullIndex == pulls.length - 1)
                res.send(nope);
            });
          }, 200);
        });
        pull.data.street.map((s,si)=> {
          let name = s.name.replace(/ /g, '').replace(/\n/g, '').toLowerCase();
          setTimeout(()=> {
            DB.find('videosBase', {search: name}, (resp)=> {
              if (!resp)
                nope.push(s.name);

              if (pullIndex == pulls.length - 1)
                res.send(nope);
            });
          }, 200);
        });
      });
    });
  });

  // Temporary links route
  app.get('/t/:token/:quality?', (req, res) => {
    let start = new Date().getTime();
    const cpath = `${process.env.RootPath}controller/temporaryLinks.js`;
    const CONTROL = require(cpath);
    try {
      const temporaryLinks = new CONTROL('trainings', req.params.token, req.params.quality);
      temporaryLinks.getMedia((response) => {
        if (response.code == 404) {
          sendResponse(response);
        } else {
          let filePath = `${process.env.VideosPath}${response.body.url}`;
          if (fs.existsSync(filePath))
            res.status(response.code).sendFile(filePath, req.param('file'), function(err) {
              Statistics.fileDownloading(new Date().getTime() - start, req.params.token);
            });
          else {
            res.sendFile(response.body.url, req.param('file'), function(err) {
              Statistics.fileDownloading(new Date().getTime() - start, req.params.token);
            });
          }
        }
      });
    } catch (e) {
      // console.log(e);
      sendResponse({
        code: 503,
        body: 'Oops. Something went wrong!'
      });
    }

    function sendResponse(response) {
      // Statistics.adminStatistic(response, req.params.method);
      res.status(response.code).send(response.body);
    }
  });

  app.get('/s/:token/:quality?', (req, res) => {
    const cpath = `${process.env.RootPath}controller/temporaryLinks.js`;
    const CONTROL = require(cpath);
    try {
      const temporaryLinks = new CONTROL('series', req.params.token, req.params.quality);
      temporaryLinks.getMedia((response) => {
        if (response.code == 404) {
          sendResponse(response);
        } else {
          let filePath = `${response.body.url}`;
          if (fs.existsSync(filePath))
            res.status(response.code).sendFile(filePath);
          else {
            // console.log('Not found' + filePath);
            res.status(404).send(filePath.replace(process.env.VideosPath, '...'));
          }
        }
      });
    } catch (e) {
      // console.log(e);
      sendResponse({
        code: 503,
        body: 'Oops. Something went wrong!'
      });
    }

    function sendResponse(response) {
      res.status(response.code).send(response.body);
    }
  });

  // Authentication for Instagram shit API
  app.get('/instagramAuth', (req, res) => {
    // Redirect on instagram authentication
    const cliend_id = '0625c43f33fb4e6798797a06c34ade9b';
    const redirect_uri = `${process.env.APIURL}callback`;
    const inst = 'https://api.instagram.com/oauth/authorize/';
    const link = `${inst}?client_id=${cliend_id}&redirect_uri=${redirect_uri}&response_type=code`;
    res.redirect(link);
  });

  app.get('/callback', (req, res) => {
    try {
      const instagram_data = {
        client_id: '0625c43f33fb4e6798797a06c34ade9b',
        grant_type: 'authorization_code',
        client_secret: '9b71bda6ae424b6293ea80537d122329',
        redirect_uri: `${process.env.APIURL}callback`,
        code: req.query.code
      };
      request.post('https://api.instagram.com/oauth/access_token')
        .form(instagram_data)
        .on('error', function(err) {
          global.sysLog(err, 'err');
        })
        .on('data', function(data) {
          const cpath = `${process.env.RootPath}controller/users.js`;
          const USER = require(cpath);
          const _user = new USER('', 'POST', 'authenticateSocial');
          const dt = JSON.parse(data);
          dt.social = 'inst';
          _user.POSTauthenticateSocial(dt, (response) => {
            sendResponse(response);
          });
        });

      function sendResponse(response) {
        res.status(response.code).send(response.body);
      }
    } catch (e) {
      sendResponse({
        code: 503,
        body: 'Oops. Something went wrong!',
        error: e
      });
    }
  });
  try {
    app.route('/API/:version?/:class/:method')
      .get((req, res) => {
        let route = require(`${process.env.Core}routes/get.js`);
        let v = req.params.version;
        if (!v) v = process.env.API_VERSION;
        if (process.env.LogAll == true)
          console.log('>', v, 'GET', req.params.class, req.params.method);
        route(req, (response) => {
          if (response.body && response.body.message)
            response.body.message = response.body.message;
          res.status(response.code).send(response.body);
        });
      })
      .post((req, res) => {
        let route = require(`${process.env.Core}routes/post.js`);
        let v = req.params.version;
        if (!v) v = process.env.API_VERSION;
        if (process.env.LogAll == true)
          console.log('>', v, 'POST', req.params.class, req.params.method);
        route(req, (response) => {
          // console.log(response.code,response.body);
          if (response.body.message)
            response.body.message = response.body.message;
          try {
            res.status(response.code).send(response.body);
          } catch (e) {}
        });
      })
      .put((req, res) => {
        let route = require(`${process.env.Core}routes/put.js`);
        let v = req.params.version;
        if (!v) v = process.env.API_VERSION;
        if (process.env.LogAll == true)
          console.log('>', v, 'PUT', req.params.class, req.params.method);
        route(req, (response) => {
          if (response.body.message)
            response.body.message = response.body.message;

          res.status(response.code).send(response.body);
        });
      });
  } catch (e) {
    console.log(e);
  }
};
