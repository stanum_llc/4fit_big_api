const Cryptr = require('cryptr');
const fs = require('fs');

const env = '123';

class Encryptor {
  constructor(type) {
    this.type = type;
    this.algorithm = 'aes-256-ctr';
    this.key = '9c40AVxZOv9XEdMerMYJXClOI4RH5Ya2';
    const key = fs.readFileSync(`${process.env.KeysPath}${this.type}.key`, 'utf-8', (err, key) => {
      return key;
    });
    this.crypt = new Cryptr(key);
    this.ignore = [
      'name',
      'surname',
      'image',
      '_id',
      'id',
      'fb_id',
      'inst_id',
      'vk_id',
      'trainingPlace',
      'firebase',
      'target',
      'userLevel',
      'notifications'
    ];
  }
  encrypt(textString) {
    if (process.env.NODE_ENV != env) {
      if (textString == null) textString = '';
      const encrypted = this.crypt.encrypt(textString);
      return encrypted;
    } else {
      return textString;
    }
  }
  encryptMany(data) {
    if (process.env.NODE_ENV != env) {
      Object.keys(data).map((e,i)=> {
        if (data[e] != null)
          data[e] = data[e].toString();
      });
      let encrypted = Object.assign({}, data);
      Object.keys(encrypted).map((e, i) => {
        if (encrypted[e] != null)
          if (encrypted[e] && this.ignore.includes(e) == false) {
          try {
            encrypted[e] = this.crypt.encrypt(encrypted[e]);
          } catch (e) {}
        }
      });
      return encrypted;

    } else {
      return data;
    }
  }
  decrypt(textString) {
    if (process.env.NODE_ENV != env) {
      if (textString == null) textString = '';
      const decrypted = this.crypt.decrypt(textString);
      return decrypted;

    } else {
      return textString;
    }
  }
  decryptMany(data) {
    if (data && data.accessPassword)
      delete data.accessPassword;

    if (process.env.NODE_ENV != env) {
      let decrypted = Object.assign({}, data);
      Object.keys(decrypted).map((e, i) => {
        if (decrypted[e] == null || decrypted[e] == undefined) decrypted[e] = '';
        if (e != '_id' && decrypted[e] && this.ignore.includes(e) == false) {
          try {
            decrypted[e] = this.crypt.decrypt(decrypted[e]);
          } catch (e) {}
        }
      });

      decrypted.userLevel = parseInt(decrypted.userLevel);
      decrypted.target = parseInt(decrypted.target);
      decrypted.trainingPlace = parseInt(decrypted.trainingPlace);
      decrypted.weekOfPregnancy = parseInt(decrypted.weekOfPregnancy);

      return decrypted;
    } else {
      return data;
    }
  }
}

module.exports = Encryptor;
