process.on('disconnect', function() {
  console.log('Assistant: parent exited. I\'m closing too.');
  process.exit(0);
});

const writeMessagePath = `${process.env.RootPath}modules/${process.env.API_VERSION}/assistant/writeMessage.js`;

process.on('message', (data) => {
  console.log('Assistant is working in background');
  let today = new Date();
  today.setHours(12, 0, 0, 0);
  today = today.getTime();
  if (process.env.NODE_ENV == 'production')
    today += 3;

  let processedHour = 0;

  const DB = require(`${process.env.Core}db.js`);
  const Firebase = require(`${process.env.Core}firebase.js`);
  const _ = require('underscore');
  const assistantMessage = require(writeMessagePath);

  setInterval(function() {
    DB.findAll('users', {}, (users) => {

      let d = new Date();
      let hour = d.getHours();
      let minute = d.getMinutes();

      if (process.env.NODE_ENV == 'production')
        hour += 3;

      if (d.getDay() < 6) {
        if (hour == 21 && processedHour != hour) {
          processedHour = hour;
          users.map((user, index) => {
            if (user.notifications && user.notifications.split(',')[0] == 1) {
              DB.find('userPurchases', {
                userId: user._id.toString(),
                status: 1
              }, (pur) => {
                if (pur) {
                  DB.find('calendar', {
                    userId: user._id.toString(),
                    date: today,
                    type: 'training'
                  }, (todayTraining) => {
                    if (!todayTraining || todayTraining.done == 0) {
                      if (user.firebase) {
                        let message = {
                          to: user.firebase,
                          notification: {
                            title: '4 Fitness Girls',
                            body: `${user.name}, вы не отметили тренировку сегодня`
                          }
                        };
                        Firebase.sendNotification(message);
                      }
                      assistantMessage(DB, {
                        uid: user._id,
                        message: `${user.name}, вы не отметили тренировку сегодня`,
                        type: 'transference',
                        action: 'transference'
                      });
                    }
                  });
                }
              });
            }
          });
        } else if (hour == 20 && hour != processedHour) {
          processedHour = hour;
          users.map((user, index) => {
            if (user.notifications && user.notifications.split(',')[3] == 1) {
              DB.find('userPurchases', {
                userId: user._id.toString(),
                status: 1
              }, (pur) => {
                if (pur) {
                  DB.find('calendar', {
                    userId: user._id.toString(),
                    date: today,
                    type: 'nutrition'
                  }, (nutrition) => {
                    if (!nutrition) {
                      if (user.firebase) {
                        let message = {
                          to: user.firebase,
                          notification: {
                            title: '4 Fitness Girls',
                            body: `${user.name}, вы не отметили питание сегодня`
                          }
                        };
                        Firebase.sendNotification(message);
                      }
                      assistantMessage(DB, {
                        uid: user._id,
                        message: `${user.name}, вы не отметили питание сегодня`,
                        type: 'nutrition',
                        action: 'check'
                      });
                    }
                  });
                }
              });
            }
          });
        } else if (hour == 19 && hour != processedHour) {
          processedHour = hour;
          users.map((user, index) => {
            if (user.notifications && user.notifications.split(',')[2] == 1) {
              DB.find('userPurchases', {
                userId: user._id.toString(),
                status: 1
              }, (pur) => {
                if (pur) {
                  DB.find('calendar', {
                    userId: user._id.toString(),
                    date: today,
                    type: 'nutrition'
                  }, (nutrition) => {
                    if (!nutrition) {
                      if (user.firebase) {
                        let message = {
                          to: user.firebase,
                          notification: {
                            title: '4 Fitness Girls',
                            body: `${user.name}, вы не отметили потребление воды сегодня`
                          }
                        };
                        Firebase.sendNotification(message);
                      }
                      assistantMessage(DB, {
                        uid: user._id,
                        message: `${user.name}, вы не отметили потребление воды сегодня`,
                        type: 'water',
                        action: 'check'
                      });
                    }
                  });
                }
              });
            }
          });
        }
      }

      // sunday
      if (d.getDay() == 6) {
        if (hour == 12 && hour != processedHour) {
          processedHour = hour;
          // weight
          users.map((user, index) => {
            if (user.notifications && user.notifications.split(',')[1] == 1) {
              DB.find('userPurchases', {
                userId: user._id.toString(),
                status: 1
              }, (pur) => {
                if (pur) {
                  if (user.firebase) {
                    let message = {
                      to: user.firebase,
                      notification: {
                        title: '4 Fitness Girls',
                        body: `${user.name}, укажите ваш текущий вес`
                      }
                    };
                    Firebase.sendNotification(message);
                  }
                  assistantMessage(DB, {
                    uid: user._id,
                    message: `${user.name}, укажите ваш текущий вес`,
                    type: 'weight',
                    action: 'write'
                  });
                }
              });
            }
          });
        } else if (hour == 14 && hour != processedHour) {
          processedHour = hour;
          users.map((user, index) => {
            if (user.notifications && user.notifications.split(',')[0] == 1) {
              DB.find('userPurchases', {
                userId: user._id.toString(),
                status: 1
              }, (pur) => {
                if (pur) {
                  DB.find('userPurchases', {
                    userId: user._id.toString(),
                    status: 1
                  }, (purchase) => {
                    if (purchase) {
                      const purchaseId = purchase._id.toString();
                      const search = {
                        userId: user._id.toString(),
                        purchaseId: purchaseId,
                        done: 0
                      };
                      DB.findAll('userCalendar', search, (trainings) => {
                        if (user.firebase) {
                          let message = {
                            to: user.firebase,
                            notification: {
                              title: '4 Fitness Girls',
                              body: `${user.name}, до окончания программы осталось ${trainings.length} тренировок`
                            }
                          };
                          Firebase.sendNotification(message);
                        }
                        assistantMessage(DB, {
                          uid: user._id,
                          message: `${user.name}, до окончания программы осталось ${trainings.length} тренировок`,
                          type: 'text',
                          action: ''
                        });
                      });
                    }
                  });
                }
              });
            }
          });
        }
      }
    });
  }, 1000 * 60 * 10);
});
