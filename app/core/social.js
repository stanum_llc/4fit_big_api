const request = require('request');

class Social {
  constructor() {
    this.fbLink = 'https://graph.facebook.com/v3.0/me';
    this.fbFields = 'id,first_name,last_name,picture';
    this.vkLink = 'https://api.vk.com/method/users.get';
    this.vkFields = 'id,name,picture,photo_200_orig,photo_max';
  }
  fbTokenCheck(token, callback) {
    let link = `${this.fbLink}?fields=${this.fbFields}&access_token=${token}`;
    let options = {
      url: link,
      method: 'GET',
      headers: {
        'User-Agent': 'request'
      }
    };

    request(options, (err, res, body)=> {
      callback(JSON.parse(body));
    });
    // request(link, (err, res, body) => {
    //   console.log(err);
    //   console.log(res);
    //   callback(JSON.parse(body));
    // });
  }
  vkTokenCheck(token, callback) {
    let link = `${this.vkLink}?v=5.74&access_token=${token}&fields=${this.vkFields}`;
    request(link, (err, res, body) => {
      callback(JSON.parse(body).response[0]);
    });
  }
}

module.exports = new Social;
