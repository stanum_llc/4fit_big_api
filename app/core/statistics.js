const jwt = require('jsonwebtoken');

class Statistics {
  constructor() {
    this.DBCore = require('./db.js');
  }
  adminStatistic(data, request, method, token) {
    let userToken = jwt.decode(token);
    let userID = undefined;
    if(userToken && userToken._id)
      userID = userToken._id;
    let d = {
      code: data.code,
      method: method + ' ' + request.class + ' > ' + request.method,
      date: new Date(Date.now()).getTime(),
      user: userID,
      time: data.time
    };
    try {
      if (data.body.message) d.message = data.body.message;

      let message = d.message ? '\t' + d.message : '';

      if (process.env.LogAll.toString() == true.toString()) {
        let ms = data.code + ' - ' + method + '\t' + request.class + ' \t ' + request.method + message;
        if (parseInt(parseInt(data.code) / 100) > 3) {
          global.sysLog(ms, 'err');
        } else {
          global.sysLog(ms, 'log');
        }

      }
      console.log(d);
      this.DBCore.insert('admin_log', d, (res) => {
        let week = new Date().getTime() - (1000 * 60 * 60 * 24 * 8);
        this.DBCore.remove('admin_log', {date: {$lt: week}}, (res)=> {

        });
      });
    } catch (e) {
      console.log(e);
    }
  }
  fileDownloading(time, fileToken) {
    let token = jwt.decode(fileToken);
    let data = {
      date: new Date().getTime(),
      file: {
        name: token.name,
        place: token.place,
        type: token.type
      },
      user: token.u
    };
    this.DBCore.insert('admin_log', data, (res)=> {
      let week = new Date().getTime() - (1000 * 60 * 60 * 24 * 8);
      this.DBCore.remove('admin_log', {date: {$lt: week}}, (res)=> {

      });
    });
  }
}

module.exports = new Statistics;
