const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const MongoUrl = process.env.MONGO_URL;
const _ = require('underscore');

class DBCore {
  constructor() {
    this.timeout = 50;
    MongoClient.connect(process.env.MONGO_URL, (err, database) => {
      if (err)
        console.log(err);
      this.DB = database.db('4FitnessGirls');
      this.database = database;
    });
  }
  insert(col, data, callback) {
    // if (!this.DB) this.reconnect();
    // this.reconnect(()=> {
    this.reconnect(() => {
      try {
        this.DB.collection(col).insert(data, (err, result) => {
          if (err) {
            try {
              // global.sysLog(err, 'err');
              // this.disconnect();
              callback(err);
            } catch (e) {
              console.log(err);
            }
          }
          // this.disconnect();
          callback(result);
        });
      } catch (e) { }
    });
    // });
  }
  insertMany(col, data, callback) {
    // if (!this.DB) this.reconnect();
    this.reconnect(() => {
      this.DB.collection(col).insertMany(data, (err, result) => {
        // this.disconnect();
        callback(result);
      });
    });
  }

  find(col, search, callback) {
    console.log('find', col);
    let back = (result) => {
      // this.disconnect();
      callback(result);
    };
    if (search._id)
      search._id = new mongodb.ObjectId(search._id);
    // try {
    // if (!this.DB) this.reconnect();
    this.reconnect(() => {
      this.DB.collection(col).findOne(search, (err, result) => {
        if (err) {
          console.log(err);
          try {
            global.sysLog(err, 'err');
          } catch (e) {
            console.log(err);
          }
        }
        // this.disconnect();
        callback(result);
      });
    });
    // } catch (e) {
    // console.log(e);
    // callback();
    // }
  }

  findLimited(col, search, limit, sortObject = {}, callback) {
    if (search._id)
      search._id = new mongodb.ObjectId(search._id);

    // if (!this.DB) this.reconnect();
    this.reconnect(() => {
      this.DB.collection(col).find(search).sort(sortObject).toArray((err, docs) => {
        if (err) {
          try {
            global.sysLog(err, 'err');
          } catch (e) {
            console.log(err);
          }
        }
        docs = _.shuffle(docs);
        // this.disconnect();
        callback(docs.slice(0, limit));
      });

    });
  }

  findAll(col, search, callback) {
    console.log('findAll', col);

    if (search._id)
      search._id = new mongodb.ObjectId(search._id);

    // if (!this.DB) this.reconnect();
    this.reconnect(() => {
      this.DB.collection(col).find(search).toArray((err, docs) => {
        if (err) {
          try {
            global.sysLog(err, 'err');
          } catch (e) {
            console.log(err);
          }
        }
        // this.disconnect();
        callback(docs);
      });

    });
  }
  findInDate(col, search, callback) {

    // if (!this.DB) this.reconnect();
    this.reconnect(() => {
      this.DB.collection(col).find({
        userid: search.userid,
        date: {
          $gt: search.date.from,
          $lt: search.date.to
        }
      }).toArray((err, docs) => {
        if (err) {
          try {
            global.sysLog(err, 'err');
          } catch (e) {
            console.log(err);
          }
        }
        // this.disconnect();
        callback(docs);
      });

    });
  }
  update(col, search, data, callback) {

    if (search._id)
      search._id = new mongodb.ObjectId(search._id);

    // if (!this.DB) this.reconnect();
    this.reconnect(() => {
      this.DB.collection(col).updateOne(search, {
        $set: data
      }, (err, result) => {
        if (err) {
          try {
            global.sysLog(err, 'err');
          } catch (e) {
            console.log(err);
          }
        }
        // this.disconnect();
        callback(result);
      });

    });
  }
  updateAll(col, search, data, callback) {

    if (search._id)
      search._id = new mongodb.ObjectId(search._id);

    // if (!this.DB) this.reconnect();
    if (data) {
      this.reconnect(() => {
        this.DB.collection(col).find(search).toArray((err, docs) => {
          if (err) {
            try {
              global.sysLog(err, 'err');
            } catch (e) {
              console.log(err);
            }
          }
          if (docs.length > 0) {
            docs.map((e, i) => {
              this.update(col, {
                _id: e._id
              }, data, (response) => {
                if (i == docs.length - 1) {
                  // this.disconnect();
                  callback('OK');
                }
              });
            });
          } else {
            // this.disconnect();
            callback('OK');
          }
        });

      });
    } else {
      // this.disconnect();
      callback('OK');
    }
  }
  clear(col, callback) {
    // if (!this.DB) this.reconnect();
    this.reconnect(() => {
      this.DB.collection(col).drop(() => {
        // this.disconnect();
        callback();
      });
    });
  }
  drop(col) {
    this.reconnect(() => {
      this.DB.collection(col).drop(() => {
      });
    });
  }
  remove(col, search, callback) {

    if (search._id)
      search._id = new mongodb.ObjectId(search._id);

    this.reconnect(() => {
      this.DB.collection(col).deleteOne(search, (err, result) => {
        if (err) {
          console.log(err);
        }
        callback(result);
      });

    });
  }
  disconnect() {
    if (this.database) {
      this.database.close();
    }
    delete this.database;
    delete this.DB;
  }
  reconnect(callback) {
    if (!this.database || !this.DB) {
      MongoClient.connect(process.env.MONGO_URL, (err, database) => {
        this.disconnect();
        if (database) {
          this.DB = database.db('4FitnessGirls');
          this.database = database;
          callback();
        } else {
          this.reconnect(() => {
            callback();
          });
        }
        // callback();
      });
    } else {
      callback();
    }
  }
};

module.exports = new DBCore();
