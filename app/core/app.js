const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const helmet = require('helmet');
const request = require('request');
const Mattermost = require('node-mattermost');
const https = require('https');

var morgan = require('morgan');
app.use(morgan(':method :url :req[header]', {immediate: true}));
app.use(morgan(':method :url :status - :response-time ms :req[header] :date'));

app.use(helmet());
app.disable('x-powered-by');
app.enable('trust proxy');

app.use(bodyParser.json());
app.use(bodyParser.json({
  limit: '1000mb',
  type: 'application/*+json'
}));
app.use(bodyParser.urlencoded({
  extended: true
}));

var port = process.env.PORT || 3000;

try {
  app.listen(port, function() {
    console.log('Listening on ' + port);
  });
} catch (e) {
  console.log(e);
}

// try {
//   https.createServer({
//       cert: fs.readFileSync('./home/video/cert/fullchain_4f.pem'),
//       key: fs.readFileSync('./home/video/cert/privkey_4.pem')
//     }, app).listen(4443);
// } catch (e) {
//   console.log(e);
// }

global.sysLog = (text, type)=> {
  if (process.env.NODE_ENV != 'local') {
    const dateTime = new Date();
    if (type == 'log') {
      text = text + '\t' + dateTime;
      console.log(text);
    } else if (type == 'err') {
      console.log(text, '\x1b[31m', dateTime, '\x1b[0m');
    }
  } else {
    console.log(text);
  }
};

module.exports = app;
