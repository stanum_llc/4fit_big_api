const fs = require('fs');
const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const MongoUrl = process.env.MONGO_URL;
const DB = require(`${process.env.Core}db.js`);
// Admin Token
const atkn = 'WpNMu$@%o^8!0z@8O>^DY`9ZLgOkKJn_H0:_`z8/o?l#[kM]KU{aMo?nOr>S~?(';

module.exports = (app) => {

  app.post('/admin/fill/pregnantTrainings', (req, res)=> {
    if (req.headers.token == atkn) {
      // // const DB = new DBCore();
      DB.drop('pregnantTrainings');
      let descriptionsFolder = `${process.env.PrivatePath}pregnant trainings/`;
      fs.readdir(descriptionsFolder, (err, files) => {
        let count = 0;
        files.forEach(file => {
          fs.readFile(`${descriptionsFolder}${file}`, 'utf-8', (err, data)=> {
            data = data.replace(/k/g,'к').replace(/K/g,'К');
            descriptionParser(data, file);
            count++;
            if (count == files.length)
              res.status(200).send({message: 'OK'});
          });
        });
      });
    } else
      res.status(401).send();
  });

  function descriptionParser(data, file) {
    // // const DB = new DBCore();
    let place = file.substring(0, file.indexOf('.'));

    if (place == 'home') place = 0;
    else if (place == 'outdoor') place = 1;
    else if (place == 'gym') place = 2;

    let descriptionsList = [];
    let table = data.split('\n');
    let trimestr = 0;
    let part = '';
    let trainings = [];
    table.map((line, index)=> {
      let lineObject = line.split(';');
      // console.log(lineObject[0]);
      // if (lineObject[0]) {
      if (lineObject[0].match(/\d триместр/)) {
        trimestr = parseInt(lineObject[0]);
      }
      if (lineObject[1]) {
        if (lineObject[1].toLowerCase() != 'упражнения') {
          if (lineObject[1].match(/ЛФК/i) && lineObject[2] == '') {
            part = 'lfk';
          } else if (lineObject[1].match(/РАЗМИНКА/i) && lineObject[2] == '') {
            part = 'warmup';
          } else if (lineObject[1].match(/Заминка/i) && lineObject[2] == '') {
            part = 'hitch';
          } else if (lineObject[1].match(/ВЕРХ/i) && lineObject[2] == '') {
            part = 'body';
          } else if (lineObject[1].match(/НОГИ/i) && lineObject[2] == '') {
            part = 'legs';
          } else if (!lineObject[1].match(/упражнения/i) && lineObject[2] != '') {
            send(lineObject, part, trimestr);
          }
          function send(line, part, trim) {
            let train = {
              name: beautifyText(line[1].replace(/\n/g, '')),
              search: line[1].replace(/\n/, '').replace(/ /g, '').toLowerCase(),
              repeats: line[2] ? line[2].replace(/\n/g, '') : '',
              note: line[3] ? line[3].replace(/\n/g, '') : '',
              part: part,
              trimestr: trim
            };

            train.repeatsDetails = {};

            if (train.repeats.match(/[0-9]х[0-9]/)) {
              // train.repeats = train.repeats.replace(/ /g, '');
              let left = train.repeats.split('х')[0];
              let right = train.repeats.split('х')[1];
              train.repeatsDetails.repeats = parseInt(right.split('-')[0].split(' ')[0]);
              train.repeatsDetails.approaches = parseInt(left.split('-')[0].split(' ')[0]);
            } else if (train.repeats.match(/-/)) {
              train.repeatsDetails.repeats = parseInt(train.repeats.split('-')[0]);
              if (train.repeats.match(/мин/gi)) {
                train.repeatsDetails = {};
                train.time = parseInt(train.repeats.split('-')[0]) * 60;
              }
            } else {
              train.repeatsDetails.repeats = parseInt(train.repeats.split(' ')[0]);
              if (train.repeats.match(/мин/gi)) {
                train.repeatsDetails = {};
                train.time = parseInt(train.repeats.split(' ')[0]) * 60;
              }
            }

            if (line[1].length > 0) {
              DB.insert('pregnantTrainings', train, ()=> {});
            }
          }
        }
      }
      // }
      // }
    });
  }

  function beautifyText(text) {
    let a = text;
    a = a.toLowerCase();
    a = a.split('');
    a[0] = a[0].toUpperCase();
    a = a.join('');
    return a;
  }

  // class DBCore{
  //   insert(col, data) {
  //     connectToDB(col, (collection, close)=> {
  //       collection.insert(data, (err, result)=> {
  //         if (err)
  //           global.sysLog(err, 'err');
  //         close();
  //       });
  //     });
  //   }
  //   drop(col) {
  //     connectToDB(col, (collection, close)=> {
  //       collection.drop(()=> {
  //         close();
  //       });
  //     });
  //   }
  // };
  //
  // function connectToDB(col, callback) {
  //   MongoClient.connect(MongoUrl, (err, database)=> {
  //     if (err)
  //       global.sysLog(err, 'err');
  //     callback(database.db('4FitnessGirls').collection(col), ()=> {
  //       database.close();
  //     });
  //   });
  // }

};
