const fs = require('fs');
const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const MongoUrl = process.env.MONGO_URL;
const DB = require(`${process.env.Core}db.js`);

// Admin Token
const atkn = 'WpNMu$@%o^8!0z@8O>^DY`9ZLgOkKJn_H0:_`z8/o?l#[kM]KU{aMo?nOr>S~?(';

let programsCount = 0;
let insertedCount = 0;

const rexp = {
  pullCat: /жен,/,
  gym: /тренажёрный зал|трен зал/g,
  home: /дома|дом/,
  street: /улица/,
  week: /[Нн]еделя [0-9]{1,}/g
};

module.exports = (app) => {
  app.post('/admin/fill/training', (req, res)=> {
    if (req.headers.token == atkn) {
      DB.drop('pull');
      DB.drop('training');

      let trainingFolder = `${process.env.PrivatePath}training/`;
      fs.readdir(trainingFolder, (err, files) => {
        let count = 0;
        files.forEach(file => {
          fs.readFile(`${trainingFolder}${file}`, 'utf-8', (err, data)=> {
            data = data.replace(/k/g,'к').replace(/K/g,'К');
            fillDB(data);
            count++;
            if (count == files.length) {
              console.log(programsCount, insertedCount);
              res.status(200).send({message: 'OK'});
            }
          });
        });
      });
    } else {
      res.status(401).send();
    }
  });
};

function fillDB(data) {
  trainingParser(data, (res)=> {
    res.map((pe,pi)=> {
      let training = {
        weight: pe.weight,
        years: pe.years,
        height: pe.height,
        level: pe.level,
        weeks: pe.weeks
      };
      DB.insert('training', training, ()=> { DB.disconnect(); });
      insertedCount++;
      Object.keys(pe.pulls).map((e,i)=> {
        let pull = {
          name: e,
          search: e.toLowerCase().replace(/ /g, '').replace(/\n/g, ''),
          weight: pe.weight,
          years: pe.years,
          height: pe.height,
          level: pe.level,
          data: pe.pulls[e]
        };
        DB.insert('pull', pull, ()=> { DB.disconnect(); });
      });
    });
  });
}
function trainingParser(data, callback) {
  let currentPull = '';
  let programs = [];
  let programNumber = -1;
  let weeks = [];
  let currentDay = -1;

  let gymIndex = -1;
  let homeIndex = -1;
  let streetIndex = -1;

  let switcher = 0;

  let d = data.split('\n');
  d.map((line, index)=> {
    let arr = line.split(';');
    arr.map((e,i)=> {
      if (arr[i].match(rexp.pullCat)) {
        programsCount++;

        switcher = 0;
        programNumber ++;
        const pn = programNumber;
        programs[programNumber] = {};
        programs[programNumber].pulls = {};
        const a = arr[i].split(',');

        const weight = a[1].match(/(\d[0-9]{1,})(\+|от)?/g);
        const years = a[2].match(/(\d[0-9]{1,})/g);
        const height = a[3] == '' ? a[4] : a[3];
        const level = a[4].replace(/ /g, '').replace(/\уровень/g, '').toLowerCase();

        /*
          PROGRAM TITLE
        */

        // program weight getting
        if (weight.length == 1) {
          if (a[1].match(/(от)/g)) {
            programs[pn].weight = {};
            programs[pn].weight.from = parseInt(weight[0]);
          } else {
            programs[pn].weight = {};
            programs[pn].weight.to = parseInt(weight[0]);
          }
        } else if (weight.length == 2) {
          programs[pn].weight = {};
          programs[pn].weight.from = parseInt(weight[0]);
          programs[pn].weight.to = parseInt(weight[1]);
        }

        // program years getting
        if (years.length == 1) {
          if (a[2].match(/\+/g) || a[2].match(/от/)) {
            programs[pn].years = {};
            programs[pn].years.from = parseInt(years[0]);
          } else {
            programs[pn].years = {};
            programs[pn].years.to = parseInt(years[0]);
          }
        } else if (years.length == 2) {
          programs[pn].years = {};
          programs[pn].years.from = parseInt(years[0]);
          programs[pn].years.to = parseInt(years[1]);
        }

        // program height getting
        if (height.match(/\до/g)) {
          programs[pn].height = {};
          programs[pn].height.to = parseInt(height.match(/(\d[0-9]{1,})/g));
        } else if (height.match(/(\с)?\выше|от/g)) {
          programs[pn].height = {};
          programs[pn].height.from = parseInt(height.match(/(\d[0-9]{1,})/g));
        }

        // program level
        programs[pn].level = (level == 'новичек') ? 0 :
                        level.match(/продвинут/) ? 1 : 2;
      }
      if (switcher == 0) {
        if (arr[i].match(/^пул /i)) {
          currentPull = arr[i].toLowerCase();
          programs[programNumber].pulls[currentPull] = {};
        }
        if (arr[i].match(rexp.gym)) {
          gymIndex = i;
        }
        if (arr[i].match(rexp.home)) {
          homeIndex = i;
        }
        if (arr[i].match(rexp.street)) {
          streetIndex = i;
        }

        if (programs[programNumber] &&
          programs[programNumber].pulls[currentPull]) {
          if (((i == gymIndex && gymIndex >= 0) ||
              (i == streetIndex && streetIndex >= 0) ||
              (i == homeIndex && homeIndex >= 0)) && arr[i] == '') {
            // console.log(arr[i], i);
          }
          if (!arr[i].match(/Неделя/)) {

            let name = arr[i].toLowerCase();
            name = name.split('');
            if (name[0]) {
              name[0] = name[0].toUpperCase();
            }
            name = name.join('');

            if (i == gymIndex && gymIndex >= 0) {
              if (!arr[i].match(rexp.gym)) {
                if (!programs[programNumber].pulls[currentPull].gym)
                  programs[programNumber].pulls[currentPull].gym = [];

                let data = {
                  name: name,
                  weight: arr[i + 1],
                  repeats: arr[i + 2]
                };
                if (arr[i].length > 2)
                  programs[programNumber].pulls[currentPull].gym.push(data);
              }
            } else if (i == streetIndex && streetIndex >= 0) {
              if (!arr[i].match(rexp.street)) {
                if (!programs[programNumber].pulls[currentPull].street)
                  programs[programNumber].pulls[currentPull].street = [];

                let data = {
                  name: name,
                  weight: arr[i + 1],
                  repeats: arr[i + 2]
                };
                if (arr[i].length > 2)
                  programs[programNumber].pulls[currentPull].street.push(data);
              }
            } else if (i == homeIndex && homeIndex >= 0) {
              if (!arr[i].match(rexp.home)) {
                if (!programs[programNumber].pulls[currentPull].home)
                  programs[programNumber].pulls[currentPull].home = [];

                let data = {
                  name: name,
                  weight: arr[i + 1],
                  repeats: arr[i + 2]
                };
                if (arr[i].length > 2)
                  programs[programNumber].pulls[currentPull].home.push(data);
              }
            }
          }
        }
      }
      if (arr[i].match(rexp.week)) {
        switcher = 1;
        if (!programs[programNumber].weeks)
          programs[programNumber].weeks = [];

        let w = arr[i].match(rexp.week);
        let weekNumber = parseInt(w.toString().match(/[0-9]/g));

        weeks[weekNumber - 1] = i;
        programs[programNumber].weeks[weekNumber - 1] = [];
      }
      if (arr[i].match(/Day [0-9]/g)) {
        let d = arr[i].match(/[0-9]/);
        currentDay = parseInt(d) - 1;

        if (!arr[i + 1].match(/([Тт]рен \d|[Тт]ренировка \d|^(?!Пул)[Дд]ень|[Вв]ых)/ig)) {
          // console.log('Нет названия дня', arr[i], ';строка - ' + (index + 1), ';лист документа - ' + programs[programNumber].level);
          console.log(arr[i], (index + 1), programs[programNumber].level);
        }
      }

      //shortcuts
      const pn = programNumber;
      const cd = currentDay;

      if (switcher == 1 && !arr[i].match(rexp.week)) {
        if (weeks.indexOf(i) >= 0) {
          let wn = weeks.indexOf(i);
          if (!programs[pn].weeks[wn][cd])
            programs[pn].weeks[wn][cd] = {name: 'Тренировка', list: []};

          if (arr[i].length > 2 && arr[i].match(/([Тт]рен \d|[Тт]ренировка \d|тренировка|^(?!Пул)[Дд]ень|[Вв]ых)/i)) {

            let name = arr[i].toLowerCase().split('');
            name[0] = name[0].toUpperCase();
            name = name.join('');

            programs[pn].weeks[wn][cd].name = name;

          } else if (arr[i].length > 2 && !arr[i].match(/([Тт]рен [0-9]|[Тт]ренировка [0-4]|тренировка|^(?!пул)[Дд]ень|[Вв]ых)/i)) {
            programs[pn].weeks[wn][cd].list.push(arr[i].toString().toLowerCase());
          }
        }
      }

    });
  });
  callback(programs);
}

// class DBCore{
//   insert(col, data) {
//     connectToDB(col, (collection, close)=> {
//       collection.insert(data, (err, result)=> {
//         if (err)
//           global.sysLog(err, 'err');
//         close();
//       });
//     });
//   }
//   drop(col) {
//     connectToDB(col, (collection, close)=> {
//       collection.drop(()=> {
//         close();
//       });
//     });
//   }
// };
//
// // const DB = new DBCore();
//
// function connectToDB(col, callback) {
//   MongoClient.connect(MongoUrl, (err, database)=> {
//     if (err)
//       global.sysLog(err, 'err');
//     callback(database.db('4FitnessGirls').collection(col), ()=> {
//       database.close();
//     });
//   });
// }
