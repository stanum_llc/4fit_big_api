const fs = require('fs');
const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const MongoUrl = process.env.MONGO_URL;
const DB = require(`${process.env.Core}db.js`);

// Admin Token
const atkn = 'WpNMu$@%o^8!0z@8O>^DY`9ZLgOkKJn_H0:_`z8/o?l#[kM]KU{aMo?nOr>S~?(';

module.exports = (app) => {

  app.post('/admin/fill/videosBase', (req, res)=> {
    if (req.headers.token == atkn) {
      // const DB = new DBCore();
      DB.drop('videosBase');
      let descriptionsFolder = `${process.env.PrivatePath}videosBase/`;
      fs.readdir(descriptionsFolder, (err, files) => {
        let count = 0;
        files.forEach(file => {
          fs.readFile(`${descriptionsFolder}${file}`, 'utf-8', (err, data)=> {
            descriptionParser(data, file, ()=> {
                // updateVideoLength(()=> {
                res.status(200).send({message: 'OK'});
                // });
              });
          });
        });
      });
    } else
      res.status(401).send();
  });

  function addVideoLength(video, callback) {
    try {
      const ffprobe = require('ffprobe');
      const ffprobeStatic = require('ffprobe-static');

      // DB.findAll('videosBase', {}, (videos)=> {
      // videos.map((video, videoIndex)=> {
      let place = video.place == 0 ? 'nastya' :
              video.place == 1 ? 'olya' :
              video.place == 2 ? 'sveta' :
              'natasha';

      // console.log(videos.length, place);

      getVideo(video, place);

      function getVideo(video, place) {
        if (process.env.NODE_ENV != 'local') {
          let filePath = `${process.env.VideosPath}${place}/short/${video.video}.mp4`;
          ffprobe(filePath, {path: ffprobeStatic.path}, function(err, info) {
            let duration = 2;

            if (err)
              console.log('Not found', filePath);
            else {
              // console.log('FOUNT', place, video.video);
              console.log(video.video);
              duration = parseInt(info.streams[0].duration);
            }

            video.videoDuration = duration;
            video.size = fs.statSync(filePath).size;

            callback(video);

            // DB.update('videosBase', {_id: video._id}, {videoDuration: duration}, (res)=> {
            //   // if (videoIndex == videos.length - 1) {
            //   DB.disconnect();
            //   callback();
            //   // }
            // });
          });
        } else {
          video.videoDuration = 2;
          callback(video);
        }
      }
      // });
      // });

    } catch (e) {
      callback(video);
    }
  }

  function descriptionParser(data, file, callback) {
    // const DB = new DBCore();
    let place = file.substring(0, file.indexOf('.'));

    if (place == 'home') place = 0;
    else if (place == 'outdoor') place = 1;
    else if (place == 'gym') place = 2;

    let descriptionsList = [];
    let table = data.split('\n');
    let count = 0;
    table.map((line, index)=> {
      if (index > 1) {
        let lineObject = line.split(';');

        //sveta
        if (lineObject[0] != '' && lineObject[2] != '') {
          if (!lineObject[2])
            console.log(lineObject);
          addVideoLength({
            video: parseInt(lineObject[0]).toString(),
            name: beautifyText(lineObject[2]),
            search: lineObject[2].replace(/\n/, '').replace(/ /g, '').toLowerCase().replace(/^\s+|\s+$/g,''),
            place: 2
          }, (train)=> {
            DB.insert('videosBase', train, ()=> {
              DB.disconnect();
              count++;
              if (count == table.length - 1)
                callback();
            });
          });
        }
        //olya
        if (lineObject[9] != '' && lineObject[11] != '') {
          if (!lineObject[11])
            console.log(lineObject);
          addVideoLength({
            video: parseInt(lineObject[9]).toString(),
            name: beautifyText(lineObject[11]),
            search: lineObject[11].replace(/\n/, '').replace(/ /g, '').toLowerCase().replace(/^\s+|\s+$/g,''),
            place: 1
          }, (train)=> {
            DB.insert('videosBase', train, ()=> {
              DB.disconnect();
              count++;
              if (count == table.length - 1)
                callback();
            });
          });

          // insert.push({
          //   video: parseInt(lineObject[8]).toString(),
          //   name: lineObject[10],
          //   search: lineObject[10].replace(/\n/, '').replace(/ /g, '').toLowerCase(),
          //   place: 2
          // });
        }
        //nastya
        if (lineObject[18] != '' && lineObject[20] != '' && lineObject[20]) {
          if (!lineObject[20])
            console.log(lineObject);

          addVideoLength({
            video: parseInt(lineObject[18]).toString(),
            name: beautifyText(lineObject[20]),
            search: lineObject[20].replace(/\n/, '').replace(/ /g, '').toLowerCase().replace(/^\s+|\s+$/g,''),
            place: 0
          }, (train)=> {
            DB.insert('videosBase', train, ()=> {
              DB.disconnect();
              count++;
              if (count == table.length - 1)
                callback();
            });
          });
          // insert.push({
          //   video: parseInt(lineObject[17]).toString(),
          //   name: lineObject[20],
          //   search: lineObject[20].replace(/\n/, '').replace(/ /g, '').toLowerCase(),
          //   place: 0
          // });
        }
        //natasha
        if (lineObject[30] != '' && lineObject[27] != '' && lineObject[27]) {
          if (!lineObject[27])
            console.log(lineObject);
          addVideoLength({
            video: parseInt(lineObject[27]).toString(),
            name: beautifyText(lineObject[30]),
            search: lineObject[30].replace(/\n/, '').replace(/ /g, '').toLowerCase().replace(/^\s+|\s+$/g,''),
            place: 3
          }, (train)=> {
            DB.insert('videosBase', train, ()=> {
              DB.disconnect();
              count++;
              if (count == table.length - 1)
                callback();
            });
          });
          // insert.push({
          //   video: parseInt(lineObject[25]).toString(),
          //   name: lineObject[28],
          //   search: lineObject[28].replace(/\n/, '').replace(/ /g, '').toLowerCase(),
          //   place: 3
          // });
        }
        // setTimeout(()=> {
        // DB.insert('videosBase', insert, ()=> {
        //   count++;
        //   if (count == table.length - 1)
        //     callback();
        // });
        // }, 1500);
      }
    });
  }
  //
  // class DBCore{
  //   insert(col, data) {
  //     connectToDB(col, (collection, close)=> {
  //       collection.insertMany(data, (err, result)=> {
  //         if (err)
  //           global.sysLog(err, 'err');
  //         close();
  //       });
  //     });
  //   }
  //   drop(col) {
  //     connectToDB(col, (collection, close)=> {
  //       collection.drop(()=> {
  //         close();
  //       });
  //     });
  //   }
  // };
  //
  // function connectToDB(col, callback) {
  //   MongoClient.connect(MongoUrl, (err, database)=> {
  //     if (err)
  //       global.sysLog(err, 'err');
  //     try {
  //       callback(database.db('4FitnessGirls').collection(col), ()=> {
  //         database.close();
  //       });
  //     } catch (e) {
  //
  //     }
  //   });
  // }
};

function beautifyText(text) {
  let a = text;
  a = a.replace(/^\s+|\s+$/g,'');

  // if (a.match(/спаржа\+зелень/))
  //   a = 'спаржа + зелень';

  a = a.toLowerCase();
  a.replace(/  /g, ' ');
  a.replace(/\t/g, ' ');
  a.replace(/\n/g, ' ');
  a = a.split('');
  for (let i = 0; i > a.length; i++) {
    if (a[0] == ' ')
      a[0] = '';
    else
      return;
  }
  a[0] = a[0].toUpperCase();
  a = a.join('');
  return a;
}
