
// Admin Token
const atkn = 'WpNMu$@%o^8!0z@8O>^DY`9ZLgOkKJn_H0:_`z8/o?l#[kM]KU{aMo?nOr>S~?(';
const fs = require('fs');

module.exports = (app) => {
  app.get('/admin/checkDBConnections', (req, res)=> {
    if (req.headers.token == atkn) {

      pregnant(()=> {
        // DBCore.disconnect();
        // trainingPrograms(()=> {
        trainings(()=> {
          // DBCore.disconnect();
          res.send('DONE');
          // });
        });
      });

    }else {
      res.status(401).send();
    }
  });
};

function trainingPrograms() {
  console.log('__ Programs searching __');

  const yo = [{to: 29}, {from: 30, to: 45}, {from: 45}];
  const height = [{to: 170}, {from: 170}];
  const weight = [{to: 55},{from: 55, to: 70},{from: 70}];
  const level = [0,1,2];

  yo.map((y,yi)=> {
    height.map((h,hi)=> {
      weight.map((w,wi)=> {
        level.map((l,li)=> {
          // console.log(l);
          findProgramBySearch({years: y,height: h,weight: w,level: l});
        });
      });
    });
  });
}

function findProgramBySearch(search) {
  // console.log(search);
  const DBCore = require(`${process.env.RootPath}core/db.js`);
  // let time = Math.floor(Math.random() * (10000 - 1000)) + 1000;
  // setTimeout(()=> {
  DBCore.find('training', search, (res)=> {
    if (!res) {
      let string = '';
      search.weight.from ? string += 'from ' + search.weight.from + ' kg ' : '';
      search.weight.to ? string += 'to ' + search.weight.to + ' kg ' : '';
      search.height.from ? string += 'from ' + search.height.from + ' cm ' : '';
      search.height.to ? string += 'to ' + search.height.to + ' cm ' : '';
      search.years.from ? string += 'from ' + search.years.from + ' years ' : '';
      search.years.to ? string += 'to ' + search.years.to + ' years ' : '';
      string += ' level ' + search.level == 0 ? 'novice' :
                            search.level == 1 ? 'advanced' :
                            search.level == 2 ? 'sport' : '?';

      console.log(string);
    } else {
    }
  });
  // }, time);
}

function trainings(callback) {
  const DBCore = require(`${process.env.RootPath}core/db.js`);
  console.log('__ Trainings searching __');
  // DBCore.disconnect();
  DBCore.findAll('training', {}, (trainingsList)=> {
    // DBCore.disconnect();
    console.log('Total trainigns ' + trainingsList.length);
    // // DBCore.disconnect();
    trainingsList.map((trainingProgram, trainingProgramIndex)=> {
      let trainingsCount = 0;

      trainingProgram.weeks.map((week, weekIndex)=> {
        let weekCount = 0;

        week.map((day, dayIndex)=> {
          let dayCount = 0;

          if (!day.name.match(/кардио|активности/i)) {
            day.list.map((pullName, pullIndex)=> {
              let pullCount = 0;
              const pullSearchName = pullName.toLowerCase().replace(/ /g, '').replace(/\n/, '');

              let pullSearch = {
                weight: trainingProgram.weight,
                years: trainingProgram.years,
                height: trainingProgram.height,
                level: trainingProgram.level,
                search: pullSearchName
              };

              if (pullName.match(/заминка|разминка/))
                pullSearch = {
                  search: (trainingProgram.level) + '_' + pullSearchName
                };

              findPull(pullSearch, pullName, ()=> {
                pullCount++;
                if (pullCount == day.list.length - 1) {
                  dayCount++;
                  console.log('d', dayCount);

                  if (dayCount == week.length - 1) {
                    weekCount++;
                    console.log('w', weekCount);
                  }

                  if (weekCount == trainingProgram.weeks.length - 1) {
                    trainingsCount++;
                    console.log('t', trainingsCount);
                  }

                  if (trainingsCount == trainingsList.length) {
                    // callback();
                  }
                }
              });
            });
          }
        });
      });
    });
  });
}

function pregnant(callback) {
  const DBCore = require(`${process.env.RootPath}core/db.js`);
  count = 0;
  console.log('__ Pregnant searching __');
  // DBCore.disconnect();
  DBCore.findAll('pregnantTrainings', {}, (trainings)=> {
    // DBCore.disconnect();
    console.log('Total pregnant ' + trainings.length);
    trainings.map((training, index)=> {
      pregnantDescription(training, ()=> {
        count += 0.5;
        if (count == trainings.length - 1)
          callback();
      });
      pregnantVideo(training, ()=> {
        count += 0.5;
        if (count == trainings.length - 1)
          callback();
      });
    });
  });
}

function pregnantDescription(training, callback) {
  const DBCore = require(`${process.env.RootPath}core/db.js`);
  // DBCore.disconnect();
  DBCore.find('pregnantDescriptions', {search: training.search}, (description)=> {
    // DBCore.disconnect();
    if (!description)
      console.log('\x1b[31m[Беременные описание]\x1b[0m', training.name);
    callback();
    // else
    // console.log('[Беременные описание]', training.name);
  });
}

function pregnantVideo(training, callback) {
  const DBCore = require(`${process.env.RootPath}core/db.js`);
  // DBCore.disconnect();
  DBCore.find('videosBase', {search: training.search, place: 3}, (video)=> {
    // DBCore.disconnect();
    if (!video)
      console.log('\x1b[31m[Беременные видео]\x1b[0m', training.name);
    callback();
    // else
    // console.log('[Беременные видео]', training.name);
  });
}

function findPull(pullSearch, pullName, callback) {
  const DBCore = require(`${process.env.RootPath}core/db.js`);
  let time = Math.floor(Math.random() * (10000 - 1000)) + 1000;
  setTimeout(()=> {
    count = 0;
    // DBCore.disconnect();
    DBCore.find('pull', pullSearch, (pullInfo)=> {
      // DBCore.disconnect();
      if (!pullInfo)
        console.log('\x1b[31m[ПУЛ]\x1b[0m', pullSearch);
      else {
        // console.log('[ПУЛ]', pullName);
        Object.keys(pullInfo.data).map((place, placeIndex)=> {
          pullInfo.data[place].map((element, index)=> {
            // console.log('1');
            findDescription(element, 0, ()=> {
              // callback();
            });
            // findVideo(element, 0, ()=> {
            //   count++;
            //   // console.log(count);
            //   if (count == 3) {
            //     // callback();
            //   }
            // });
            // findVideo(element, 1, ()=> {
            //   count++;
            //   // console.log(count);
            //   if (count == 3) {
            //     // callback();
            //   }
            // });
            // findVideo(element, 2, ()=> {
            //   count++;
            //   // console.log(count);
            //   if (count == 3) {
            //     // callback();
            //   }
            // });
          });
        });
      }
    });
  }, time);
}

function findVideo(element, place, callback) {
  const DBCore = require(`${process.env.RootPath}core/db.js`);
  // console.log('Searching video ' + element.name);
  const search = element.name.replace(/ /g, '').replace(/\n/, '').toLowerCase();

  // DBCore.disconnect();
  DBCore.find('videosBase', {search: search}, (video)=> {
    // console.log(search);
    // DBCore.disconnect();
    // console.log({search: search, place: place}, video);

    // if (element.name.match('Выпад с шагом вперед'))
    //   console.log(video);
    //
    let pl = place == 0 ? 'home' : place == 1 ? 'outdoor' : 'gym';
    if (!video)
      console.log('\x1b[31m[ВИДЕО]\x1b[0m', element.name, pl);
    // else
    // console.log('[ВИДЕО]', element.name, place);
    // callback();
  });
}

function findDescription(element, place, callback) {
  const DBCore = require(`${process.env.RootPath}core/db.js`);
  // // DBCore.disconnect();
  // console.log('Searching description ' + element.name);
  const search = element.name.replace(/ /g, '').replace(/\n/, '').toLowerCase();
  // console.log(search);
  // DBCore.disconnect();
  DBCore.find('descriptions', {search: search}, (description)=> {
    // DBCore.disconnect();
    // console.log({search: search, place: place}, description);
    if (!description)
    console.log('\x1b[31m[ОПИСАНИЕ]\x1b[0m', element.name);
    // callback();
    // findVideo(element, place, ()=> {
    //   // callback();
    // });
  });
}
