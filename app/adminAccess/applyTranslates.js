const atkn = 'WpNMu$@%o^8!0z@8O>^DY`9ZLgOkKJn_H0:_`z8/o?l#[kM]KU{aMo?nOr>S~?(';
const fs = require('fs');
const DB = require(`${process.env.Core}db.js`);

let translates = {};
let wrongTranslates = [];
let translated = [];

module.exports = (app) => {
  app.post('/admin/translate/:table', (req, res)=> {
    fs.readFile(`${process.env.PrivatePath}/eng/translates.csv`, 'utf-8', (err, data)=> {
      DB.findAll(req.params.table, {}, (toTranslate)=> {
        let table = data.split('\n');
        table.forEach((line, index)=> {
          let lineArray = line.split(';');
          let name = lineArray[0];//.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
          translates[name] = lineArray[1];
        });

        setTimeout(()=> {
          toTranslate.forEach((toTrans, di)=> {
            // translated.push(parseDescription(toTrans, di));
            // translated.push(parsePregnantTrainings(toTrans, di));
            // translated.push(parsePull(toTrans, di));

            // parseNutrition(toTrans, di, (tran)=>{
            //   translated.push(tran);
            //   if(translated.length == toTranslate.length){
            //     translated.forEach((telement, translatedIndex) => {
            //       const id = telement._id;
            //       delete telement._id;
            //       DB.update(req.params.table, {_id: id}, telement, (res)=>{
            //         console.log(translatedIndex, translated.length);
            //       });
            //     })
            //   }
            // });

            parseTrainings(toTrans, di, (tran)=> {
              // res.send(tran);
              translated.push(tran);
              if (translated.length == toTranslate.length) {
                translated.forEach((telement, translatedIndex) => {
                  const id = telement._id;
                  delete telement._id;
                  DB.update(req.params.table, {_id: id}, telement, (res)=> {
                    console.log(translatedIndex, translated.length);
                  });
                });
              }
            });

            // translated.push(parseVideoBase(toTrans, di));
          });
          setTimeout(()=> {
            if (wrongTranslates.length > 0) {
              res.send(wrongTranslates.join('\n'));
            } else {
              res.send(translated);
              // translated.forEach((telement, translatedIndex) => {
              //   DB.update(req.params.table, {_id: telement._id}, telement, (res)=> {
              //     console.log(translatedIndex, translated.length);
              //     if (translatedIndex == translated.length - 2)
              //       res.send(translated);
              //   });
              // });
            }
          }, 15000);
        }, 1000);
      });
    });
  });
};

function parseVideoBase(vid, vi) {
  try {
    // console.log(translates[transformName(vid.name)], transformName(vid.name))
    // if(!translates[transformName(vid.name)])
    //   console.log(vid.name);
    vid.name = translates[transformName(vid.name)];
    vid.search = transformName(vid.name);
  } catch (e) {
    console.log(vid);
  }
  return vid;
}

function parsePull(pull, pi) {
  if (!translates[transformName(pull.name)])
    console.log(pull.name);
  else {
    pull.name = translates[transformName(pull.name)];
    pull.search = transformName(pull.name);
  }
  try {
    Object.keys(pull.data).forEach((place, placeIndex) => {
      pull.data[place].forEach((train, ti) => {
        if (!translates[transformName(train.name)])
          console.log(pull.name);
        else {
          train.name = translates[transformName(train.name)];
          train.search = transformName(train.name);
        }
      });
    });
  } catch (e) {
    console.log(e);
  }

  return pull;
}

function parsePregnantTrainings(tr, ti) {
  if (!translates[transformName(tr.name)])
    wrongTranslates.push(tr.name);
  else {
    tr.name = translates[transformName(tr.name)];
    tr.search = transformName(tr.name);
  }
  return tr;
}

function parseTrainings(nut, di, callback) {
  let text = JSON.stringify(nut);
  Object.keys(translates).forEach((key, index) => {
    text = text.replace(key, translates[key]);
  });
  Object.keys(translates).forEach((key, index) => {
    try {
      var regex = new RegExp(key, 'gi');
      text = text.replace(regex, translates[key]);
    } catch (e) {
      // console.log(key);
    }
  });

  console.log(JSON.parse(text).weeks.length);
  text = text.replace(/Не ограничено/g, 'Not limited');
  text = text.replace(/Выходной/g, 'Day-off');
  text = text.replace(/Тренировка [0-9] \((.*?)\)/gi, 'Training');
  text = text.replace(/День кардио/g, 'Cardio day');
  text = text.replace(/сек/g, 'sec');
  text = text.replace(/мин/g, 'min');
  console.log(JSON.parse(text).weeks.length);

  setTimeout(()=> {
    callback(JSON.parse(text));
  }, 1000);
}

function parseNutrition(nut, di, callback) {
  let text = JSON.stringify(nut);
  text = text.replace(/Овощи \(зелень. огурец. брокколи\)/g, 'Vegetables (greens, cucumber, broccoli)');
  Object.keys(translates).forEach((key, index) => {
    text = text.replace(key, translates[key]);
  });
  Object.keys(translates).forEach((key, index) => {
    try {
      var regex = new RegExp(key, 'gi');
      text = text.replace(regex, translates[key]);
    } catch (e) {
      console.log(key);
    }
  });

  text = text.replace(/Не ограничено/g, 'Not limited');

  setTimeout(()=> {
    callback(JSON.parse(text));
  }, 200);
}

function parseDescription(desc, di) {
  Object.keys(desc).forEach((descKey, descIndex)=> {
    if (descKey != '_id') {
      let name = desc[descKey].replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
      if (translates[name]) {
        desc[descKey] = translates[name];
        if (descKey == 'name') {
          desc['search'] = translates[name].replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
        }
      } else {
        if (desc[descKey].length > 0 && desc[descKey] != '#ERROR!')
          wrongTranslates.push(desc[descKey]);
      }
    }
  });
  return desc;
}

function transformName(name) {
  return name.replace(/[&\/\\#,+()$~%.'":*?<>{}\s]/g, '').toLowerCase();
}
