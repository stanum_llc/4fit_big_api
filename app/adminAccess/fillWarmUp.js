const fs = require('fs');
const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const MongoUrl = process.env.MONGO_URL;
const DB = require(`${process.env.Core}db.js`);
// Admin Token
const atkn = 'WpNMu$@%o^8!0z@8O>^DY`9ZLgOkKJn_H0:_`z8/o?l#[kM]KU{aMo?nOr>S~?(';

module.exports = (app) => {

  app.post('/admin/fill/warmup', (req, res)=> {
    if (req.headers.token == atkn) {
      // const DB = new DBCore();
      // DB.drop('descriptions');
      let descriptionsFolder = `${process.env.PrivatePath}warmup/`;
      fs.readdir(descriptionsFolder, (err, files) => {
        let count = 0;
        files.forEach(file => {
          fs.readFile(`${descriptionsFolder}${file}`, 'utf-8', (err, data)=> {
            descriptionParser(data, file);
            count++;
            if (count == files.length)
              res.status(200).send({message: 'OK'});
          });
        });
      });
    } else
      res.status(401).send();
  });

  function descriptionParser(data, file) {
    // const DB = new DBCore();

    let pulls = {};

    let descriptionsList = [];
    let table = data.split('\n');
    let level = '';
    let pullName = '';
    let filled = 0;

    let name = '';

    table.map((line, index)=> {

      if (index > 0) {
        let lineObject = line.split(';');
        if (lineObject[0]) {
          if (lineObject[0] != '') {
            if (lineObject[0] == 'НОВИЧОК') level = 0;
            if (lineObject[0] == 'ПРОДВИНУТЫЙ') level = 1;
            if (lineObject[0] == 'СПОРТ') level = 2;
          }
        }
        if (lineObject[3]) {
          if (lineObject[3].match(/пул/i)) {

            home = 0;
            street = 0;
            gym = 0;

            pullName = beautifyText(lineObject[3]);
            // console.log(pullName);

            filled = 1;
            name = level + '_' + pullName.replace(/ /g, '').replace(/\n/g, '').toLowerCase();

            if (!pulls[name]) {
              pulls[name] = {
                name: pullName,
                search: name,
                level: level,
                data: {
                  gym: [],
                  street: [],
                  home: []
                }
              };
            }

            if (lineObject[4].match(/улица/i)) {
              street = 1;
            }
            if (lineObject[5].match(/дом/i)) {
              home = 1;
            }
            if (lineObject[6].match(/зал/i)) {
              gym = 1;
            }
          } else {

            let train = {
              name: beautifyText(lineObject[3]),
              repeats: lineObject[7]
            };

            if (home == 1)
              pulls[name].data.home.push(train);

            if (street == 1)
              pulls[name].data.street.push(train);

            if (gym == 1)
              pulls[name].data.gym.push(train);
          }
        }
        // console.log(index, table.length - 1);
        if (index == table.length - 1) {
          Object.keys(pulls).map((e,i)=> {
            DB.insert('pull', pulls[e], ()=> {DB.disconnect(); });
          });
        }
      }
    });
  }

  function beautifyText(text) {
    let a = text;
    a = a.toLowerCase();
    a = a.split('');
    a[0] = a[0].toUpperCase();
    a = a.join('');
    return a;
  }

  // class DBCore{
  //   insert(col, data) {
  //     connectToDB(col, (collection, close)=> {
  //       collection.insert(data, (err, result)=> {
  //         if (err)
  //           global.sysLog(err, 'err');
  //         close();
  //       });
  //     });
  //   }
  //   drop(col) {
  //     connectToDB(col, (collection, close)=> {
  //       collection.drop(()=> {
  //         close();
  //       });
  //     });
  //   }
  // };
  //
  // function connectToDB(col, callback) {
  //   MongoClient.connect(MongoUrl, (err, database)=> {
  //     if (err)
  //       global.sysLog(err, 'err');
  //     callback(database.db('4FitnessGirls').collection(col), ()=> {
  //       database.close();
  //     });
  //   });
  // }

};
