const fs = require('fs');
const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const MongoUrl = process.env.MONGO_URL;
const DB = require(`${process.env.Core}db.js`);
// Admin Token
const atkn = 'WpNMu$@%o^8!0z@8O>^DY`9ZLgOkKJn_H0:_`z8/o?l#[kM]KU{aMo?nOr>S~?(';

module.exports = (app) => {

  app.post('/admin/fill/pregnantDescriptions', (req, res)=> {
    if (req.headers.token == atkn) {
      // // const DB = new DBCore();
      DB.drop('pregnantDescriptions');
      let descriptionsFolder = `${process.env.PrivatePath}pregnant descriptions/`;
      fs.readdir(descriptionsFolder, (err, files) => {
        let count = 0;
        files.forEach(file => {
          fs.readFile(`${descriptionsFolder}${file}`, 'utf-8', (err, data)=> {
            data = data.replace(/k/g,'к').replace(/K/g,'К');
            descriptionParser(data, file);
            count++;
            if (count == files.length)
              res.status(200).send({message: 'OK'});
          });
        });
      });
    } else
      res.status(401).send();
  });

  function descriptionParser(data, file) {
    // // const DB = new DBCore();
    let place = file.substring(0, file.indexOf('.'));

    if (place == 'home') place = 0;
    else if (place == 'outdoor') place = 1;
    else if (place == 'gym') place = 2;

    let descriptionsList = [];
    let table = data.split('\n');

    table.map((line, index)=> {
      if (index > 0) {
        let lineObject = line.split(';');
        let name = lineObject[0].replace(/\n/g, '');

        if (lineObject[0]) {
          let description = {
            search: lineObject[0].replace(/\n/, '').replace(/ /g, '').toLowerCase(),
            name: name,
            position: lineObject[1].replace(/\n/g, ''),
            mainAccents: lineObject[2].replace(/\n/g, ''),
            breathing: lineObject[3].replace(/\n/g, ''),
            additionalInfo: lineObject[4].replace(/\n/g, ''),
            mainMuscles: lineObject[5].replace(/\n/g, ''),
            additionalMuscles: lineObject[6].replace(/\n/g, '')
          };
          if (name.length > 0)
            DB.insert('pregnantDescriptions', description, ()=> {});
        }
      }
    });
  }

  // class DBCore{
  //   insert(col, data) {
  //     connectToDB(col, (collection, close)=> {
  //       collection.insert(data, (err, result)=> {
  //         if (err)
  //           global.sysLog(err, 'err');
  //         close();
  //       });
  //     });
  //   }
  //   drop(col) {
  //     connectToDB(col, (collection, close)=> {
  //       collection.drop(()=> {
  //         close();
  //       });
  //     });
  //   }
  // };
  //
  // function connectToDB(col, callback) {
  //   MongoClient.connect(MongoUrl, (err, database)=> {
  //     if (err)
  //       global.sysLog(err, 'err');
  //     callback(database.db('4FitnessGirls').collection(col), ()=> {
  //       database.close();
  //     });
  //   });
  // }

};
