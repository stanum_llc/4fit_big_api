const fs = require('fs');
const request = require('request');
// Admin Token
const atkn = 'WpNMu$@%o^8!0z@8O>^DY`9ZLgOkKJn_H0:_`z8/o?l#[kM]KU{aMo?nOr>S~?(';

module.exports = (app) => {
  app.post('/admin/fill/netall', (req, res)=> {
    const path = 'http://api.4fitnessgirls.net/admin/fill/';

    const paths = [
      'descriptions',
      'nutrition',
      'pregnantDescriptions',
      'pregnantTrainings',
      'purchases',
      'series',
      'training',
      'videosBase',
      'warmup'
    ];

    if (req.headers.token == atkn) {
      fill(0);
      function fill(index) {
        if (index < paths.length) {
          var options = {
              url: path + paths[index],
              method: 'POST',
              headers: {
                'token': atkn
              }
            };
          request(options, (err, response)=> {
            if (JSON.parse(response.body).message == 'OK') {
              global.sysLog(paths[index] + ' is filled', 'log');
              index += 1;
              fill(index);
            }
          });
        } else {
          res.send({message: 'OK'});
        }
      }
    }
  });
};
