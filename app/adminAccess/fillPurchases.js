const fs = require('fs');
const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const MongoUrl = process.env.MONGO_URL;
const DB = require(`${process.env.Core}db.js`);

// Admin Token
const atkn = 'WpNMu$@%o^8!0z@8O>^DY`9ZLgOkKJn_H0:_`z8/o?l#[kM]KU{aMo?nOr>S~?(';

module.exports = (app) => {
  app.post('/admin/fill/purchases', (req, res) => {
    if (req.headers.token == atkn) {
      try {
        let purchases = [{
            pid: 1,
            price: 4.99,
            subscription: 0,
            type: 'series',
            android_id: '4fitnessgirls_series.season.2',
            ios_id: 'com.4FitnessGirls.series.season.2',
            enabled: 1,
            label: 'Один сезон мини-сериала',
            shortDescription: 'Подписка на мини-сериал (доступ к просмотру сезона)',
            details: 2
          },
          {
            pid: 2,
            price: 4.99,
            subscription: 0,
            type: 'series',
            android_id: '4fitnessgirls_series.season.3',
            ios_id: 'com.4FitnessGirls.series.season.3',
            enabled: 1,
            label: 'Один сезон мини-сериала',
            shortDescription: 'Подписка на мини-сериал (доступ к просмотру сезона)',
            details: 3
          },
          {
            pid: 3,
            price: 4.99,
            subscription: 0,
            type: 'series',
            android_id: '4fitnessgirls_series.season.4',
            ios_id: 'com.4FitnessGirls.series.season.4',
            enabled: 1,
            label: 'Один сезон мини-сериала',
            shortDescription: 'Подписка на мини-сериал (доступ к просмотру сезона)',
            details: 4
          },
          // Beginner
          {
            pid: 4,
            price: 0.99,
            subscription: 0,
            type: 'trainings',
            android_id: 'beginner_monthly',
            ios_id: '4fitnessgirls.sub.month.beginner',
            enabled: 1,
            target: 0,
            label: 'Жиросжигающая',
            image: process.env.APIURL + 'public/image/weight_loss.png',
            shortDescription: 'Жиросжигающая  - идеальная программа для тех, кто хочет сбросить лишний вес и повысить тонус тела. Программа предусматривает высокоинтенсивные  тренировки при небольших рабочих весах. Питание сбалансировано с учетом дефицита калорий.',
            description: {
              title: 'Новичок - программа для тех, кто только начинает свой путь в спорте',
              list: [
                'умеренно легкая',
                'невысокие нагрузки',
                'вы не занимались фитнесом ранее',
                'вы не знаете с чего начать'],
              text: 'Эта программа идеальный выбор для вас!'
            },
            details: 1
          },
          {
            pid: 5,
            price: 0.99,
            subscription: 0,
            type: 'trainings',
            android_id: 'beginner_monthly',
            ios_id: '4fitnessgirls.sub.month.beginner',
            enabled: 1,
            target: 1,
            label: 'Увеличение мышечной массы',
            image: process.env.APIURL + 'public/image/mas.png',
            shortDescription: 'Увеличение мышечной массы - программа, позволяющая улучшить пропорции тела. Рассчитана на увеличение массы и объемов тела, а также на повышение силовых показателей! Программа тренировок оптимизирована на силовые упражнения с прогрессией нагрузок. Диета состоит из пищи богатой белками и углеводами с профицитом калорий.',
            description: {
              title: 'Новичок - программа для тех, кто только начинает свой путь в спорте',
              list: [
                'умеренно легкая',
                'невысокие нагрузки',
                'вы не занимались фитнесом ранее',
                'вы не знаете с чего начать'],
              text: 'Эта программа идеальный выбор для вас!'
            },
            details: 1
          },
          {
            pid: 6,
            price: 0.99,
            subscription: 0,
            type: 'trainings',
            android_id: 'beginner_monthly',
            ios_id: '4fitnessgirls.sub.month.beginner',
            enabled: 1,
            target: 2,
            label: 'Повышение тонуса',
            image: process.env.APIURL + 'public/image/power.png',
            shortDescription: 'Повышение тонуса - программа оптимальна для поддержания текущей формы, сохранения упругости и качества тела! Программа предусматривает тренировки в среднем диапазоне повторений с рабочими весами. Питание - сбалансировано по КБЖУ.',
            description: {
              title: 'Новичок - программа для тех, кто только начинает свой путь в спорте',
              list: [
                'умеренно легкая',
                'невысокие нагрузки',
                'вы не занимались фитнесом ранее',
                'вы не знаете с чего начать'],
              text: 'Эта программа идеальный выбор для вас!'
            },
            details: 1
          },
          {
            pid: 7,
            price: 2.99,
            subscription: 0,
            type: 'trainings',
            android_id: 'middle_monthly',
            ios_id: '4fitnessgirls.sub.month.amateur',
            enabled: 1,
            target: 0,
            label: 'Жиросжигающая',
            image: process.env.APIURL + 'public/image/weight_loss.png',
            shortDescription: 'Жиросжигающая  - идеальная программа для тех, кто хочет сбросить лишний вес и повысить тонус тела. Программа предусматривает высокоинтенсивные  тренировки при небольших рабочих весах. Питание сбалансировано с учетом дефицита калорий.',
            description: {
              title: 'Продвинутый - этот уровень для тех, кому нужна сбалансированная программа для достижения поставленной цели',
              list: [
                'умеренная нагрузка в течении всей недели',
                'оптимальный рацион для спортивного тела',
                'вы тренировались ранее',
                'вы имеете представление о базовых упражнениях',
                'вы следите за питанием'],
              text: 'Выбирайте продвинутый уровень прямо сейчас!'
            },
            details: 2
          },
          {
            pid: 8,
            price: 2.99,
            subscription: 0,
            type: 'trainings',
            android_id: 'middle_monthly',
            ios_id: '4fitnessgirls.sub.month.amateur',
            enabled: 1,
            target: 1,
            label: 'Увеличение мышечной массы',
            image: process.env.APIURL + 'public/image/mas.png',
            shortDescription: 'Увеличение мышечной массы - программа, позволяющая улучшить пропорции тела. Рассчитана на увеличение массы и объемов тела, а также на повышение силовых показателей! Программа тренировок оптимизирована на силовые упражнения с прогрессией нагрузок. Диета состоит из пищи богатой белками и углеводами с профицитом калорий.',
            description: {
              title: 'Продвинутый - этот уровень для тех, кому нужна сбалансированная программа для достижения поставленной цели',
              list: [
                'умеренная нагрузка в течении всей недели',
                'оптимальный рацион для спортивного тела',
                'вы тренировались ранее',
                'вы имеете представление о базовых упражнениях',
                'вы следите за питанием'],
              text: 'Выбирайте продвинутый уровень прямо сейчас!'
            },
            details: 2
          },
          {
            pid: 9,
            price: 2.99,
            subscription: 0,
            type: 'trainings',
            android_id: 'middle_monthly',
            ios_id: '4fitnessgirls.sub.month.amateur',
            enabled: 1,
            target: 2,
            label: 'Повышение тонуса',
            image: process.env.APIURL + 'public/image/power.png',
            shortDescription: 'Повышение тонуса - программа оптимальна для поддержания текущей формы, сохранения упругости и качества тела! Программа предусматривает тренировки в среднем диапазоне повторений с рабочими весами. Питание - сбалансировано по КБЖУ.',
            description: {
              title: 'Продвинутый - этот уровень для тех, кому нужна сбалансированная программа для достижения поставленной цели',
              list: [
                'умеренная нагрузка в течении всей недели',
                'оптимальный рацион для спортивного тела',
                'вы тренировались ранее',
                'вы имеете представление о базовых упражнениях',
                'вы следите за питанием'],
              text: 'Выбирайте продвинутый уровень прямо сейчас!'
            },
            details: 2
          },
          {
            pid: 10,
            price: 4.99,
            subscription: 0,
            type: 'trainings',
            android_id: 'professional_monthly',
            ios_id: '4fitnessgirls.sub.month.professional',
            enabled: 1,
            target: 0,
            label: 'Жиросжигающая',
            image: process.env.APIURL + 'public/image/weight_loss.png',
            shortDescription: 'Жиросжигающая  - идеальная программа для тех, кто хочет сбросить лишний вес и повысить тонус тела. Программа предусматривает высокоинтенсивные  тренировки при небольших рабочих весах. Питание сбалансировано с учетом дефицита калорий.',
            description: {
              title: 'Профессионал - это подход к тренировкам с самым высоким уровнем ответственности и нагрузки',
              list: [
                'программа контролирует поставленные цели',
                'детально прорабатывает каждую группу мышц',
                'вы готовы уделить построению своего тела много времени',
                'вы чувствуете себя с фитнесом “на ты”',
                'хотите развиваться дальше'],
              text: 'Это максимально емкая программа для максимизации желаемого результата именно для тебя!'
            },
            details: 3
          },
          {
            pid: 11,
            price: 4.99,
            subscription: 0,
            type: 'trainings',
            android_id: 'professional_monthly',
            ios_id: '4fitnessgirls.sub.month.professional',
            enabled: 1,
            target: 2,
            label: 'Повышение тонуса',
            image: process.env.APIURL + 'public/image/power.png',
            shortDescription: 'Повышение тонуса - программа оптимальна для поддержания текущей формы, сохранения упругости и качества тела! Программа предусматривает тренировки в среднем диапазоне повторений с рабочими весами. Питание - сбалансировано по КБЖУ.',
            description: {
              title: 'Профессионал - это подход к тренировкам с самым высоким уровнем ответственности и нагрузки',
              list: [
                'программа контролирует поставленные цели',
                'детально прорабатывает каждую группу мышц',
                'вы готовы уделить построению своего тела много времени',
                'вы чувствуете себя с фитнесом “на ты”',
                'хотите развиваться дальше'],
              text: 'Это максимально емкая программа для максимизации желаемого результата именно для тебя!'
            },
            details: 3
          },
          {
            pid: 12,
            price: 4.99,
            subscription: 0,
            type: 'trainings',
            android_id: 'professional_monthly',
            ios_id: '4fitnessgirls.sub.month.professional',
            enabled: 1,
            target: 1,
            label: 'Увеличение мышечной массы',
            image: process.env.APIURL + 'public/image/mas.png',
            shortDescription: 'Увеличение мышечной массы - программа, позволяющая улучшить пропорции тела. Рассчитана на увеличение массы и объемов тела, а также на повышение силовых показателей! Программа тренировок оптимизирована на силовые упражнения с прогрессией нагрузок. Диета состоит из пищи богатой белками и углеводами с профицитом калорий.',
            description: {
              title: 'Профессионал - это подход к тренировкам с самым высоким уровнем ответственности и нагрузки',
              list: [
                'программа контролирует поставленные цели',
                'детально прорабатывает каждую группу мышц',
                'вы готовы уделить построению своего тела много времени',
                'вы чувствуете себя с фитнесом “на ты”',
                'хотите развиваться дальше'],
              text: 'Это максимально емкая программа для максимизации желаемого результата именно для тебя!'
            },
            details: 3
          },
          {
            pid: 13,
            price: 5.99,
            subscription: 0,
            type: 'pregnant',
            android_id: 'pregnant_monthly',
            ios_id: '4fitnessgirls.sub.month.pregnant',
            enabled: 1,
            target: 4,
            label: 'Фитнес и питание для беременных',
            image: process.env.APIURL + 'public/image/pregnant.png',
            shortDescription: 'Беременным можно приступать к тренировкам только после разрешения врача! Тренировки составлены в зависимости от Вашего срока беременности, носят умеренный характер и не предполагают прогрессии нагрузок. Регулярные тренировки помогают женщинам сохранять хорошее самочувствие во время беременности, укреплять здоровье и быстрее восстанавливаться после родов.',
            description: {
              title: 'Будущая мама - название этой программы говорит само за себя',
              list: [
                'поможет правильно подготовить тело к родам',
                'сохранить тонус тела, не потеряв силы',
                'вы хотите максимально легко пройти период беременности',
                'вы хотите быть в наилучшей своей форме во время ожидания малыша'
              ],
              text: 'Это программа - находка для будущей мамы! Вы можете приступить к занятиям в любой триместр вашей беременности после консультации с врачом'
            },
            details: 4
          },
          {
            pid: 14,
            price: 3.99,
            subscription: 0,
            type: 'restoreTrainings',
            android_id: 'restore_program',
            ios_id: 'restore_program',
            enabled: 1,
            label: 'Возобновление прерваной программы',
            shortDescription: 'Возобновлпение прерванной программы',
            details: null
          },
          {
            pid: 15,
            price: 9.99,
            subscription: 0,
            type: 'maraphone',
            enabled: 0,
            label: 'Участие в марафоне Новичок',
            shortDescription: 'Участие в марафоне как участник Новичок',
            details: 1
          },
          {
            pid: 16,
            price: 19.99,
            subscription: 0,
            type: 'maraphone',
            enabled: 0,
            label: 'Участие в марафоне Продвинутый',
            shortDescription: 'Участие в марафоне как участник Продвинутый',
            details: 2
          },
          {
            pid: 17,
            price: 49.99,
            subscription: 0,
            type: 'maraphone',
            enabled: 0,
            label: 'Участие в марафоне Спорт',
            shortDescription: 'Участие в марафоне как участник Спорт',
            details: 3
          },
          {
            pid: 18,
            price: 0.99,
            subscription: 0,
            type: 4,
            enabled: 0,
            label: 'Ставки на участников марафона',
            shortDescription: 'Ставки на участников марафона ',
            details: null
          },
          {
            pid: 19,
            price: 9.99,
            subscription: 1,
            type: 'premium',
            android_id: 'premium_monthly',
            ios_id: '4fitnessgirls.sub.month.premium',
            enabled: 1,
            label: 'Премиум',
            image: process.env.APIURL + 'public/image/premium.png',
            shortDescription: `Премиум подписка раскрывающая неограниченный доступ ко
                    всем функциям приложения - ежемесячная подписка с автопродлением`,
            details: null
          },
          {
            pid: 20,
            price: 49.99,
            subscription: 1,
            type: 'premium',
            android_id: 'premium_half_year',
            ios_id: '4fitnessgirls.sub.halfyear.premium',
            enabled: 1,
            label: 'Премиум',
            image: process.env.APIURL + 'public/image/premium.png',
            shortDescription: `Премиум подписка раскрывающая неограниченный доступ ко
                    всем функциям приложения - 6 месячная подписка с автопродлением`,
            details: null
          },
        ];
        DB.drop('purchases');
        DB.insert('purchases', purchases, ()=> {});
        res.send({
          message: 'OK'
        });
      } catch (e) {
        console.log(e);
      }
    }
  });
};

// class DBCore {
//   insert(col, data) {
//     connectToDB(col, (collection, close) => {
//       collection.insertMany(data, (err, result) => {
//         if (err)
//           global.sysLog(err, 'err');
//         close();
//       });
//     });
//   }
//   drop(col) {
//     connectToDB(col, (collection, close) => {
//       collection.drop(() => {
//         close();
//       });
//     });
//   }
// };
//
// // const DB = new DBCore();
//
// function connectToDB(col, callback) {
//   MongoClient.connect(MongoUrl, (err, database) => {
//     if (err)
//       global.sysLog(err, 'err');
//     callback(database.db('4FitnessGirls').collection(col), () => {
//       database.close();
//     });
//   });
// }
