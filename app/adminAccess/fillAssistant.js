const fs = require('fs');
const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const MongoUrl = process.env.MONGO_URL;
const DB = require(`${process.env.Core}db.js`);

// Admin Token
const atkn = 'WpNMu$@%o^8!0z@8O>^DY`9ZLgOkKJn_H0:_`z8/o?l#[kM]KU{aMo?nOr>S~?(';

module.exports = (app) => {
	app.post('/admin/fill/assistant', (req, res) => {
		if (req.headers.token == atkn) {
			let messages = [
				{
					alias: 'purchase',
					type: 'text',
					text: ["Welcome, ${user.name}! You've started the workout plan ${training.name}. Date of completion: ${training.endDate}."],
					frequency: 'once',
					action: '',
        },
				{
					alias: 'dayDone',
					type: 'text',
					text: ["Great, ${user.name}! Don't forget, your next workout is planned for ${training.date}."],
					frequency: 'once',
					action: '',
        },
				{
					alias: 'training',
					type: 'text',
					text: ["${user.name}, we are waiting for you to workout today."],
					time: '9:00',
					frequency: 'daily',
					action: '',
        },
				{
					alias: 'trainingMove',
					type: 'transference',
					text: ["${user.name}, you haven't marked a workout today. Postpone for tomorrow?"],
					time: '21:00',
					action: 'transference',
					frequency: 'daily'
        },
				{
					alias: 'trainingProgramEnd',
					type: 'text',
					text: ["${user.name}, your program will end in ${trainings.daysToFinish} days. You are reaching your goal ${plan}"],
					time: '14:00',
					action: '',
					frequency: 'trainingPrograms'
        },
				{
					alias: 'breakfast',
					type: 'text',
					text: [
						"Good morning, ${user.name}! Don't forget about your tasty breakfast.",
						"${user.name}, have a nice day! Have you had breakfast already?",
						"${user.name}, don't forget to have breakfast and smile!",
						"Good morning, ${user.name}! Your breakfast is waiting for you.",
						"${user.name}, please check out what is for breakfast today."
					],
					time: '8:30',
					action: '',
					frequency: 'daily'
        },
				{
					alias: 'nutrition',
					type: 'nutrition',
					text: [
						"${user.name}, you haven't checked your nutrition for today. Please enter the nutrition section and check your meals.",
						"${user.name}, what have you eaten today? Please fill the nutrition section and check your meals.",
						"${user.name}, how was your day? Please enter the nutrition section and check your meals."
					],
					time: '19:00',
					action: 'check'
				},
				{
					alias: 'waterMorning',
					type: 'water',
					text: [
						"Good morning, ${user.name}! Start your day with a glass of water.",
						"${user.name}, have a nice day! Don't forget about water after waking up.",
						"Good morning, ${user.name}! Drink water before breakfast!",
						"Good morning, ${user.name}! It's very important to start your day with a glass of water."
					],
					time: '8:00',
					action: 'check'
				},
				{
					alias:'waterBalance',
					type:'water',
					text:[ "${user.name}, we hope that you haven't forgotten about water balance. Please fill the nutrition section and check in water consumption."],
					action: 'check',
					time: '18:00'
				},
				{
					alias: 'weight',
					type: 'weight',
					text: ["${user.name}, today is the weight control day. Please stand on the weigh-scales and enter your current weight."],
					time: '7:40'
				},
				{
					alias: 'lostUser',
					type: 'text',
					text: {
						3: "Knock-knock! ${user.name}, are you here? Exercises and nutrition are waiting for you.",
						6: "${user.name}, you have been absent for a week...what happened?"
					},
					time: '13:00'
				}
      ];
			DB.drop('assistant');
			DB.insert('assistant', messages, () => {});
			res.send({
				message: 'OK'
			});
		}
	});
};

// class DBCore {
//   insert(col, data) {
//     connectToDB(col, (collection, close) => {
//       collection.insertMany(data, (err, result) => {
//         if (err)
//           global.sysLog(err, 'err');
//         close();
//       });
//     });
//   }
//   drop(col) {
//     connectToDB(col, (collection, close) => {
//       collection.drop(() => {
//         close();
//       });
//     });
//   }
// };
//
// // const DB = new DBCore();
//
// function connectToDB(col, callback) {
//   MongoClient.connect(MongoUrl, (err, database) => {
//     if (err)
//       global.sysLog(err, 'err');
//     callback(database.db('4FitnessGirls').collection(col), () => {
//       database.close();
//     });
//   });
// }
