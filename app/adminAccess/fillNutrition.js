const fs = require('fs');
// const DB = require(process.env.);

const DB = require(`${process.env.Core}db.js`);
// Admin Token
const atkn = 'WpNMu$@%o^8!0z@8O>^DY`9ZLgOkKJn_H0:_`z8/o?l#[kM]KU{aMo?nOr>S~?(';

module.exports = (app) => {

  app.post('/admin/fill/nutrition', (req, res)=> {
    if (req.headers.token == atkn) {
      // // const DB = new DBCore();
      DB.drop('nutrition');
      let nutritionFolder = `${process.env.PrivatePath}nutrition/`;
      fs.readdir(nutritionFolder, (err, files) => {
        let count = 0;
        files.forEach(file => {
          fs.readFile(`${nutritionFolder}${file}`, 'utf-8', (err, data)=> {
            nutritionParser(data);
            count++;
            if (count == files.length) {
              DB.disconnect();
              res.status(200).send({message: 'OK'});
            }
          });
        });
      });
    } else {
      DB.disconnect();
      res.status(401).send();
    }
  });

  function nutritionParser(data) {
    let d = data;
    d = d.replace(/,/g, '.');
    d = d.replace(/;/g, ',');
    d = d.replace(/# /g, '');
    d = d.replace(/;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;/g, '');
    d = d.replace('кол-во грамм', 'грамм');
    d = d.replace('кол-во штук', 'штук');
    d = d.replace('кол-во грамм', 'з-грамм');
    d = d.replace('кол-во штук', 'з-штук');

    d = d.replace('БЕЛОК гр', '-белок');
    d = d.replace('ЖИР грм', '-жир');
    d = d.replace('УГЛЕВ грм', '-углев');
    d = d.replace('ККАЛ', '-ккал');

    const csv = d.split('\n');
    const names = csv[0].split(',');
    let json = {};
    let r = {};

    delete csv[0];

    csv.map((e, i)=> {
      if (e) {
        json[i] = {};
        let n = e.split(',');

        n.map((ne,ni)=> {
          if (ne.length > 0)
            json[i][names[ni]] = ne;
        });
      }
    });
    let day;
    let days;
    let group;
    let priyom;
    let waterLimit;

    for (let e in json) {
      if (json[e]['группы'] && json[e]['группы'].match(/группа/gi)) {
        group = json[e]['группы'].match(/\d/gi).join('');
        if (!r.group)
          r[group] = {};
      }
      if (json[e]['день'] && json[e]['день'].match(/день/gi)) {
        day = json[e]['день'].match(/([0-9]{1,})/gi)[0];
        days = (json[e]['день'].match(/\/([0-9]{1,})/gi)[0]).toString().replace('/', '');
        if (!r[group][day])
          r[group][day] = {
            kilocalories: group,
            day: day,
            days: days,
            waterDrinked: 0,
            waterLimit
          };
        if (!r[group][day]['menu'])
          r[group][day]['menu'] = [];
      }
      if (json[e]['приёма пищи'] && json[e]['приёма пищи'].match(/при[её]м пищи/gi)) {
        priyom = json[e]['приёма пищи'].match(/([0-9]{1})/gi) - 1;
        if (!r[group][day].menu[priyom])
          r[group][day].menu[priyom] = {};
      }
      if (json[e]['приёма пищи'] && json[e]['приёма пищи'].match(/сумма/gi)) {
        r[group][day].menu[priyom].protein = json[e]['-белок'] ? json[e]['-белок'] : 0;
        r[group][day].menu[priyom].fat = json[e]['-жир'] ? json[e]['-жир'] : 0;
        r[group][day].menu[priyom].carbohydrates = json[e]['-углев'] ? json[e]['-углев'] : 0;
        r[group][day].menu[priyom].kilocalories = json[e]['-ккал'] ? json[e]['-ккал'] : 0;
      } else {
        if (json[e]['наименование'] && (json[e]['штук'] || json[e]['грамм'])) {
          let menu = {
            name: beautifyText(json[e]['наименование']),
            gramm: parseInt(json[e]['грамм']) ? parseInt(json[e]['грамм']) : 0,
            count: json[e]['штук'] ? json[e]['штук'] : '0',
            protein: parseInt(json[e]['-белок']) ? parseInt(json[e]['-белок']) : 0,
            fat: parseInt(json[e]['-жир']) ? parseInt(json[e]['-жир']) : 0,
            carbohydrates: parseInt(json[e]['-углев']) ? parseInt(json[e]['-углев']) : 0,
            kilocalories: parseInt(json[e]['-ккал']) ? parseInt(json[e]['-ккал']) : 0
          };
          if (json[e]['замена']) {
            menu['replace'] = {
              name: beautifyText(json[e]['замена']),
              gramm: parseInt(json[e]['з-грамм']) ? parseInt(json[e]['з-грамм']) : 0,
              count: json[e]['з-штук'] ? json[e]['з-штук'] : '0'
            };
            if (json[e]['з-штук'] == 'не ограничено') {
              menu['replace'].count = '∞';
              menu['replace'].gramm = -1;
            }
          }

          if (json[e]['штук'] == 'не ограничено') {
            menu.count = '∞';
            menu.gramm = -1;
          }

          if (!r[group][day].menu[priyom].products)
            r[group][day].menu[priyom].products = [];
          r[group][day].menu[priyom].products.push(menu);

          try {
            if (menu.name == r[group][day].menu[priyom].products[r[group][day].menu[priyom].products.length - 2].name)
              console.log(menu.name, group + ' kkal', e);
          } catch (e) {}
        }
      }
      if (json[e]['']) {
        let count = parseFloat(json[e][''].match(/[0-9](.[0-9])?( )?лит/));
        waterLimit = count * 1000;

        if (count > 3)
          waterLimit = 2000 + (waterLimit / 10);

        r[group][day].waterDrinked = 0;
        if (!r[group][day].waterLimit)
          r[group][day].waterLimit = waterLimit;
      }

    }

    let result = r;
    // console.log(group);
    // // const DB = new DBCore();
    if (group)
      Object.keys(result[group]).map((e,i)=> {
        DB.insert('nutrition', result[group][e], ()=> {});
      });
  }

  function beautifyText(text) {
    let a = text;

    if (a.match(/спаржа\+зелень/))
      a = 'спаржа + зелень';

    a = a.toLowerCase();
    a.replace(/  /g, ' ');
    a.replace(/\t/g, ' ');
    a.replace(/\n/g, ' ');
    a = a.split('');
    for (let i = 0; i > a.length; i++) {
      if (a[0] == ' ')
        a[0] = '';
      else
        return;
    }
    a[0] = a[0].toUpperCase();
    a = a.join('');
    return a;
  }
};
